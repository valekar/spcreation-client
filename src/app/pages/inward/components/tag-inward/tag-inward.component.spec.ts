import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagInwardComponent } from './tag-inward.component';

describe('TagInwardComponent', () => {
  let component: TagInwardComponent;
  let fixture: ComponentFixture<TagInwardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagInwardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagInwardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
