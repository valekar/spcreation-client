import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { routing }       from './change-password.routing';
import { SlimLoadingBarModule} from 'ng2-slim-loading-bar';

import { ChangePasswordComponent } from './change-password.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgaModule,
    routing,
     SlimLoadingBarModule.forRoot()
  ],
  declarations: [
    ChangePasswordComponent
  ]
})
export class ChangePasswordModule { }
