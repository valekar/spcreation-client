import { Component, OnInit,Input,Output,EventEmitter  } from '@angular/core';
import {Order} from '.././../../../../../models/order.model';
import {CONSTANT} from '../../../../../../models/CONSTANTS';


@Component({
  selector: 'app-action-render',
  templateUrl: './action-render.component.html',
  styleUrls: ['./action-render.component.scss']
})
export class ActionRenderComponent implements OnInit {

  @Input() rowData:Order;
  reject:String ;
  approve:String;
  pending:String;
  confirmed:String;
  @Output() actionEmitter : EventEmitter<any> = new EventEmitter(); 

  constructor() { }

  ngOnInit() {

  this.reject = CONSTANT.STATUS_REJECT.toUpperCase();
  this.approve = CONSTANT.STATUS_APPROVE.toUpperCase();
  this.pending = CONSTANT.STATUS_PENDING.toUpperCase();
  this.confirmed = CONSTANT.STATUS_CONFIRMED.toUpperCase();
  }

  pendingOrder(){
    this.actionEmitter.emit(CONSTANT.STATUS_PENDING);
  }

  rejectOrder(){
    this.actionEmitter.emit(CONSTANT.STATUS_REJECT);
  }

  approveOrder(){
    this.actionEmitter.emit(CONSTANT.STATUS_APPROVE);
  }

}
