import { Injectable } from '@angular/core';
import {CONSTANT} from '../../models/CONSTANTS';
import {Response} from '@angular/http'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import {CommonService} from '../../services/common.service';
import {URLS} from '../../models/URLS';
import {Applique} from '../../models/user.model';
import {CustomHttp} from '../../../app-error-interceptor.service';

@Injectable()
export class AppliqueService {

  constructor(private http:CustomHttp,private commonService:CommonService) {}

  getAppliques(){
    return this.http.get(URLS.APPLIQUE_URL,this.commonService.getHeaderOptions())
    .map((res:Response)=>{return res.json();})
    .catch((err:Response) => {return Observable.throw(err.json());});
  }

  getOneApplique(id:string){
    return this.http.get(URLS.APPLIQUE_URL+id,this.commonService.getHeaderOptions())
    .map((res:Response)=>{return res.json()})
    .catch((err:Response)=> {return Observable.throw(err.json())});
  }

  addApplique(applique:Applique){
    return this.http.post(URLS.APPLIQUE_URL, applique, this.commonService.getHeaderOptions())
    .map((res:Response):any => {return res.json()})
    .catch((err:Response)=>{return Observable.throw(err.json())});
  }

  editApplique(applique:Applique, id:number){
    return this.http.put(URLS.APPLIQUE_URL+id,applique,this.commonService.getHeaderOptions())
    .map((res:Response)=>{ return res.json()})
    .catch((err:Response)=>{ return Observable.throw(err.json())});
  }

  deleteApplique(id:string){
    return this.http.delete(URLS.APPLIQUE_URL+id,this.commonService.getHeaderOptions())
    .map((res:Response)=>{ return res.json()})
    .catch((err:Response)=>{ return Observable.throw(err.json())});
  }

  getAppliqueColumns(){
    return {
      number:{
        title:'Applique number',
        type:'string'
      },
      type:{
        title:'Applique Type',
        type:'string',
      },
      color:{
        title:'Color of Applique',
        type:'string'
      },
      updated_by:{
        title:'Updated By',
        type:'string',
        editable:false,
      },
      formatted_updated_at:{
        title :"Updated At",
        type:"string",
        editable:false,
      }
      /*created_by:{
        title:"Created By",
        type:"string",
        editable:false,

      },
      formatted_created_at:{
        title:"Created At",
        type:'string',
        editable:false,
      }*/

    }
  }

}
