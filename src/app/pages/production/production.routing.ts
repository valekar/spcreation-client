import { Routes, RouterModule } from '@angular/router';
import { ProductionComponent } from './production.component';
import { ConfirmOrderComponent } from './components/confirm-order/confirm-order.component';
import { PlanningComponent } from './components/planning/planning.component';
import { InProductionComponent } from './components/in-production/in-production.component';
import { InProductionDetailsComponent } from './components/in-production-details/in-production-details.component';
const routes: Routes = [
  {
    path: '',
    component: ProductionComponent,
    children: [
      { path: 'confirm-orders', component: ConfirmOrderComponent },
      { path: 'planning', component: PlanningComponent },
      { path: 'in-production', component: InProductionComponent },
      { path: 'in-production/:id', component: InProductionDetailsComponent }
    ]
  }
];

export const routing = RouterModule.forChild(routes);