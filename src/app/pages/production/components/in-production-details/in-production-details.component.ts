import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InProductionService } from '../../services/in-production.service';
import { ResObj, SuccessReponseWithObj, SuccessResponse, ErrorResponse } from '../../../models/common.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SlimBarService } from '../../../services/slim-bar.service';
import { ModalComponent } from '../../../services/modal/modal.component';
import { InProduction } from '../../../models/production.model';
import { LocalDataSource } from 'ng2-smart-table';
import { CONSTANT } from '../../../models/CONSTANTS';
import { Tag, Inward } from '../../../models/inward.model';
@Component({
  selector: 'app-in-production-details',
  templateUrl: './in-production-details.component.html',
  styleUrls: ['./in-production-details.component.scss'],
  providers: [InProductionService]
})
export class InProductionDetailsComponent implements OnInit {

  inProdId: number;
  activeModal: any;
  inProdData: InProduction = null;
  settings: any;
  columns: any;
  source: Array<Array<any>> = new Array();
  constructor(private inProdService: InProductionService, private activatedRoute: ActivatedRoute,
    private slimBarService: SlimBarService, private modalService: NgbModal) {
    this.slimBarService.progress();
    this.columns = inProdService.getTagColumns();
    this.settings = {
      actions: {
        add: false,
        edit: true,
        delete: false
      },
      hideSubHeader: false,
      add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
      edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
      delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
      columns: this.columns
    }
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((urlParams: any) => {
      this.inProdId = +urlParams.id;

      this.getInProd(this.inProdId);
    });
  }

  onSaveConfirm(event) {
    if (event.newData.quantityCompleted > event.newData.quantity) {
      this.Modal(CONSTANT.VALIDATION_ERROR, CONSTANT.COMPLETE_QUANTITY, true);

    }
    else {
      this.Modal(CONSTANT.WARNING,
        CONSTANT.UPDATE_DESC + CONSTANT.STRONG_TAG_OPEN + event.data.tagId + " tag with status " + event.newData.status
        + CONSTANT.STRONG_TAG_CLOSE + CONSTANT.QUESTION_MARK,
        false);
      // this is the event from modal
      this.activeModal.result.then((result: boolean) => {
        this.update(event.newData, event);
      });
    }
  }

  private update(tag: Tag, event) {
    this.slimBarService.startLoading();
    this.inProdService.editTag(tag, tag.id)
      .subscribe((res: SuccessReponseWithObj<Inward>) => {
        this.slimBarService.completeLoading();
        this.Modal(res.header, res.description, true);
        //console.log(res);
        event.confirm.resolve();
      },
      (err: ErrorResponse) => {
        //console.log(err);
        this.slimBarService.completeLoading();
        this.Modal(err.header, err.description, true);
      }
      );

  }


  getInProd(id: number) {
    this.slimBarService.startLoading();
    this.inProdService.get(id).subscribe((res: SuccessReponseWithObj<InProduction>) => {
      //this.Modal(res.header,res.description,true);
      this.slimBarService.completeLoading();
      console.log(res.obj);
      this.inProdData = res.obj;
      this.generateSourceForTags();
    })
  }


  generateSourceForTags() {
    let i = 0;
    for (let machine of this.inProdData.machines) {
      if (machine.noOfOrders > 0) {
        this.source[i] = new Array();
        let j = 0;
        for (let order of machine.orders) {
          this.source[i][j] = new LocalDataSource(order.inward.tags);
          j++;
        }
      }
      i++;
    }
  }

  download() {
    this.inProdService.download(this.inProdId);
    //.subscribe((blob:any)=>{
    //   //var saveAs:any;
    //   var link=document.createElement('a');
    //   link.href=window.URL.createObjectURL(blob);
    //   link.download="Report.xlsx";
    //   link.click();
    // });
  }

  private Modal(header: String, description: String, isCloseModal: boolean) {
    this.activeModal = this.modalService.open(ModalComponent, { size: 'sm' });
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }

}
