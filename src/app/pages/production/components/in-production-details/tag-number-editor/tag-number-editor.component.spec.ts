import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagNumberEditorComponent } from './tag-number-editor.component';

describe('TagNumberEditorComponent', () => {
  let component: TagNumberEditorComponent;
  let fixture: ComponentFixture<TagNumberEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagNumberEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagNumberEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
