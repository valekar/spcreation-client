import { Injectable } from '@angular/core';
import {Http, Headers , Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import {SlimBarService} from '../pages/services/slim-bar.service';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {ModalComponent} from '../pages/services/modal/modal.component';
import {CommonService} from '../pages/services/common.service';
import {URLS} from '../pages/models/URLS';
@Injectable()
export class AuthenticationService {

  constructor(private http: Http,private router :Router,private slimBarService:SlimBarService,private commonService:CommonService ) { }

  login(username : string , password :string){
    
    //console.log("the json");
    let headers = new Headers(
      { 'Content-Type': 'application/json', 'Accept' : 'application/json', 'Authorization':"Basic c3Jpbmk6c2VjcmV0"});
        let options = new RequestOptions({ headers: headers });
        //this.slimBarService.startLoading();
    return this.http.post(URLS.AUTHENTICATE_URL, 
            JSON.stringify({username : username , password : password,grant_type:"password"}),options)
              .map((response : Response) => {
                let user = response.json();
                if(user){
                  localStorage.setItem('currentUser', JSON.stringify(user));
                }
                //this.slimBarService.completeLoading();
                return response.json();
              });
  }

  logout() {
    this.slimBarService.startLoading();
    this.http.get(URLS.REVOKE_TOKEN_URL,this.commonService.getHeaderOptions()).subscribe((res:any)=>{console.log(res)});
    localStorage.removeItem('currentUser');
    this.router.navigate([URLS.ROOT_URL]);
    this.slimBarService.completeLoading();  
  }

  isUserLoggedin(){
    if(localStorage.getItem('currentUser')){
      return true;
    }
    return false;
  }

  redirectToLogin(url){
    return this.router.navigate([URLS.ROOT_URL], {queryParams:{returnUrl : url}});
  }
  


}
