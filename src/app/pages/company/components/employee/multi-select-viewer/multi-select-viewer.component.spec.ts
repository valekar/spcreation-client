import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiSelectViewerComponent } from './multi-select-viewer.component';

describe('MultiSelectViewerComponent', () => {
  let component: MultiSelectViewerComponent;
  let fixture: ComponentFixture<MultiSelectViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiSelectViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiSelectViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
