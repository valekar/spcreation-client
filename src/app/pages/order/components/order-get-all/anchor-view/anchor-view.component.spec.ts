import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnchorViewComponent } from './anchor-view.component';

describe('AnchorViewComponent', () => {
  let component: AnchorViewComponent;
  let fixture: ComponentFixture<AnchorViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnchorViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnchorViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
