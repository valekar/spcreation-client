import { Injectable } from '@angular/core';
import {CONSTANT} from '../../models/CONSTANTS';
import {Response} from '@angular/http'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import {CommonService} from '../../services/common.service';
import {URLS} from '../../models/URLS';
import {Customer} from '../../models/company.model';
import {CustomHttp} from '../../../app-error-interceptor.service';

@Injectable()
export class CustomerService {

  constructor(private http:CustomHttp,private commonService:CommonService) { }
  getAll(){
    return this.http.get(URLS.CUSTOMER_URL, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  get(id:number){
    return this.http.get(URLS.CUSTOMER_URL + id, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  add(customer:Customer){
    return this.http.post(URLS.CUSTOMER_URL,customer,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  edit(customer:Customer,id:number){
    return this.http.put(URLS.CUSTOMER_URL+id,customer,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  delete(id:number){
    return this.http.delete(URLS.CUSTOMER_URL+id,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  getColumns(){
    return {
      name:{
        title:"Name of Customer",
        type:"text"
      },
      address:{
        title:"Address",
        editor:{
          type:"textarea"
        }
      },
      updated_by:{
        title:'Updated By',
        type:'text',
        editable:false
      },
      formatted_updated_at:{
        title :"Updated At",
        type:"text",
        editable:false,
      }
    }
  }
}
