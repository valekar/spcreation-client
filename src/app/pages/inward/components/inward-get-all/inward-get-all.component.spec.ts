import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InwardGetAllComponent } from './inward-get-all.component';

describe('InwardGetAllComponent', () => {
  let component: InwardGetAllComponent;
  let fixture: ComponentFixture<InwardGetAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InwardGetAllComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InwardGetAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
