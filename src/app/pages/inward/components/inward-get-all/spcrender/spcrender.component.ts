import { Component, OnInit,Input } from '@angular/core';
import {Inward} from '../../../../models/inward.model';

@Component({
  selector: 'app-spcrender',
  templateUrl: './spcrender.component.html',
  styleUrls: ['./spcrender.component.scss']
})
export class SPCRenderComponent implements OnInit {

  @Input() rowData:Inward;
  constructor() { }

  ngOnInit() {
  }

}
