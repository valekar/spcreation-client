import { Component, OnInit } from '@angular/core';

import { Inward, Tag } from '../../../models/inward.model';
import { ResObj, ErrorResponse, SuccessReponseWithObj } from '../../../models/common.model';
import { Validators, FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { CONSTANT } from '../../../models/CONSTANTS';
import { SlimBarService } from '../../../services/slim-bar.service';
import { ModalComponent } from '../../../services/modal/modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { Employee } from '../../../models/company.model';
import { PlanningService } from '../../services/planning.service';
import { DragulaService } from 'ng2-dragula/ng2-dragula';
import {Checking } from '../../../models/checking.model';
@Component({
  selector: 'app-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.scss'],
  providers: [PlanningService]
})
export class PlanningComponent implements OnInit {

  noOfEmployees = 15;
  dummyArray: Array<number> = new Array();
  planningForm: FormGroup;
  helperList: Array<Employee> = new Array();
  checkerList: Array<Employee> = new Array();
  tagList: Array<Tag>;
  activeModal:any;
  STATUS_PRODUCED: string = CONSTANT.STATUS_PRODUCED;
  constructor(private slimBarService: SlimBarService, private router: Router, private dragulaService: DragulaService,
    private _fb: FormBuilder, private modalService: NgbModal, private planningService: PlanningService) {
    this.slimBarService.progress();
  }

  ngOnInit() {
    this.initializeDummyArray();
    this.planningForm = this.checkingForm();
    this.getEmployees();
    this.getProducedTags();
    this.dragulaCalculations();

    // this.planningService.getAll().subscribe((res:ResObj<any>)=>{
    //   console.log(res.obj);
    // });
  }


  checkingForm() {
    return this._fb.group({
      shift: ['',Validators.required],
      planningDate: ['',Validators.required],
      noOfEmployees: ['',Validators.required],
      employees: this._fb.array([]),
      confirmCheckBox:[false,Validators.required]
    });
  }

  employeeForm() {
    return this._fb.group({
      checker: ['',Validators.required],
      helper: ['',Validators.required],
      assignedQuantity: [0],
      assignedTags: this._fb.array([])
    });
  }

  tagForm(tag: Tag) {
    return this._fb.group({
      tag: [tag,Validators.required]
    });
  }


  initializeDummyArray() {
    for (let i = 0; i < this.noOfEmployees; i++) {
      this.dummyArray.push(i);
    }
  }
 //does nothing but keep it
  planDateChanged(){
  //does nothing but keep it, coz it complains while bundling
  }

  changeInEmployeesCount(employee:any) {
    let count = this.planningForm.get('noOfEmployees').value;
    this.planningForm.controls.employees = this._fb.array([]);
    const employeeControl = <FormArray>this.planningForm.controls.employees;
    for (let i = 0; i < count; i++) {
      employeeControl.push(this.employeeForm());
    }
  }

  getEmployees() {
    this.planningService.getEmployees().subscribe((res: ResObj<Employee>) => {
      //this.helperList = res.obj;
      for (let employee of res.obj) {
        //console.log(employee);
        for (let departartment of employee.departments) {
          if (departartment.name.toUpperCase() == CONSTANT.HELPER.toUpperCase()) {
            //console.log(employee);
            this.helperList.push(employee);
          }
          else if (departartment.name.toUpperCase() == CONSTANT.CHECKER.toUpperCase()) {
            //console.log(employee);
            this.checkerList.push(employee);
          }
        }
      }
    });
  }

  getProducedTags() {
    this.planningService.getProducedTags().subscribe((res: ResObj<Tag>) => {
      this.tagList = res.obj;
      //console.log(this.tagList);
    });
  }


  dragulaCalculations() {
    this.dragulaService.drop.subscribe((value) => {
      let tagsToBeCheckedTd: Element = null;
      let producedTagsTd: Element = null;
      let i = +value[0];

      let producedTdStr = "produced_" + i;
      let toBeProducedTdStr = "toBeProduced_" + i;
      let element_2: Element = value[2];
      let element_3: Element = value[3];
      if (element_2.id == producedTdStr) {
        producedTagsTd = element_2;
        tagsToBeCheckedTd = element_3;
      }
      else {
        producedTagsTd = element_3;
        tagsToBeCheckedTd = element_2;
      }
      //let tsag:Tag = tagsToBeCheckedTd.children[0].children[1].id;
      //console.log(tagsToBeCheckedTd.children);
      this.tagsTobeProccessed(i, tagsToBeCheckedTd);
    });
  }

  tagsTobeProccessed(i: number, tagsToBeCheckedTd: Element) {
    let draggedTagList: Array<Tag> = new Array();
    for (let i = 0; i < tagsToBeCheckedTd.children.length; i++) {
      let tag_id = tagsToBeCheckedTd.children[i].children[1].id;
      let tag: Tag = this.tagList.find(x => x.id == +tag_id);
      draggedTagList.push(tag);
    }
    //add into the form control
    let employeeControl = this.planningForm.controls.employees['controls'][i].controls;
    employeeControl.assignedTags = this._fb.array([]);
    
    let totalQuantity = 0;
    if (draggedTagList.length > 0) {
      for (let tag of draggedTagList) {
        employeeControl.assignedTags.push(this.tagForm(tag));
        totalQuantity += tag.quantity;
      }
    }
    employeeControl.assignedQuantity.setValue(totalQuantity);
  }

  setCheckingPlanner() {
    //console.log(this.planningForm);
    this.Modal(CONSTANT.ARE_YOU_SURE,CONSTANT.CREATE_DESC,false);

    this.activeModal.result.then(()=>{
      this.callPostPlanner();
    });
    
  }

  callPostPlanner(){
    let finalObj:Checking = this.objectMapping(this.planningForm);
    let valid:boolean = this.finalObjValidation(finalObj);
    if(valid){
      this.slimBarService.startLoading();
      this.planningService.add(finalObj).subscribe((res:SuccessReponseWithObj<Checking>)=>{
        this.slimBarService.completeLoading();
        console.log(res.obj);
      },(err:ErrorResponse)=> {this.slimBarService.completeLoading()});
    }
  }

  objectMapping(planningForm:FormGroup){
    let finalObj:Checking = new Checking();
    let planningControls = planningForm.controls;
    
     finalObj.shift = planningControls.shift.value;
     finalObj.planningDate = planningControls.planningDate.value;
     finalObj.employees = planningControls.employees.value;

    //console.log(finalObj);
    return finalObj;
  }

  finalObjValidation(finalObj:any){
    let valid = true;
    for(let employee of finalObj.employees){
        if(employee.checker == ""){
          this.Modal(CONSTANT.VALIDATION_ERROR,CONSTANT.CHECKER_EMPTY_DESC,true);
          valid = false;
          break;
        }
        if(employee.helper == ""){
          this.Modal(CONSTANT.VALIDATION_ERROR,CONSTANT.HELPER_EMPTY_DESC,true);
          valid = false;
          break;
        }
        if(employee.assignedQuantity == 0){
          this.Modal(CONSTANT.VALIDATION_ERROR,CONSTANT.TAG_EMPTY_DESC,true);
        }
    }
    return valid;
  }

  private Modal(header: String, description: String, isCloseModal: boolean) {
    this.activeModal = this.modalService.open(ModalComponent, { size: 'lg' });
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }

}
