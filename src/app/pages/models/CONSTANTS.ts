export class CONSTANT{
    public static UNAUTH_HEADER = "Login Failed!";
    public static UNAUTH_DESCRITPION = "Please enter correct credentials"; 
    public static CONFIRM_YES = true;
    public static CONFIRM_NO = false;
    public static DELETE_WARNING_DESC = "Are you sure that you want to delete ";
    public static ARE_YOU_SURE = "Are you sure?";
    public static DO_YOU_WANT_TO_CONFIRM = "Do you really want to confirm the Order?<br>";
    public static CANNOT_REVERT_BACK_NOTE = "NOTE:You will not be able to revert back again"; 
    public static WARNING = "Warning!";
    public static STRONG_TAG_OPEN = "<strong>";
    public static STRONG_TAG_CLOSE = "</strong>";
    public static DELETED_HEADER = "Deleted!";
    public static DELETED_DESC = "Successfully deleted";
    public static NOTICE_UPDATE = "Update!";
    public static NOTICE_CREATE = "Create!";
    public static UPDATE_DESC = "Are you sure that you want to update ";
    public static CREATE_DESC = "Are you sure that you want to create?";
    public static QUESTION_MARK:string = " ?";
    public static BACK_TO_ORDERS = "/pages/orders/";
    public static ORDERS_EDIT = "/pages/orders/";
    public static BACK_TO_INWARD_TAGS = "/pages/inwards/tags";
    public static TO_IN_PROD_REDIRECT = "/pages/productions/in-production/";

    public static PRIVILEGE_LIST = [
        {value:"none",title:"None"},
        //{value:"",title:"None"},
        {value:"write",title:"Write"},
        //{value:"read",title:"Read-Only"}
    ];

    public static NONE = "none";

    public static ENABLED_LIST = [
        {value:"true",title:"True"},
        {value:"false",title:"False"}
    ];

    public static ADMIN_TABLE_COLUMNS =  {
        username: {
          title: 'Username',
          type: 'string',
        },
        email: {
          title: 'E-mail',
          type: 'text',
        },
        enabled:{
            title:'Enabled',
            type:'text',
            editor:{
                type:'list',
                config:{
                    list:CONSTANT.ENABLED_LIST
                }
            }
        },
      }

    public static ADMIN_TABLE_ADD_CONFIG =  {
        addButtonContent: '<i class="ion-ios-plus-outline"></i>',
        createButtonContent: '<i class="ion-checkmark"></i>',
        cancelButtonContent: '<i class="ion-close"></i>',
        confirmCreate:true
      }
    public static ADMIN_TABLE_EDIT_CONFIG = {
        editButtonContent: '<i class="ion-edit"></i>',
        saveButtonContent: '<i class="ion-checkmark"></i>',
        cancelButtonContent: '<i class="ion-close"></i>',
        confirmSave:true,
        mode:external
      }
    public static ADMIN_TABLE_DELETE_CONFIG =  {
        deleteButtonContent: '<i class="ion-trash-a"></i>',
        confirmDelete: true
      }  

    public static ENABALED_COLUMN = "enabled";
    public static FALSE = "false"; 
    public static TRUE = "true";


    //validations
    public static VALIDATION_ERROR = "Validation Error!";
    public static RATE_VALIDATION_MESSAGE ="Please enter only numbers for the fields \"Rate\" and \"Number of Stitches\"";
    public static MACHINE_VALIDATION_MESSAGE = "Please enter only numerical values for the field \"No of Heads\"";
    public static APPLIQUE_VALIDATION_MESSAGE = "Please enter only number for the field \"Applique Number\"";
    public static PAPER_VALIDATION_MESSAGE = "Please enter only number for the field \"Size\"";
    public static EMPLOYEE_VALIDATION_MESSAGE = "Please enter only numerical values for the field \"Age\"";
    public static SELECT_APPLIQUE = "Please select applique";
    public static SELECT_PAPER = "Please select paper";
    public static SELECT_THREAD = "Pleae select thread";
    public static SELECT_ORDERS = "Please select atleast one order";
    public static DRAG_TAGS = "Drag atleast one tag into \"To be Processed\" field";
    public static MUST_SELECT_OPERATOR = "You must select an operator";
    public static MUST_SELECT_FRAMER = "You must select an framer";
    public static MUST_SELECT_INCHARGE = "You must select an in-charge";
    public static COMPLETE_QUANTITY = "Quantity completed value is greater than original quantity";
    public static CANNOT_CHANGE_CONFIRMED_INWARDS = "Cannot change a confirmed order/inward";
    public static CHECKER_EMPTY_DESC = "Checker cannot be empty.Please verify again";
    public static HELPER_EMPTY_DESC = "Helper cannot be empty. Please verify once";
    public static TAG_EMPTY_DESC = "Tag cannot be empty. Please verify once.";

    //order approvals
    public static ORDER_APPROVE="Approve Order";
    public static ORDER_PENDING = "Order Pending";
    public static ORDER_REJECT="Reject Order";
      //status
    public static STATUS_APPROVE="APPROVED";
    public static STATUS_PENDING = "PENDING";
    public static STATUS_REJECT="REJECTED";
    public static STATUS_CONFIRMED="CONFIRMED";
    public static STATUS_IN_PRODUCTION = "IN PRODUCTION";
    public static STATUS_PRODUCED = "PRODUCED";
    //TAG STATUSES
    public static STATUS_COMPLETED = "COMPLETED";
    public static STATUS_CHECKED = "CHECKED";
    //for plannng drop down 
    public static FRAMER = "framer";
    public static INCHARGE = "incharge";
    public static OPERATOR = "operator";
    public static HELPER = "helper";
    public static CHECKER = "checker";
}


