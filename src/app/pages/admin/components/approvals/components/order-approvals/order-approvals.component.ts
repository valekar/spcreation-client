import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SlimBarService } from '../../../../../services/slim-bar.service';
import { ErrorResponse, SuccessReponseWithObj, ResObj, SuccessResponse } from '../../../../../models/common.model';
import { LocalDataSource } from 'ng2-smart-table';
import { CONSTANT } from '../../../../../models/CONSTANTS';
import { ModalComponent } from '../../../../../services/modal/modal.component';
import { Order } from '../../../../../models/order.model';
import { ApprovalsService } from '../../services/approvals.service';
import { ActionRenderComponent } from '../../components/order-approvals/action-render/action-render.component';


@Component({
  selector: 'app-order-approvals',
  templateUrl: './order-approvals.component.html',
  styleUrls: ['./order-approvals.component.scss']
})
export class OrderApprovalsComponent implements OnInit {

  settings: any;
  columns: any;
  activeModal: any;
  source: LocalDataSource;
  orderList: Array<Order>;
  //actions:any;


  constructor(private approvalsService: ApprovalsService, private slimBarService: SlimBarService,
    private modalService: NgbModal) {
    this.slimBarService.progress();
    this.columns = approvalsService.getColumns();
  
    //add actions to column
    this.columns["Actions"] = this.actionJSON();

    this.settings = {
      actions: {
        add: false,
        edit: false,
        delete: false
      },
      pager: {
        perPage: 8
      },
      hideSubHeader: false,
      add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
      edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
      delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
      columns: this.columns,
      // rowClassFunction:(row)=>{
      //   console.log("rowasasdsa");
      // }
    };
    
  }

  ngOnInit() {
    this.getAllOrders();
  }

  actionJSON() {
    return {
      title: "Actions",
      type: "custom",
      renderComponent: ActionRenderComponent,
      onComponentInitFunction: (instance) => {
        instance.actionEmitter.subscribe(row => {
          if (row == CONSTANT.STATUS_APPROVE) {
            this.approveOrder(instance.rowData);
          }
          if (row == CONSTANT.STATUS_REJECT) {
            this.rejectOrder(instance.rowData);
          }
          if (row == CONSTANT.STATUS_PENDING) {
            this.pendingOrder(instance.rowData);
          }
        });
      },

      filter: false
    };
  }


  private Modal(header: String, description: String, isCloseModal: boolean) {
    this.activeModal = this.modalService.open(ModalComponent, { size: 'sm' });
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }

  private getAllOrders() {
    this.slimBarService.startLoading();
    this.approvalsService.getAll().subscribe(
      (res: ResObj<Order>) => {
        this.orderList = res.obj;
        //console.log(this.orderList);
        this.source = new LocalDataSource(this.orderList);
        this.slimBarService.completeLoading();
      },
      (err: ErrorResponse) => {
        this.slimBarService.completeLoading();
      }
    );
  }

  approveOrder(order: Order) {
    this.slimBarService.startLoading();
    this.approvalsService.approveOrder(order.id)
      .subscribe((res: SuccessReponseWithObj<Order>) => {
        this.slimBarService.completeLoading();
        this.Modal(res.header, res.description, true);
        this.source.remove(order);
        this.source.add(res.obj);
        this.source.refresh();
      },
      (err: ErrorResponse) => {
        this.Modal(err.header, err.description, true);
      }
      );
  }

  rejectOrder(order: Order) {
    this.slimBarService.startLoading();
    this.approvalsService.rejectOrder(order.id)
      .subscribe((res: SuccessReponseWithObj<Order>) => {
        this.slimBarService.completeLoading();
        this.Modal(res.header, res.description, true);
        this.source.remove(order);
        this.source.add(res.obj);
        this.source.refresh();
      },
      (err: ErrorResponse) => {
        this.Modal(err.header, err.description, true);
      }
      );

  }

  pendingOrder(order: Order) {
    this.slimBarService.startLoading();
    this.approvalsService.pendingOrder(order.id)
      .subscribe((res: SuccessReponseWithObj<Order>) => {
        this.slimBarService.completeLoading();
        this.Modal(res.header, res.description, true);
        this.source.remove(order);
        this.source.add(res.obj);
        this.source.refresh();
      },
      (err: ErrorResponse) => {
        this.Modal(err.header, err.description, true);
      }
      );

  }

}
