export class URLS {

  public static ROOT_URL = "/";
  
  public static AUTHENTICATE_URL = "/oauth/token";
  public static PAGES_ORDER_URL = "/pages/orders";


  //ALL BELOW URLS REQUIRE CHANGE IN DOMAIN NAMES
    
  public static server_url:string = "";

  public static REVOKE_TOKEN_URL =  URLS.server_url + "/api/oauth/revoke/token";
  public static USER_REGISTER_URL = URLS.server_url + "/api/users/register";
  public static CHANGE_PASSWORD_VALIDATE_URL  = URLS.server_url + "/api/users/change-password/validate";
  public static USER_PASSOWRD_RESET_URL = "/api/users/reset-password";
  public static USER_CHANGE_PASSWORD_URL = "/api/users/change-password";
  
  public static ADMIN_GET_USERS: string = "/api/admin/users/";
  public static ADMIN_DELETE_USER: string = "/api/admin/users/";
  public static ADMIN_UPDATE_USER: string = "/api/admin/users/";
  public static ADMIN_CREATE_USER: string = "/api/admin/users/";
  //this is for applique
  public static APPLIQUE_URL: string = "/api/appliques/";
  //paper
  public static PAPER_URL: string = URLS.server_url + "/api/papers/";
  //machine
  public static MACHINE_URL: string = "/api/machines/";
  //department
  public static DEPARTMENT_URL: string = "/api/departments/";
  //thread
  public static THREAD_URL: string = "/api/threads/";
  //rate
  public static RATE_URL: string = "/api/rates/";
  //customer
  public static CUSTOMER_URL: string = "/api/customers/";
  //employee
  public static EMPLOYEE_URL: string = "/api/employees/";
  //order
  public static ORDER_URL: string = "/api/orders/";
  //order count
  public static ORDER_COUNT_URL = "/api/orders/count/";
  // order page
  public static ORDER_PAGE_URL = "/api/orders?";
  // approval orders
  public static GET_APPROVAL_ORDERS_PAGE_URL = "/api/approvals/orders/";
  //approve orders
  public static APPROVE_ORDER_URL = "/api/approvals/order/approve/"
  //reject orders
  public static REJECT_ORDER_URL = "/api/approvals/order/reject/"
  //reject orders
  public static PENDING_ORDER_URL = "/api/approvals/order/pending/"
  //inwards
  public static INWARD_URL = "/api/inwards/";
  //approved orders for dropdown
  public static APPROVED_ORDERS_URL = "/api/inwards/approved/orders";
  //tags
  public static TAG_URL = "/api/tags/";
  //confirm order through Inwards
  public static CONFIRM_ORDER = "/api/productions/inwards/order/";
  public static INWARD_APPROVED_ORDERS_URL = "/api/productions/inwards/";
  //get confirmed orders
  public static GET_CONFIRMED_ORDER_INWARDS = "/api/productions/inwards/orders/confirmed/";
  //employee url for planning / checking
  public static PLANNING_EMPLOYEE_URL = "/api/employees?type=";
  //constants
  public static CONSTANT_URL = "/api/constants/";
  //set production planning
  public static PRODUCTION_PLANNING_SET_URL = "/api/productions/set/planning";
  //production 
  public static PRODUCTION_URL = "/api/productions/";
  //checking
  public static CHECKING_URL = "/api/checkings/";
  //checking - tags get produced tags
  public static CHECKING_PRODUCED_TAGS_URL = "/api/checkings/produced/tags/";
  //reports
  public static REPORTS_INWARD_CUSTOMER_URL = "/api/reports/inwards/customer/";
  public static PRODUCTION_PLANNING_DOWNLOAD_URL = "/api/production/planning/";
  public static CHECKING_PLANNING_DOWNLOAD_URL = "/api/checking/planning/";
} 