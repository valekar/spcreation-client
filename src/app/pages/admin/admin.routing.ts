import { Routes, RouterModule }  from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { AdminComponent } from './admin.component';
import {UserPermissionsComponent} from './components/user-permissions/user-permissions.component';
import {ApprovalsComponent} from './components/approvals/approvals.component';
import {ConstantComponent} from './components/constant/constant.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children:[
      {path:"user-permissions",component:UserPermissionsComponent},
      {path:"approvals",loadChildren:'./components/approvals/approvals.module#ApprovalsModule'},
      {path:"constants", component:ConstantComponent}
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
