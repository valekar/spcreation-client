import { TestBed, inject } from '@angular/core/testing';

import { SlimBarService } from './slim-bar.service';

describe('SlimBarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SlimBarService]
    });
  });

  it('should be created', inject([SlimBarService], (service: SlimBarService) => {
    expect(service).toBeTruthy();
  }));
});
