import { Component, OnInit } from '@angular/core';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {SlimBarService} from '../../../services/slim-bar.service';
import {ErrorResponse,SuccessReponseWithObj,ResObj,SuccessResponse} from '../../../models/common.model';
import {LocalDataSource } from 'ng2-smart-table';
import {CONSTANT} from '../../../models/CONSTANTS';
import {ModalComponent} from '../../../services/modal/modal.component';
import {Order} from '../../../models/order.model';
import {OrderService} from '../../services/order.service';
import {ShareOrderDataService} from '../../services/share-order-data.service';

@Component({
  selector: 'app-order-get-all',
  templateUrl: './order-get-all.component.html',
  styleUrls: ['./order-get-all.component.scss'],
  providers:[SlimBarService,OrderService]
})
export class OrderGetAllComponent implements OnInit {

  settings:any;
  columns:any;
  activeModal:any;
  source:LocalDataSource;
  orderList:Array<Order>;
  constructor(private orderService:OrderService,private slimBarService:SlimBarService,
    private modalService:NgbModal) {
    this.slimBarService.progress();
    this.columns =orderService.getColumns();
    this.settings = {
      actions:{
        add:false,
        edit:false,
        delete:false
      },
      pager:{
        perPage:8
      },
      hideSubHeader: false,
      add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
      edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
      delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
      columns:this.columns
   }
  }

  ngOnInit() {
    this.getAllOrders();
  }

  

  private Modal(header:String,description:String,isCloseModal:boolean){
    this.activeModal = this.modalService.open(ModalComponent, {size: 'sm'});
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }

  private getAllOrders(){
    this.slimBarService.startLoading();
    //for now lets not use server pagination
    let page = 0;
    let size = 0;
    //for now lets not use server pagination
    this.orderService.getAll(page,size).subscribe(
      (res:ResObj<Order>)=>{
        this.orderList = res.obj;
        //console.log(this.orderList);
        this.source = new LocalDataSource(this.orderList);
        this.slimBarService.completeLoading();
      },
      (err:ErrorResponse) => {
        this.slimBarService.completeLoading();
      }
    );
  }
}
