import { Injectable } from '@angular/core';
import {CustomHttp} from '../../../app-error-interceptor.service';
import {CONSTANT} from '../../models/CONSTANTS';
import {Response} from '@angular/http'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import {CommonService} from '../../services/common.service';
import {URLS} from '../../models/URLS';
import {Checking } from '../../models/checking.model';
@Injectable()
export class PlanningService {

  constructor(private http:CustomHttp,private commonService:CommonService) { }

  getAll(){
    return this.http.get(URLS.CHECKING_URL,this.commonService.getHeaderOptions())
    .map((res:Response)=> {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  add(checking:Checking){
    return this.http.post(URLS.CHECKING_URL,checking,this.commonService.getHeaderOptions())
    .map((res:Response)=> {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  getProducedTags(){
    return this.http.get(URLS.CHECKING_PRODUCED_TAGS_URL, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  getEmployees(){
    return this.http.get(URLS.PLANNING_EMPLOYEE_URL,this.commonService.getHeaderOptions())
    .map((res:Response)=> {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  getCheckers(){
    return this.http.get(URLS.PLANNING_EMPLOYEE_URL+CONSTANT.CHECKER,this.commonService.getHeaderOptions())
    .map((res:Response)=> {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  
}
