import { TestBed, inject } from '@angular/core/testing';

import { AppliqueService } from './applique.service';

describe('AppliqueService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppliqueService]
    });
  });

  it('should be created', inject([AppliqueService], (service: AppliqueService) => {
    expect(service).toBeTruthy();
  }));
});
