import { Injectable } from '@angular/core';
import {CONSTANT} from '../../models/CONSTANTS';
import {Response} from '@angular/http'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import {CommonService} from '../../services/common.service';
import {URLS} from '../../models/URLS';
import {Department} from '../../models/company.model';
import {CustomHttp} from '../../../app-error-interceptor.service';
import {DropDownEditorComponent} from '../components/department/drop-down-editor/drop-down-editor.component';


@Injectable()
export class DepartmentService {

  constructor(private http:CustomHttp,private commonService:CommonService) { }

    getAll(){
      return this.http.get(URLS.DEPARTMENT_URL, this.commonService.getHeaderOptions())
      .map((res:Response) => {return res.json()})
      .catch((err:Response)=> { return Observable.throw(err.json())});
    }
  
    get(id:number){
      return this.http.get(URLS.DEPARTMENT_URL + id, this.commonService.getHeaderOptions())
      .map((res:Response) => {return res.json()})
      .catch((err:Response)=> { return Observable.throw(err.json())});
    }
  
    add(department:Department){
      return this.http.post(URLS.DEPARTMENT_URL,department,this.commonService.getHeaderOptions())
      .map((res:Response) => {return res.json()})
      .catch((err:Response)=> { return Observable.throw(err.json())});
    }
  
    edit(department:Department,id:number){
      return this.http.put(URLS.DEPARTMENT_URL+id,department,this.commonService.getHeaderOptions())
      .map((res:Response) => {return res.json()})
      .catch((err:Response)=> { return Observable.throw(err.json())});
    }
  
    delete(id:number){
      return this.http.delete(URLS.DEPARTMENT_URL+id,this.commonService.getHeaderOptions())
      .map((res:Response) => {return res.json()})
      .catch((err:Response)=> { return Observable.throw(err.json())});
    }

    getAllConstants(){
      return this.http.get(URLS.CONSTANT_URL,this.commonService.getHeaderOptions())
      .map((res:Response) => {return res.json()})
      .catch((err:Response)=> { return Observable.throw(err.json())});
    }
  
    getColumns(){
      return {
        name:{
          title:"Name of Department",
          type:"string",
          editor:{
            type:"custom",
            component:DropDownEditorComponent
          }
        },
        updated_by:{
          title:'Updated By',
          type:'string',
          editable:false,
        },
        formatted_updated_at:{
          title :"Updated At",
          type:"string",
          editable:false,
        }
      }
    }
  
}
