import { Component, OnInit } from '@angular/core';
import {SuccessResponse,ErrorResponse,SuccessReponseWithObj,ResObj} from '../../../models/user.model';
import {LocalDataSource } from 'ng2-smart-table';
import {CONSTANT} from '../../../models/CONSTANTS';
import {ModalComponent} from '../../../services/modal/modal.component';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {SlimBarService} from '../../../services/slim-bar.service';
import {ConstantService} from '../../services/constant.service';
import {Constant} from '../../../models/constant.model';

@Component({
  selector: 'app-constant',
  templateUrl: './constant.component.html',
  styleUrls: ['./constant.component.scss'],
  providers:[SlimBarService,ConstantService]

})
export class ConstantComponent implements OnInit {

  settings:any;
  columns:any;
  activeModal:any;
  source:LocalDataSource;
  constantList:Array<Constant>;

  constructor(private constantService:ConstantService,private slimBarService:SlimBarService,private modalService:NgbModal) { 
    this.slimBarService.progress();
    this.columns =constantService.getColumns();
    this.settings = {
      actions:{
        add:true,
        edit:false,
        delete:false
      },
      hideSubHeader: false,
      add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
      edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
      delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
      columns:this.columns
    }
  }

  ngOnInit() {
    this.getConstants();
  }

  onCreateConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.NOTICE_CREATE, CONSTANT.CREATE_DESC,false);
    //this is the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.createConstant(event.newData,event);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
  }


  onSaveConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.NOTICE_UPDATE, 
      CONSTANT.UPDATE_DESC + CONSTANT.STRONG_TAG_OPEN+ event.data.key + CONSTANT.STRONG_TAG_CLOSE + CONSTANT.QUESTION_MARK,
      false);
    //this the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.updateConstant(event.newData,event);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
  }

  onDeleteConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.WARNING, 
      CONSTANT.DELETE_WARNING_DESC + CONSTANT.STRONG_TAG_OPEN+ event.data.key + CONSTANT.STRONG_TAG_CLOSE+ CONSTANT.QUESTION_MARK,
      false);
    // this is the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.deleteConstant(event.data.id);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
    
  }

  private createConstant(constant:Constant,event){
   
      this.slimBarService.startLoading();
      // console.log(user);
       this.constantService.add(constant)
       .subscribe((res:SuccessReponseWithObj<Constant>)=>{
         this.slimBarService.completeLoading();
         this.getConstants();
         this.Modal(res.header,res.description,true);
         //console.log(res);
         event.confirm.resolve();
       },
       (err:ErrorResponse)=>{
         this.slimBarService.completeLoading();
         this.Modal(err.header,err.description,true);
       }
     );
    
    
  }

  private deleteConstant(id:number){
    this.slimBarService.startLoading();
     this.constantService.delete(id).subscribe(
       (res:SuccessResponse)=>{
         
        for(let i=0;i<this.constantList.length;i++){
          if(id == this.constantList[i].id){
            this.constantList.splice(i,1);
            break;
          } 
        } 
        this.source.load(this.constantList);
        this.slimBarService.completeLoading(); 
        this.Modal(res.header,res.description,true);      
       },
       (err:ErrorResponse) => {
        this.slimBarService.completeLoading();
        this.Modal(err.header,err.description,true);
       }
      ); 
  }

  private updateConstant(constant:Constant,event){
      this.slimBarService.startLoading();
      this.constantService.edit(constant,constant.id)
      .subscribe((res:SuccessReponseWithObj<Constant>) => {
        this.slimBarService.completeLoading(); 
        this.Modal(res.header,res.description,true);
        //console.log(res);
        event.confirm.resolve();
      },
      (err:ErrorResponse) => {
        //console.log(err);
        this.slimBarService.completeLoading();
        this.Modal(err.header,err.description,true);
       }
    );
    
     
  }

  private Modal(header:String,description:String,isCloseModal:boolean){
    this.activeModal = this.modalService.open(ModalComponent, {size: 'sm'});
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }


  private getConstants(){
    this.slimBarService.startLoading();
    this.constantService.getAll().subscribe((res:ResObj<Constant>)=>{
      //console.log(res);
      this.constantList = res.obj;
      this.source = new LocalDataSource(this.constantList);
      this.slimBarService.completeLoading();
    },
    (err:ErrorResponse) => {console.log(err)}
    );
  }
}
