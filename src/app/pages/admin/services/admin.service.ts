import { Injectable } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { URLS } from '../../models/URLS';
import { SlimBarService } from '../../services/slim-bar.service';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { SuccessResponse, ErrorResponse, ModifiedUser } from '../../models/user.model';
import { Observable } from 'rxjs/Observable';
//import 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { CONSTANT } from '../../models/CONSTANTS';
import { CustomHttp } from '../../../app-error-interceptor.service';
import { DropDownEditorComponent } from '../components/user-permissions/drop-down-editor/drop-down-editor.component';
@Injectable()
export class AdminService {

  constructor(private http: CustomHttp, private commonService: CommonService, private slimBarService: SlimBarService) {
    this.slimBarService.progress();
  }


  adminGetUsers() {
    this.slimBarService.startLoading();
    return this.http.get(URLS.ADMIN_GET_USERS, this.commonService.getHeaderOptions())
      .map((res: Response) => {
        this.slimBarService.completeLoading();
        return res.json();
      })
  }
  adminDeleteUser(id: string) {
    return this.http.delete(URLS.ADMIN_DELETE_USER + id, this.commonService.getHeaderOptions())
      .map((res: Response) => { return res.json() })
      .catch((err: Response) => { return Observable.throw(err.json()) });
  }
  adminUpdateUser(user: ModifiedUser) {
    return this.http.put(URLS.ADMIN_UPDATE_USER + user.id, user, this.commonService.getHeaderOptions())
      .map((res: Response) => { return res.json() })
      .catch((err: Response) => { return Observable.throw(err.json()) });
  }
  adminCreateUser(user: ModifiedUser) {
    //console.log(user);
    return this.http.post(URLS.ADMIN_CREATE_USER, user, this.commonService.getHeaderOptions())
      .map((res: Response) => { return res.json() })
      .catch((err: Response) => { return Observable.throw(err.json()) });
  }

  getAdminColumns() {
    return {
      // id: {
      //   title: 'ID',
      //   type: 'text',
      //   //editable: true,
      //   //addable: false,
      //   //show:false
      // },
      username: {
        title: 'Username',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'text',
      },
      enabled: {
        title: 'Enabled',
        type: 'text',
        editor: {
          type: 'custom',
          component: DropDownEditorComponent

        }
      },
      order_tab: {
        title: 'Order',
        type: 'text',
        editor: {
          type: 'custom',
          component: DropDownEditorComponent
        }
      },
      inward_tab: {
        title: 'Inward',
        type: 'text',
        editor: {
          type: 'custom',
          component: DropDownEditorComponent
        }
      },
      production_tab: {
        title: 'Production',
        type: 'text',
        editor: {
          type: 'custom',
          component: DropDownEditorComponent
        }
      },
      checking_tab: {
        title: 'Checking',
        type: 'text',
        editor: {
          type: 'custom',
          component: DropDownEditorComponent
        }
      },
      reports_tab: {
        title: 'Reports',
        type: 'text',
        editor: {
          type: 'custom',
          component: DropDownEditorComponent
        }
      },
      admin_tab: {
        title: 'Admin',
        type: 'text',
        editor: {
          type: 'custom',
          component: DropDownEditorComponent
        }
      },
      company_tab: {
        title: 'Company',
        type: 'text',
        editor: {
          type: 'custom',
          component: DropDownEditorComponent
        }
      },
      store_tab: {
        title: 'Store',
        type: 'text',
        editor: {
          type: 'custom',
          component: DropDownEditorComponent
        }
      }
    }
  }

}
