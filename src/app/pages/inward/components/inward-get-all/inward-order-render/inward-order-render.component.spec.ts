import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InwardOrderRenderComponent } from './inward-order-render.component';

describe('InwardOrderRenderComponent', () => {
  let component: InwardOrderRenderComponent;
  let fixture: ComponentFixture<InwardOrderRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InwardOrderRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InwardOrderRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
