import { Component, OnInit } from '@angular/core';
import { TagService } from '../../services/tag.service';
import { ErrorResponse, SuccessReponseWithObj, ResObj, SuccessResponse } from '../../../models/common.model';
import { LocalDataSource } from 'ng2-smart-table';
import { CONSTANT } from '../../../models/CONSTANTS';
import { ModalComponent } from '../../../services/modal/modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SlimBarService } from '../../../services/slim-bar.service';
import { InwardService } from '../../services/inward.service';
import { Inward, Tag } from '../../../models/inward.model';
import { Order } from '../../../models/order.model';
import { ActivatedRoute,Router } from '@angular/router';


@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.scss'],
  //providers:[TagService] provided in module
})
export class TagComponent implements OnInit {

  settings: any;
  columns: any;
  activeModal: any;
  source: LocalDataSource;
  tagList: Array<Tag>;
  orderSize: number = 100;
  inwardObj: Inward;
  inwardId: number = null;
  //expectedTags:number=0;
  //expectedClothes:number=0;
  constructor(private inwardService: InwardService, private activatedRoute: ActivatedRoute,private router:Router,
    private tagService: TagService, private slimBarService: SlimBarService, private modalService: NgbModal) {
    this.slimBarService.progress();
    this.columns = tagService.getTagColumns();
    this.settings = {
      actions: {
        add: true,
        edit: true,
        delete: true
      },
      hideSubHeader: false,
      add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
      edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
      delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
      columns: this.columns
    }
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((urlParams: any) => {
      this.inwardService.get(+urlParams.id).subscribe((res: SuccessReponseWithObj<Inward>) => {
        this.inwardId = +urlParams.id;
        this.inwardObj = res.obj;
        this.slimBarService.completeLoading();
        this.getAllTags(this.inwardId);
      },
        (err: ErrorResponse) => {
          this.Modal(err.header, err.description, true);
          this.slimBarService.completeLoading();
        }
      )
    })
  }



  onCreateConfirm(event) {
    this.create(event.newData, event);

  }


  onSaveConfirm(event) {
    this.update(event.newData, event);
  }

  onDeleteConfirm(event) {
    this.Modal(CONSTANT.WARNING,
      CONSTANT.DELETE_WARNING_DESC + CONSTANT.STRONG_TAG_OPEN + event.data.tagId + CONSTANT.STRONG_TAG_CLOSE + CONSTANT.QUESTION_MARK,
      false);
    // this is the event from modal
    this.activeModal.result.then((result: boolean) => {
      this.delete(event.data.id);
    });
  }

  //add/edit/delete/get tag
  private create(tag: Tag, event) {
    this.slimBarService.startLoading();
    tag.inward = this.inwardObj;
    this.tagService.add(tag)
      .subscribe((res: SuccessReponseWithObj<Inward>) => {
        this.slimBarService.completeLoading();
        this.getAllTags(this.inwardId);
        this.Modal(res.header, res.description, true);

        event.confirm.resolve();
      },
      (err: ErrorResponse) => {
        this.slimBarService.completeLoading();
        this.Modal(err.header, err.description, true);
      }
      )
  }

  private delete(id: number) {
    this.slimBarService.startLoading();
    this.tagService.delete(id).subscribe(
      (res: SuccessResponse) => {
        for (let i = 0; i < this.tagList.length; i++) {
          if (id == this.tagList[i].id) {
            this.tagList.splice(i, 1);
            break;
          }
        }
        this.source.load(this.tagList);
        this.slimBarService.completeLoading();
        this.Modal(res.header, res.description, true);
      },
      (err: ErrorResponse) => {
        this.slimBarService.completeLoading();
        this.Modal(err.header, err.description, true);
      }
    );
  }

  private update(tag: Tag, event) {
    this.slimBarService.startLoading();
    this.tagService.edit(tag, tag.id)
      .subscribe((res: SuccessReponseWithObj<Inward>) => {
        this.slimBarService.completeLoading();
        this.Modal(res.header, res.description, true);
        //console.log(res);
        event.confirm.resolve();
      },
      (err: ErrorResponse) => {
        //console.log(err);
        this.slimBarService.completeLoading();
        this.Modal(err.header, err.description, true);
      }
      );
  }

  private getAllTags(inwardId:number) {
    this.slimBarService.startLoading();
    this.tagService.getAllByInward(inwardId).subscribe(
      (res: ResObj<Tag>) => {
        this.tagList = res.obj;
        if(this.tagList.length>0){
         // this.setExpectations();
        }

        this.source = new LocalDataSource(this.tagList);
        this.slimBarService.completeLoading();
      },
      (err: ErrorResponse) => {
        this.Modal(err.header, err.description, true);
      }
    );
  }

  private Modal(header: String, description: String, isCloseModal: boolean) {
    this.activeModal = this.modalService.open(ModalComponent, { size: 'sm' });
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }

  back(){
    this.router.navigate([CONSTANT.BACK_TO_INWARD_TAGS]);
  }


}
