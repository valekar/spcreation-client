import { Injectable } from '@angular/core';
import { Response } from '@angular/http'
import { CustomHttp } from '../../../../../app-error-interceptor.service';
import { CommonService } from '../../../../services/common.service';
import { URLS } from '../../../../models/URLS';
import { ImageRenderComponent } from '../components/order-approvals/image-render/image-render.component';
import { CustomerRenderComponent } from '../components/order-approvals/customer-render/customer-render.component';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ApprovalsService {

  constructor(private customHttp: CustomHttp, private commonService: CommonService) { }

  getAll() {
    return this.customHttp.get(URLS.GET_APPROVAL_ORDERS_PAGE_URL, this.commonService.getHeaderOptions())
      .map((res: Response) => { return res.json() })
      .catch((err: Response) => { return Observable.throw(err.json()) });
  }

  approveOrder(id: number) {
    return this.customHttp.put(URLS.APPROVE_ORDER_URL + id, null, this.commonService.getHeaderOptions())
      .map((res: Response) => { return res.json() })
      .catch((err: Response) => { return Observable.throw(err.json()) });
  }

  rejectOrder(id: number) {
    return this.customHttp.put(URLS.REJECT_ORDER_URL + id, null, this.commonService.getHeaderOptions())
      .map((res: Response) => { return res.json() })
      .catch((err: Response) => { return Observable.throw(err.json()) });
  }

  pendingOrder(id: number) {
    return this.customHttp.put(URLS.PENDING_ORDER_URL + id, null, this.commonService.getHeaderOptions())
      .map((res: Response) => { return res.json() })
      .catch((err: Response) => { return Observable.throw(err.json()) });
  }

  getColumns() {
    return {
      spc: {
        title: "SPC #",
        type: "text",
        width: "10%"
      },
      imageBytes: {
        title: "Image",
        type: 'custom',
        renderComponent: ImageRenderComponent,
        filter: false,
        width: "8%"
      },
      customer: {
        title: "Customer Name",
        type: "custom",
        renderComponent: CustomerRenderComponent,
        width: "12%"
      },
      grade: {
        title: "Grade",
        type: "text",
        width: "8%"
      },
      designName: {
        title: "Design Name",
        type: "text",
        width: "10%"
      },
      quantity: {
        title: "Quantity",
        type: "text",
        width: "10%"
      },
      part:{
        title:"Part",
        type:"text",
        width:"10%"
      },
      position:{
       title:"Emd Position",
       type:"text",
       width:"10%"
     },
     measurement:{
       title:"Measurement",
       type:"text",
       width:"10%"
     },
      sam: {
        title: "Sample #",
        type: "text",
        width: "10%"
      },
      status: {
        title: "Status",
        type: "html",
        valuePrepareFunction(cell, row) {
          return `<div class="text-uppercase" ><i>${cell}</i></div>`;
        },
        width: "10%"
      },
      // Actions:{
      //   title: "Actions",
      //   type: "custom",
      //   renderComponent: ActionRenderComponent,
       
      //   filter: false
      // }
    }
  }

}
