import { Injectable } from '@angular/core';
import { Request, XHRBackend, RequestOptions, Response, Http, RequestOptionsArgs, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Router } from '@angular/router';

@Injectable()
class CustomHttp extends Http{
  constructor(backend: XHRBackend, defaultOptions: RequestOptions,private router: Router) {
    super(backend, defaultOptions);
    console.log("request intercepted");
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    return super.request(url, options).catch((error: Response) => {
        console.log("rrr");
            if ((error.status === 401 || error.status === 403) && (window.location.href.match(/\?/g) || []).length < 2) {
                console.log('The authentication session expires or the user is not authorised. Force refresh of the current page.');
                //window.location.href = window.location.href + '?' + new Date().getMilliseconds();
                this.router.navigate(['/']);
            }
            if((error.status == 500)){
              console.log(error);
            }
            return Observable.throw(error);
        });
  }
  
}

export {CustomHttp}
