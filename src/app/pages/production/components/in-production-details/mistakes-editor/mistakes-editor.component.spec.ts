import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MistakesEditorComponent } from './mistakes-editor.component';

describe('MistakesEditorComponent', () => {
  let component: MistakesEditorComponent;
  let fixture: ComponentFixture<MistakesEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MistakesEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MistakesEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
