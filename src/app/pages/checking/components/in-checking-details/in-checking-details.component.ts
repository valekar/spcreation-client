import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';
import {InCheckingService} from '../../services/in-checking.service';
import {SuccessReponseWithObj,ErrorResponse} from '../../../models/common.model';
import {Checking} from '../../../models/checking.model';
import {SlimBarService} from '../../../services/slim-bar.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { LocalDataSource } from 'ng2-smart-table';
import { CONSTANT } from '../../../models/CONSTANTS';
import { ModalComponent } from '../../../services/modal/modal.component';
import {Tag} from '../../../models/inward.model';
@Component({
  selector: 'app-in-checking-details',
  templateUrl: './in-checking-details.component.html',
  styleUrls: ['./in-checking-details.component.scss'],
  providers:[InCheckingService]
})
export class InCheckingDetailsComponent implements OnInit {

  inCheckingId:number;
  checkingData:Checking;
  columns:any;
  settings:any;
  activeModal:any;
  source:Array<LocalDataSource> = new Array();
  constructor(private activatedRoute:ActivatedRoute,private router:Router,private slimBarService: SlimBarService, 
    private modalService: NgbModal,private inCheckService:InCheckingService) { 
      this.slimBarService.progress();
      this.columns = inCheckService.getTagColumns();
      this.settings = {
        actions: {
          add: false,
          edit: true,
          delete: false
        },
        hideSubHeader: false,
        add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
        edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
        delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
        columns: this.columns
      }
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((urlParams)=>{
      this.inCheckingId = +urlParams.id;
      this.getInCheckingDetailPlan();
    })
  }

  getInCheckingDetailPlan(){
    this.inCheckService.getPlanById(this.inCheckingId).subscribe(
      (res:SuccessReponseWithObj<Checking>)=>{
        this.checkingData = res.obj;
        console.log(this.checkingData);
        let i=0;
        for(let employee of this.checkingData.employees){
          let tags:Array<Tag> = new Array();
          for(let tagForm of employee.assignedTags){
            tags.push(tagForm.tag);
          }
          this.source[i] = new LocalDataSource(tags);
        }
      });
  }

  private Modal(header: String, description: String, isCloseModal: boolean) {
    this.activeModal = this.modalService.open(ModalComponent, { size: 'sm' });
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }
  

  onSaveConfirm(event) {
    if (event.newData.checkingQuantityCompleted > event.newData.quantity) {
      this.Modal(CONSTANT.VALIDATION_ERROR, CONSTANT.COMPLETE_QUANTITY, true);

    }
    else {
      this.Modal(CONSTANT.WARNING,
        CONSTANT.UPDATE_DESC + CONSTANT.STRONG_TAG_OPEN + event.data.tagId + " tag with status " + event.newData.status
        + CONSTANT.STRONG_TAG_CLOSE + CONSTANT.QUESTION_MARK,
        false);
      // this is the event from modal
      this.activeModal.result.then((result: boolean) => {
        this.update(event.newData, event);
      });
    }
  }

  private update(tag: Tag, event) {
    this.slimBarService.startLoading();
    this.inCheckService.editTag(tag, tag.id)
      .subscribe((res: SuccessReponseWithObj<Tag>) => {
        this.slimBarService.completeLoading();
        this.Modal(res.header, res.description, true);
        console.log(res.obj);
        event.confirm.resolve();
      },
      (err: ErrorResponse) => {
        //console.log(err);
        this.slimBarService.completeLoading();
        this.Modal(err.header, err.description, true);
      }
      );
  }

  download(){
    this.inCheckService.downloadCheckPlanning(this.inCheckingId);
  }
}
