import { Injectable } from '@angular/core';
import { CustomHttp } from '../../../app-error-interceptor.service';
import { CommonService } from '../../services/common.service';
import { URLS } from '../../models/URLS';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import { ShowDetailRenderComponent } from '../components/in-checking/show-detail-render/show-detail-render.component';
import {TagNumberEditorComponent} from '../components/in-checking-details/tag-number-editor/tag-number-editor.component';
import {MistakesEditorComponent} from '../components/in-checking-details/mistakes-editor/mistakes-editor.component';
import {Tag } from '../../models/inward.model';
import {ExcelService} from '../../services/excel.service';

@Injectable()
export class InCheckingService {

  constructor(private http: CustomHttp, private commonService: CommonService,private excelService:ExcelService) { }

  getAll() {
    return this.http.get(URLS.CHECKING_URL, this.commonService.getHeaderOptions())
      .map((res: Response) => { return res.json(); })
      .catch((err: Response) => { return Observable.throw(err.json()) });
  }

  getPlanById(id:number) {
    return this.http.get(URLS.CHECKING_URL+id, this.commonService.getHeaderOptions())
      .map((res: Response) => { return res.json(); })
      .catch((err: Response) => { return Observable.throw(err.json()) });
  }

  editTag(tag:Tag,id:number){
    return this.http.put(URLS.TAG_URL+id,tag,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  downloadCheckPlanning(id:number){
    return this.http.get(URLS.CHECKING_PLANNING_DOWNLOAD_URL+id+"/download",this.commonService.getBlobOptions())
    .subscribe((res:any)=>{
      this.excelService.saveAsExcelFile(res._body,"Checking_Planning");
    });
  }

  getInCheckingColumns() {
    return {
      shift: {
        type: "text",
        title: "Shift"
      },
      planningDate: {
        type: "text",
        title: "Planning Date"
      },
      updated_by: {
        title: 'Updated By',
        type: 'text',
        editable: false
      },
      formatted_updated_at: {
        title: "Updated At",
        type: "text",
        editable: false,
      },
      showDetails: {
        title: "Details",
        type: "custom",
        renderComponent: ShowDetailRenderComponent
      }
    }
  }

  getTagColumns(){
    return {
      tagId:{
        type:"text",
        title:"Tag Id",
        editable:false
      },
      size:{
        type:"text",
        title:"Size",
        editable:false
      },
      bundle:{
        type:"text",
        title:"Bundle",
        editable:false
      },
      quantity:{
        type:"text",
        title:"Quantity",
        editable:false
      },
      quantityCompleted:{
        type:"text",
        title:"Produced Qty",
        width:"11%",
        editable:false
      },
      mistakes:{
        type:"text",
        title:"Prod Mistakes",
        width:"11%",
        editable:false
      },
      checkingQuantityCompleted:{
        type:"text",
        title:"Checking Qty",
        width:"11%",
        editor:{
          component:TagNumberEditorComponent,
          type:"custom"
        }
      },
      checkingMistakes:{
        type:"text",
        title:"Check Mistakes",
        width:"12%",
        editor:{
          type:"custom",
          component:MistakesEditorComponent
        }
      },
      status:{
        type:"text",
        title:"Status",
        width:"13%",
        editable:false
      }
    }
  }
}
