import { Injectable } from '@angular/core';
import {CONSTANT} from '../../models/CONSTANTS';
import {Response} from '@angular/http'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import {CommonService} from '../../services/common.service';
import {URLS} from '../../models/URLS';
import { Employee } from '../../models/company.model';
import {CustomHttp} from '../../../app-error-interceptor.service';

import {MultiSelectEditorComponent} from '../components/employee/multi-select-editor/multi-select-editor.component';
import {MultiSelectViewerComponent} from '../components/employee/multi-select-viewer/multi-select-viewer.component';
@Injectable()
export class EmployeeService {

  constructor(private http:CustomHttp,private commonService:CommonService) { }

  getAll(){
    return this.http.get(URLS.EMPLOYEE_URL, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  get(id:number){
    return this.http.get(URLS.EMPLOYEE_URL + id, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  add(employee:Employee){
    return this.http.post(URLS.EMPLOYEE_URL,employee,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  edit(employee:Employee,id:number){
    return this.http.put(URLS.EMPLOYEE_URL+id,employee,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  delete(id:number){
    return this.http.delete(URLS.EMPLOYEE_URL+id,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  getColumns(){
    return {
      name:{
        title:"Name",
        width:"10%",
        type:"text"
      },
      address:{
        title:"Address",
        width:"20%",
        editor:{
          type:"textarea"
        }
      },
      age:{
        title:"Age",
        type:"text"
      },
      sex:{
        title:"Sex",
        type:"text"
      },
      blood_group:{
        title:"Blood",
        type:"text"
      },
      departments:{
        title:"Departments",
        type:"custom",
        width:"20%",
        renderComponent:MultiSelectViewerComponent,
        editor:{
          type:"custom",
          component:MultiSelectEditorComponent
        }
      },
      updated_by:{
        title:'Updated By',
        width:"10%",
        type:'text',
        editable:false,
      },
      formatted_updated_at:{
        title :"Updated At",
        width:"10%",
        type:"text",
        editable:false,
      }

    }
  }

}
