import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagInwardLinkRenderComponent } from './tag-inward-link-render.component';

describe('TagInwardLinkRenderComponent', () => {
  let component: TagInwardLinkRenderComponent;
  let fixture: ComponentFixture<TagInwardLinkRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagInwardLinkRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagInwardLinkRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
