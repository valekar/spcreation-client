import { Component, OnInit } from '@angular/core';
import {ErrorResponse,SuccessReponseWithObj,ResObj,SuccessResponse} from '../../../models/common.model';
import {LocalDataSource } from 'ng2-smart-table';
import {CONSTANT} from '../../../models/CONSTANTS';
import {ModalComponent} from '../../../services/modal/modal.component';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {SlimBarService} from '../../../services/slim-bar.service';
import {CustomerService} from '../../services/customer.service';
import {Customer} from '../../../models/company.model';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
  providers:[CustomerService,SlimBarService]
})
export class CustomerComponent implements OnInit {

  settings:any;
  columns:any;
  activeModal:any;
  source:LocalDataSource;
  customerList:Array<Customer>;
  constructor(private customerService:CustomerService, private slimBarService:SlimBarService,private modalService:NgbModal) {
    this.slimBarService.progress();
    this.columns =customerService.getColumns();
    this.settings = {
      actions:{
        add:true,
        edit:true,
        delete:true
      },
      hideSubHeader: false,
      add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
      edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
      delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
      columns:this.columns
    }

   }
  ngOnInit() {
    this.getAllCustomers();
  }


  onCreateConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.NOTICE_CREATE, CONSTANT.CREATE_DESC,false);
    //this is the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.create(event.newData,event);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
  }


  onSaveConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.NOTICE_UPDATE, 
      CONSTANT.UPDATE_DESC + CONSTANT.STRONG_TAG_OPEN+ event.data.name + CONSTANT.STRONG_TAG_CLOSE + CONSTANT.QUESTION_MARK,
      false);
    //this the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.update(event.newData,event);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
  }

  onDeleteConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.WARNING, 
      CONSTANT.DELETE_WARNING_DESC + CONSTANT.STRONG_TAG_OPEN+ event.data.name + CONSTANT.STRONG_TAG_CLOSE+ CONSTANT.QUESTION_MARK,
      false);
    // this is the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.delete(event.data.id);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
    
  }

   //add/edit/delete/get rate
 private create(customer:Customer,event){
  this.slimBarService.startLoading();
 // console.log(user);
  this.customerService.add(customer)
  .subscribe((res:SuccessReponseWithObj<Customer>)=>{
    this.slimBarService.completeLoading();
    this.getAllCustomers();
    this.Modal(res.header,res.description,true);
    //console.log(res);
    event.confirm.resolve();
  },
  (err:ErrorResponse)=>{
    this.slimBarService.completeLoading();
    this.Modal(err.header,err.description,true);
  }
)
}

private delete(id:number){
  this.slimBarService.startLoading();
   this.customerService.delete(id).subscribe(
     (res:SuccessResponse)=>{
       
      for(let i=0;i<this.customerList.length;i++){
        if(id == this.customerList[i].id){
          this.customerList.splice(i,1);
          break;
        } 
      } 
      this.source.load(this.customerList);
      this.slimBarService.completeLoading(); 
      this.Modal(res.header,res.description,true);      
     },
     (err:ErrorResponse) => {
      this.slimBarService.completeLoading();
      this.Modal(err.header,err.description,true);
     }
    ); 
}

private update(customer:Customer,event){
  this.slimBarService.startLoading();
    this.customerService.edit(customer,customer.id)
    .subscribe((res:SuccessReponseWithObj<Customer>) => {
      this.slimBarService.completeLoading(); 
      this.Modal(res.header,res.description,true);
      console.log(res);
      event.confirm.resolve();
    },
    (err:ErrorResponse) => {
      //console.log(err);
      this.slimBarService.completeLoading();
      this.Modal(err.header,err.description,true);
     }
  ); 
}
private getAllCustomers(){
  this.slimBarService.startLoading();
  this.customerService.getAll().subscribe(
    (res:ResObj<Customer>)=>{
      this.customerList = res.obj;
      this.source = new LocalDataSource(this.customerList);
      this.slimBarService.completeLoading();
    },
    (err:ErrorResponse) => {}
  );
}

//end of add/edit/delete/get
private Modal(header:String,description:String,isCloseModal:boolean){
  this.activeModal = this.modalService.open(ModalComponent, {size: 'sm'});
  this.activeModal.componentInstance.modalHeader = header;
  this.activeModal.componentInstance.modalContent = description;
  this.activeModal.componentInstance.isCloseModal = isCloseModal;
}

}
