import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { ResetPassword } from './reset-password.component';
import { routing }       from './reset-password.routing';
import { SlimLoadingBarModule} from 'ng2-slim-loading-bar';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgaModule,
    routing,
     SlimLoadingBarModule.forRoot(),
  ],
  declarations: [
    ResetPassword
  ]
})
export class ResetPasswordModule {}
