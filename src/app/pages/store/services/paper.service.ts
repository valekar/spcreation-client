import { Injectable } from '@angular/core';
import {CONSTANT} from '../../models/CONSTANTS';
import {Response} from '@angular/http'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import {CommonService} from '../../services/common.service';
import {URLS} from '../../models/URLS';
import {Paper,PaperResObj} from '../../models/user.model';
import {CustomHttp} from '../../../app-error-interceptor.service';

@Injectable()
export class PaperService {

  constructor(private http:CustomHttp,private commonService:CommonService) { }


  getPapers(){
    return this.http.get(URLS.PAPER_URL, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  getPaper(id:number){
    return this.http.get(URLS.PAPER_URL + id, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  addPaper(paper:Paper){
    return this.http.post(URLS.PAPER_URL,paper,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  editPaper(paper:Paper,id:number){
    return this.http.put(URLS.PAPER_URL+id,paper,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  deletePaper(id:number){
    return this.http.delete(URLS.PAPER_URL+id,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  getColumns(){
    return {
      type:{
        title:"Type of Paper",
        type:"string"
      },
      quality:{
        title:"Quality",
        type:"string"
      },
      size:{
        title:"Size",
        type:"string"
      },
      updated_by:{
        title:'Updated By',
        type:'string',
        editable:false,
      },
      formatted_updated_at:{
        title :"Updated At",
        type:"string",
        editable:false,
      }
    }
  }

}
