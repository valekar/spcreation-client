import { Injectable } from '@angular/core';
import {Order} from '../../models/order.model';
@Injectable()
export class ShareOrderDataService {

  order:Order =null;
  constructor() { 
    //this.order = new Order();
    //this.order.designName = "XXXX";
  }


  getOrderData():Order{
    return this.order;
  }

  //this data is set in order-details 
  setOrderData(order:Order):void{
    this.order = order;
  }

}
