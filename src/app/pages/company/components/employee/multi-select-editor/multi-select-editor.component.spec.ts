import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiSelectEditorComponent } from './multi-select-editor.component';

describe('MultiSelectEditorComponent', () => {
  let component: MultiSelectEditorComponent;
  let fixture: ComponentFixture<MultiSelectEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiSelectEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiSelectEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
