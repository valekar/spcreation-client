import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../services/admin.service';
import { SuccessResponse, ModifiedUser, ErrorResponse, UserSuccessResponse } from '../../../models/user.model';
import { LocalDataSource } from 'ng2-smart-table';
import { CONSTANT } from '../../../models/CONSTANTS';
import { ModalComponent } from '../../../services/modal/modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SlimBarService } from '../../../services/slim-bar.service';
import 'rxjs/add/operator/catch';
import { DropDownViewComponent } from './drop-down-view/drop-down.component';
import { DropDownEditorComponent } from './drop-down-editor/drop-down-editor.component';

@Component({
  selector: 'app-user-permissions',
  templateUrl: './user-permissions.component.html',
  styleUrls: ['./user-permissions.component.scss']
})
export class UserPermissionsComponent implements OnInit {

  source: LocalDataSource;
  columns: any;
  settings: any;
  activeModal: any;
  modifiedUsersData: Array<ModifiedUser>;
  constructor(private adminService: AdminService, private modalService: NgbModal, private slimBarService: SlimBarService) {
    this.columns = adminService.getAdminColumns()
    this.settings = {
      actions: {
        add: true,
        edit: true,
        delete: true
      },
      hideSubHeader: false,
      add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
      edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
      delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
      columns: this.columns,
      pager:{
        perPage:8,
        display:true
      }
    };
  }

  ngOnInit() {
    this.getAllUsers();
  }

  onDeleteConfirm(event) {
    //console.log(event);
    this.Modal(CONSTANT.WARNING,
      CONSTANT.DELETE_WARNING_DESC + CONSTANT.STRONG_TAG_OPEN + event.data.username + CONSTANT.STRONG_TAG_CLOSE + CONSTANT.QUESTION_MARK,
      false);
    // this is the event from modal
    this.activeModal.result.then((result: boolean) => {
      this.deleteUser(event.data.id);
    },
      (reason: boolean) => {
        //console.log(reason)
      });

  }

  onSaveConfirm(event) {
    //console.log(event);
    this.Modal(CONSTANT.NOTICE_UPDATE,
      CONSTANT.UPDATE_DESC + CONSTANT.STRONG_TAG_OPEN + event.data.username + CONSTANT.STRONG_TAG_CLOSE + CONSTANT.QUESTION_MARK,
      false);
    //this the event from modal
    this.activeModal.result.then((result: boolean) => {
      this.updateUser(event.newData, event);
    },
      (reason: boolean) => {
        //console.log(reason)
      });
  }

  onCreateConfirm(event) {
    //console.log(event);
    this.Modal(CONSTANT.NOTICE_CREATE, CONSTANT.CREATE_DESC, false);
    //this is the event from modal
    this.activeModal.result.then((result: boolean) => {
      this.createUser(event.newData, event);
    },
      (reason: boolean) => {
        //console.log(reason)
      });
  }


  private Modal(header: String, description: String, isCloseModal: boolean) {
    this.activeModal = this.modalService.open(ModalComponent, { size: 'sm' });
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }

  private deleteUser(id: string) {
    this.slimBarService.startLoading();
    this.adminService.adminDeleteUser(id).subscribe(
      (res: SuccessResponse) => {

        for (let i = 0; i < this.modifiedUsersData.length; i++) {
          if (id == this.modifiedUsersData[i].id.toString()) {
            this.modifiedUsersData.splice(i, 1);
            break;
          }
        }
        this.source.load(this.modifiedUsersData);
        this.slimBarService.completeLoading();
        this.Modal(res.header, res.description, true);

      },
      (err: ErrorResponse) => {
        this.slimBarService.completeLoading();
        this.Modal(err.header, err.description, true);
      }
    );
  }

  private updateUser(user: ModifiedUser, event) {
    this.slimBarService.startLoading();
    console.log(user);
    this.adminService.adminUpdateUser(user)
      .subscribe((res: UserSuccessResponse) => {
        this.slimBarService.completeLoading();
        this.Modal(res.header, res.description, true);
        //console.log(res);
        event.confirm.resolve();
      },
      (err: ErrorResponse) => {
        //console.log(err);
        this.slimBarService.completeLoading();
        this.Modal(err.header, err.description, true);
      }
      );
  }

  private createUser(user: ModifiedUser, event) {
    this.slimBarService.startLoading();
    // console.log(user);
    this.adminService.adminCreateUser(user)
      .subscribe((res: UserSuccessResponse) => {
        this.slimBarService.completeLoading();
        this.getAllUsers();
        this.Modal(res.header, res.description, true);
        //console.log(res);
        event.confirm.resolve();
      },
      (err: ErrorResponse) => {
        this.slimBarService.completeLoading();
        this.Modal(err.header, err.description, true);
      }
      )
  }


  private getAllUsers() {
    this.adminService.adminGetUsers().subscribe(
      (res: Array<ModifiedUser>) => {
        console.log(res);
        this.modifiedUsersData = res;
        this.source = new LocalDataSource(this.modifiedUsersData);
        // console.log(this.source);
      },
      (err: ErrorResponse) => {
        console.log(err);
      }
    );
  }
}
