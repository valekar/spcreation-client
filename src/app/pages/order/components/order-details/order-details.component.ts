import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SlimBarService } from '../../../services/slim-bar.service';
import { Order } from '../../../models/order.model';
import { OrderService } from '../../services/order.service';
import { ResObj, SuccessReponseWithObj, SuccessResponse, ErrorResponse } from '../../../models/common.model';
import { ModalComponent } from '../../../services/modal/modal.component';
import { Router, ActivatedRoute } from '@angular/router';
import { CONSTANT } from '../../../models/CONSTANTS';
import { ShareOrderDataService } from '../../services/share-order-data.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss'],
  providers: [OrderService, SlimBarService]
})
export class OrderDetailsComponent implements OnInit {

  activeModal: any;
  order: Order;
  orderId: number;
  statusApproved:string = CONSTANT.STATUS_APPROVE;
  statusConfirmed:string = CONSTANT.STATUS_CONFIRMED;
  statusPending:string = CONSTANT.STATUS_PENDING;
  constructor(private orderService: OrderService, private slimBarService: SlimBarService, private modalService: NgbModal,
    private activatedRoute: ActivatedRoute, private router: Router, private sharedService: ShareOrderDataService) {
    this.slimBarService.progress();
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((urlParams: any) => {
      this.slimBarService.startLoading();
      this.orderId = +urlParams.id
      this.orderService.get(this.orderId).subscribe((res: SuccessReponseWithObj<Order>) => {
        this.order = res.obj;
        //console.log(this.order);
        this.slimBarService.completeLoading();
      },
        (err: ErrorResponse) => {
          this.slimBarService.completeLoading();
        });

    });
  }


  getAllOrders(id: number) {
    this.slimBarService.startLoading();
    this.orderService.get(id).subscribe((res: SuccessReponseWithObj<Order>) => {
      this.order = res.obj;
      //console.log(this.order);
      this.slimBarService.completeLoading();
    },
      (err: ErrorResponse) => {
        this.slimBarService.completeLoading();
      });
  }

  private Modal(header: String, description: String, isCloseModal: boolean) {
    this.activeModal = this.modalService.open(ModalComponent, { size: 'sm' });
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }

  back() {
    this.router.navigate([CONSTANT.BACK_TO_ORDERS]);
  }

  edit() {
    this.sharedService.setOrderData(this.order);
    this.router.navigate([CONSTANT.ORDERS_EDIT + this.orderId + "/edit"]);
  }

  delete() {
    this.Modal(CONSTANT.ARE_YOU_SURE, CONSTANT.DELETE_WARNING_DESC, false);
    //this the event from modal
    this.activeModal.result.then((result: boolean) => {
      this.slimBarService.startLoading();
      this.orderService.delete(this.order.id).subscribe((res: SuccessReponseWithObj<null>) => {
        this.router.navigate([CONSTANT.BACK_TO_ORDERS]);
        this.slimBarService.completeLoading();
      },
        (err: ErrorResponse) => { console.log(err) });


    },
      (reason: boolean) => {

      });
  }


}
