import {Customer}  from './company.model';
import {Thread,Applique,Paper} from './user.model';

export class Image{
    id:number;
    name:string;
}

export class Combo {
    id:number;
    cones:number;
    boxes:number;
    frameSize:number;
    layers:number;
    thread:Thread = null;
    applique:Applique = null;
    paper:Paper = null; 
}

export class Order{
    id:number;
    url:string;
    customer:Customer;
    grade:string;
    designName:string;
    embroideryType:string;
    duration:string;
    quantity:number;
    image:Image;
    fabricColor:string ;
    //fabricSize:string;
    fabric_size:string;
    noOfCombos:number;
    position:string;
    part:string;
    measurement:string;
    stitches:number;
    combos:Array<Combo>;
    SAM :string;
    SPC:string;
    spc :string;
    sam:string;
    imageBytes:any;
    status:string;
}
