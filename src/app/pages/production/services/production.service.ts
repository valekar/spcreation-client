import { Injectable } from '@angular/core';
import { CustomHttp } from '../../../app-error-interceptor.service';
import { CommonService } from '../../services/common.service';
import { URLS } from '../../models/URLS';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import { OrderRenderComponent } from '../components/confirm-order/order-render/order-render.component';
import { OrderStatusRenderComponent } from '../components/confirm-order/order-status-render/order-status-render.component';
import { StatusChangeRenderComponent } from '../components/confirm-order/status-change-render/status-change-render.component';
import { SPCRenderComponent } from '../components/confirm-order/spcrender/spcrender.component';
import { SlimBarService } from '../../services/slim-bar.service';
import { ModalComponent } from '../../services/modal/modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { FormGroup, FormArray, FormControl } from '@angular/forms';
import { PlanningForm, MachineForm, OrderForm } from '../../models/production.model';
import {CONSTANT} from '../../models/CONSTANTS';

@Injectable()
export class ProductionService {

  activeModal:any ;
  constructor(private http: CustomHttp, private commonService: CommonService,private slimBarService: SlimBarService, 
    private modalService: NgbModal) { }

  confirmOrder(inwardId: number) {
    return this.http.put(URLS.CONFIRM_ORDER + inwardId + "/confirm/", null, this.commonService.getHeaderOptions())
      .map((res: Response) => { return res.json(); })
      .catch((err: Response) => { return Observable.throw(err.json()) });
  }

  getAllInwards() {
    return this.http.get(URLS.INWARD_APPROVED_ORDERS_URL, this.commonService.getHeaderOptions())
      .map((res: Response) => { return res.json(); })
      .catch((err: Response) => { return Observable.throw(err.json()) });
  }

  getAllMachines() {
    return this.http.get(URLS.MACHINE_URL, this.commonService.getHeaderOptions())
      .map((res: Response) => { return res.json(); })
      .catch((err: Response) => { return Observable.throw(err.json()) });
  }

  getAllConfirmedOrderInwards() {
    return this.http.get(URLS.GET_CONFIRMED_ORDER_INWARDS, this.commonService.getHeaderOptions())
      .map((res: Response) => { return res.json(); })
      .catch((err: Response) => { return Observable.throw(err.json()) });
  }

  getAllEmployeesWithType(employeesType: string) {
    return this.http.get(URLS.PLANNING_EMPLOYEE_URL + employeesType, this.commonService.getHeaderOptions())
      .map((res: Response) => { return res.json(); })
      .catch((err: Response) => { return Observable.throw(err.json()) });
  }

  getAllDepartments() {
    return this.http.get(URLS.DEPARTMENT_URL, this.commonService.getHeaderOptions())
      .map((res: Response) => { return res.json(); })
      .catch((err: Response) => { return Observable.throw(err.json()) });
  }

  getAllContants() {
    return this.http.get(URLS.CONSTANT_URL, this.commonService.getHeaderOptions())
      .map((res: Response) => { return res.json(); })
      .catch((err: Response) => { return Observable.throw(err.json()) });
  }

  getAllEmployees() {
    return this.http.get(URLS.EMPLOYEE_URL, this.commonService.getHeaderOptions())
      .map((res: Response) => { return res.json(); })
      .catch((err: Response) => { return Observable.throw(err.json()) });
  }



  planningCalculations(planningForm: FormGroup) {
    let planningFormControl = planningForm.controls;
    let finalObj: PlanningForm = new PlanningForm();
    finalObj.shift = planningFormControl.shift.value;
    finalObj.planningDate = planningFormControl.planningDate.value;
    finalObj.machines = planningFormControl.machines.value;
    for (let i = 0; i < finalObj.machines.length; i++) {
      if (planningFormControl.machines['controls'][i]['controls'].orders.controls.length > 0) {
        for (let j = 0; j < planningFormControl.machines['controls'][i]['controls'].orders.controls.length; j++) {
          finalObj.machines[i].orders[j] = new OrderForm();
          finalObj.machines[i].orders[j] = planningFormControl.machines['controls'][i]['controls'].orders['controls'][j].value;
          //console.log(planningFormControl.machines['controls'][i]['controls'].orders['controls'][j]['controls'].tagsToBeProcessed.controls);
          //planningFormControl.machines['controls'][i]['controls'].orders['controls'][j]['controls'];
          //if(planningFormControl.machines['controls'][i]['controls'].orders['controls'][j]['controls'].tagsToBeProcessed.controls.length>0){
          // for(let k=0;k<planningFormControl.machines['controls'][i]['controls'].orders['controls'][j]['controls'].tagsToBeProcessed.controls.length;k++){
          //   finalObj.machines[i].orders[j].tagsToBeProcessed[k] =
          //   planningFormControl.machines['controls'][i]['controls'].orders['controls'][j]['controls'].tagsToBeProcessed.controls[k].controls.tagField.value;
          // }
          // }

        }
      }
    }
    //console.log(finalObj);
    //console.log(planningFormControl.machines['controls'][0]['controls'].orders.controls);
    if(this.finalObjValidation(finalObj)){
      return finalObj;
    }
    else{
      return null;
    }
  }

  finalObjValidation(finalObj:any){
    let valid = true;
    //console.log(finalObj);    
    //return valid;
    for(let machine of finalObj.machines){
      if(machine.noOfOrders!=""){
        if(machine.inwardField == ""){
          valid = false;
          this.Modal(CONSTANT.VALIDATION_ERROR, CONSTANT.SELECT_ORDERS,true);
          break;
        }
        else{
          for(let order of machine.orders){
            if(order.tagsClothesQuantity=="") {
              valid = false;
              this.Modal(CONSTANT.VALIDATION_ERROR, CONSTANT.DRAG_TAGS,true);
              break;
            }
            if( order.operatorField == ""){
              this.Modal(CONSTANT.VALIDATION_ERROR, CONSTANT.MUST_SELECT_OPERATOR,true);
              valid = false
              break;
            }
            if(order.framerField == ""){
              valid = false;
              this.Modal(CONSTANT.VALIDATION_ERROR, CONSTANT.MUST_SELECT_FRAMER,true);
              break;
            }
            if(order.inchargeField==""){
              valid = false;
              this.Modal(CONSTANT.VALIDATION_ERROR, CONSTANT.MUST_SELECT_INCHARGE,true);
              break;
            }
          }
        }
      }
    }
    return valid;
  }

  private Modal(header: String, description: String, isCloseModal: boolean) {
    this.activeModal = this.modalService.open(ModalComponent, { size: 'lg' });
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }


  postProductionPlanning(planningForm: PlanningForm) {
    //console.log(planningForm);
    return this.http.post(URLS.PRODUCTION_PLANNING_SET_URL, planningForm, this.commonService.getHeaderOptions())
      .map((res: Response) => { return res.json(); })
      .catch((err: Response) => { return Observable.throw(err.json()) });
  }

  // getAll(){
  //   return this.http.get(URLS.PRODUCTION_URL,this.commonService.getHeaderOptions())
  //   .map((res: Response) => { return res.json(); })
  //   .catch((err: Response) => { return Observable.throw(err.json()) });
  // }

  getColumns() {
    return {
      SPC: {
        title: "SPC #",
        type: "custom",
        width: "8%",
        renderComponent: SPCRenderComponent
      },
      inwardNumber: {
        title: "Inward #",
        type: "text",
        width: "8%"

      },
      layStr: {
        title: "Lay",
        type: "text",
        width: "12%",

      },
      order: {
        title: "Order",
        type: "custom",
        width: "30%",
        renderComponent: OrderRenderComponent,
        filter: false
      },
      status: {
        title: "Status",
        type: "custom",
        renderComponent: OrderStatusRenderComponent,
        filter: false
      },
      updated_by: {
        title: 'Updated By',
        type: 'text',
        width: "10%",
        editable: false
      },
      formatted_updated_at: {
        title: "Updated At",
        type: "text",
        width: "10%",
        editable: false,
      }
    }
  }

}
