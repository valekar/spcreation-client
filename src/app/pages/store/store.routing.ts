import { Routes, RouterModule }  from '@angular/router';
import {StoreComponent} from './store.component'
import {AppliqueComponent} from './components/applique/applique.component';
import {PaperComponent} from './components/paper/paper.component';
import {MachineComponent} from './components/machine/machine.component';
import {ThreadComponent} from './components/thread/thread.component';

const routes: Routes = [
    {
      path: '',
      component:   StoreComponent,
       children: [
         { path: 'appliques', component: AppliqueComponent },
         { path: 'papers', component: PaperComponent },
         { path: 'machines', component: MachineComponent },
         { path: 'threads', component: ThreadComponent },
      ]
    }
  ];

export const routing = RouterModule.forChild(routes);