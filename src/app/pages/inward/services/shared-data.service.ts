import { Injectable } from '@angular/core';

@Injectable()
export class SharedDataService {

  sharedOrderSize:number;
  constructor() { }

  getOrderSize(){
    return this.sharedOrderSize;
  }

  setOrderSize(orderSize:number){
    this.sharedOrderSize = orderSize;
  }

}
