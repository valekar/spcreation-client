import { Component, OnInit } from '@angular/core';
import { Cell, DefaultEditor,Editor } from 'ng2-smart-table';

@Component({
  selector: 'app-date-time-editor',
  templateUrl: './date-time-editor.component.html',
  styleUrls: ['./date-time-editor.component.scss']
})
export class DateTimeEditorComponent extends DefaultEditor implements OnInit {

  selectedCellDate:string;
  constructor() {
    super();
   }

  ngOnInit() {
    //console.log(this.cell.getValue());
    if(this.cell.getValue()==""||this.cell.getValue()==undefined||this.cell.getValue()==null){
      //this.selectedCellDate = this.getCurrentTime();
      this.cell.newValue = this.selectedCellDate;
      //console.log(this.cell.newValue);
    }
    else{
      this.selectedCellDate = this.cell.getValue();
    }
  }

  dateChanged(){
    this.cell.newValue = this.selectedCellDate;
  }

  getCurrentTime(){
    let d = new Date();
    let currentTime = "";
    currentTime = ("00" + (d.getMonth() + 1)).slice(-2) + "/" + 
    ("00" + d.getDate()).slice(-2) + "/" + 
    d.getFullYear() + " " + 
    ("00" + d.getHours()).slice(-2) + ":" + 
    ("00" + d.getMinutes()).slice(-2) + ":" + 
    ("00" + d.getSeconds()).slice(-2)
    return currentTime;
  }

}
