import { Component, OnInit,Input } from '@angular/core';
import {Department,Employee} from '../../../../models/company.model';

@Component({
  selector: 'app-multi-select-viewer',
  templateUrl: './multi-select-viewer.component.html',
  styleUrls: ['./multi-select-viewer.component.scss']
})
export class MultiSelectViewerComponent implements OnInit {

  constructor() { }
  ngOnInit() {

  }

  @Input() rowData: Employee;

}
