import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderRenderComponent } from './order-render.component';

describe('OrderRenderComponent', () => {
  let component: OrderRenderComponent;
  let fixture: ComponentFixture<OrderRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
