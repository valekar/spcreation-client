import { Injectable } from '@angular/core';
import { CustomHttp } from '../../../app-error-interceptor.service';
import { CommonService } from '../../services/common.service';
import { URLS } from '../../models/URLS';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import {ShowDetailRenderComponent} from '../components/in-production/show-detail-render/show-detail-render.component';
import {TagNumberEditorComponent} from '../components/in-production-details/tag-number-editor/tag-number-editor.component';
import {Tag} from '../../models/inward.model';
import {MistakesEditorComponent} from '../components/in-production-details/mistakes-editor/mistakes-editor.component';
import {ExcelService} from '../../services/excel.service';

@Injectable()
export class InProductionService {

  constructor(private http: CustomHttp, private commonService: CommonService,private excelService:ExcelService) { }

  getAll(){
    return this.http.get(URLS.PRODUCTION_URL,this.commonService.getHeaderOptions())
    .map((res: Response) => { return res.json(); })
    .catch((err: Response) => { return Observable.throw(err.json()) });
  }

  get(id:number){
    return this.http.get(URLS.PRODUCTION_URL+id,this.commonService.getHeaderOptions())
    .map((res: Response) => { return res.json(); })
    .catch((err: Response) => { return Observable.throw(err.json()) });
  }

  editTag(tag:Tag,id:number){
    return this.http.put(URLS.TAG_URL+id,tag,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  download(id:number){
    return this.http.get(URLS.PRODUCTION_PLANNING_DOWNLOAD_URL+id+"/download.xls",this.commonService.getBlobOptions()).subscribe((res:any)=>{
      //console.log(res._body);
      //return new Blob([res._body],{ type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' })
      this.excelService.saveAsExcelFile(res._body,"Production_Planning");
    });
  }


  getColumns(){
    return {
      shift:{
        type:"text",
        title:"Shift"
      },
      planningDate:{
        type:"text",
        title:"Date"
      },
      updated_by: {
        title: 'Updated By',
        type: 'text',
        editable: false
      },
      formatted_updated_at: {
        title: "Updated At",
        type: "text",
        editable: false,
      },
      showDetails:{
        title:"Details",
        type:"custom",
        renderComponent:ShowDetailRenderComponent
      }
    }
  }

  getTagColumns(){
    return {
      tagId:{
        type:"text",
        title:"Tag Id",
        editable:false
      },
      size:{
        type:"text",
        title:"Size",
        editable:false
      },
      bundle:{
        type:"text",
        title:"Bundle",
        editable:false
      },
      quantity:{
        type:"text",
        title:"Quantity",
        editable:false
      },
      quantityCompleted:{
        type:"text",
        title:"Quantity Completed",
        editor:{
          component:TagNumberEditorComponent,
          type:"custom"
        }
      },
      mistakes:{
        type:"text",
        title:"Mistakes",
        editor:{
          type:"custom",
          component:MistakesEditorComponent
        }
      },
      status:{
        type:"text",
        title:"Status",
        editable:false
      }
    }
  }
}
