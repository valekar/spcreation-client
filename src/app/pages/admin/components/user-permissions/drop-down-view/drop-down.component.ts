import { Component, OnInit,Input, Output,EventEmitter } from '@angular/core';
import {ViewCell} from 'ng2-smart-table';
import {CONSTANT} from '../../../../models/CONSTANTS';

@Component({
  selector: 'app-drop-down',
  templateUrl: './drop-down.component.html',
  styleUrls: ['./drop-down.component.scss']
})
export class DropDownViewComponent implements ViewCell, OnInit {

  renderValue:string;
  
  @Input() value : string|number
  @Input() rowData:any;
  @Input() title:any;

  @Output() save : EventEmitter<any> = new EventEmitter();

  constructor() { 
    
  }

  ngOnInit() {
    this.renderValue = this.value.toString();
    //console.log(this.rowData);
  }

  onClick(){
    //console.log("Hello");
    //this.save.emit("as");
  }

}
