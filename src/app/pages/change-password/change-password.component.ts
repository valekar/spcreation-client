import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user.service';
import {CommonService} from '../services/common.service';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {EmailValidator, EqualPasswordsValidator} from '../../theme/validators';
import {User,SuccessResponse,ChangePasswordToken} from '../models/user.model';
import {Router,ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
  providers:[UserService,CommonService]
})
export class ChangePasswordComponent implements OnInit {
  public form:FormGroup;
  public password:AbstractControl;
  public repeatPassword:AbstractControl;
  public passwords:FormGroup;
  public isValidToken:boolean;
  public sub:Subscription;
  public token:string;
  constructor(private userService:UserService,fb:FormBuilder,
    private router:Router,private activatedRoute:ActivatedRoute) {

    this.form = fb.group({
      'passwords': fb.group({
      'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'repeatPassword': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
      }, {validator: EqualPasswordsValidator.validate('password', 'repeatPassword')})
    });


    this.passwords = <FormGroup> this.form.controls['passwords'];
    this.password = this.passwords.controls['password'];
    this.repeatPassword = this.passwords.controls['repeatPassword'];
  }

  ngOnInit() {
     this.sub = this.activatedRoute.data
    .subscribe((res) => {
      let successResponse: SuccessResponse = res.data;
      //console.log(successResponse.header)
      if(successResponse.header == "Valid Token"){
        this.isValidToken = true;
        this.token = successResponse.token;
      }
      else {
        this.isValidToken = false;
      }
    });
    
  }

  public onSubmit(values:Object):void {
    let changePassword = new ChangePasswordToken();
    changePassword.token = this.token;
    changePassword.repeatPassword = values["passwords"]["repeatPassword"];
    changePassword.password = values["passwords"]["password"];
    if (this.form.valid) {
      console.log(changePassword);
      this.userService.changePassword(changePassword);
    }
  }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }

}
