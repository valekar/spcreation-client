import { Component, OnInit } from '@angular/core';
import { TagService } from '../../services/tag.service';
import { ErrorResponse, SuccessReponseWithObj, ResObj, SuccessResponse } from '../../../models/common.model';
import { LocalDataSource } from 'ng2-smart-table';
import { CONSTANT } from '../../../models/CONSTANTS';
import { ModalComponent } from '../../../services/modal/modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SlimBarService } from '../../../services/slim-bar.service';
import { InwardService } from '../../services/inward.service';
import { Inward } from '../../../models/inward.model';
import { Order } from '../../../models/order.model';

@Component({
  selector: 'app-tag-inward',
  templateUrl: './tag-inward.component.html',
  styleUrls: ['./tag-inward.component.scss'],
 // providers:[TagService] provided in module
})
export class TagInwardComponent implements OnInit {
  settings: any;
  columns: any;
  activeModal: any;
  source: LocalDataSource;
  inwardList: Array<Inward>;
  orderSize: number = 100;

  constructor(private inwardService: InwardService, private tagService:TagService,private slimBarService: SlimBarService, private modalService: NgbModal) {
    this.slimBarService.progress();
    this.columns = tagService.getInwardColumns();
    this.settings = {
      actions: {
        add: false,
        edit: false,
        delete: false
      },
      hideSubHeader: false,
      add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
      edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
      delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
      columns: this.columns
    }
  }

  ngOnInit() {
    this.getAllInwards();
  }

  private getAllInwards() {
    this.slimBarService.startLoading();
    this.inwardService.getAll().subscribe(
      (res: ResObj<Inward>) => {
        this.inwardList = res.obj;
        this.source = new LocalDataSource(this.inwardList);
        this.slimBarService.completeLoading();
      },
      (err: ErrorResponse) => { }
    );
  }

}
