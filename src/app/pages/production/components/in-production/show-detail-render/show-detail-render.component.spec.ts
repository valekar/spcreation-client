import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowDetailRenderComponent } from './show-detail-render.component';

describe('ShowDetailRenderComponent', () => {
  let component: ShowDetailRenderComponent;
  let fixture: ComponentFixture<ShowDetailRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowDetailRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowDetailRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
