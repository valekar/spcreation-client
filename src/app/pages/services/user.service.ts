import { Injectable ,Inject} from '@angular/core';
import { Http,Response } from '@angular/http';
import {CommonService } from '../services/common.service';
import {User,ResetPasswordModel, SuccessResponse,ErrorResponse,ChangePasswordToken} from '../models/user.model';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {SlimBarService} from '../services/slim-bar.service';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {ModalComponent} from '../services/modal/modal.component';
import {Router } from '@angular/router';
import 'rxjs/add/operator/map';
import {URLS} from '../models/URLS';

import 'rxjs/add/operator/toPromise';
 
@Injectable()
export class UserService {

    //private subject : Subject<any>;
    //resetPassword$: Observable<any>;

    constructor(private http:Http,private commonService : CommonService,
        private slimBarService : SlimBarService,private modalService: NgbModal,private router :Router){
        this.slimBarService.progress();
        // this.subject = new BehaviorSubject<any>(null);
         //this.resetPassword$ = this.subject.asObservable();
    }
    
    saveUser(object:User){
        this.slimBarService.startLoading();
        return this.http.post(URLS.USER_REGISTER_URL,object, this.commonService.getHeaderOptionsWithoutAuth())
        .map((res:Response):any => 
            {
                this.slimBarService.completeLoading();
                //console.log(res.json()); 
                return res;
            }); 
    }

    validateToken(token:ChangePasswordToken){
        this.slimBarService.startLoading();
        /*let validatePromise = Promise.resolve(
            this.http.get('/api/users/change-password/'+token,this.commonService.getHeaderOptionsWithoutAuth())
        );*/
        
           let myPromise =  this.http.post(URLS.CHANGE_PASSWORD_VALIDATE_URL,token,this.commonService.getHeaderOptionsWithoutAuth())
            .map((res) => {
                  //console.log(res);
                  return res.json();  
            }).toPromise();

        return Promise.resolve(myPromise).then((res) => {return res;});
    }

    resetPassword(obj : ResetPasswordModel){
        this.slimBarService.startLoading();
        return this.http.post(URLS.USER_PASSOWRD_RESET_URL,obj,this.commonService.getHeaderOptionsWithoutAuth())
        .map((res:Response) => {return res;})
        .subscribe(
            (res:Response) => { 
                let r:SuccessResponse = res.json();
                this.slimBarService.completeLoading(); 
                this.Modal(r.header,r.description);   
            },
            (err:Response) => {
                let e:ErrorResponse = err.json();
                //console.log(err);
                this.slimBarService.completeLoading();
                this.Modal(e.header,e.description);
            } 
        );
    }

      changePassword(object:ChangePasswordToken){
        this.slimBarService.startLoading();
        return this.http.post(URLS.USER_CHANGE_PASSWORD_URL,object, this.commonService.getHeaderOptionsWithoutAuth())
        .map((res:Response):any => 
            {
                this.slimBarService.completeLoading();
                //console.log(res.json()); 
                return res;
            })
        .subscribe(
            (res:Response) => { 
                let r:SuccessResponse = res.json();
                this.slimBarService.completeLoading(); 
                this.Modal(r.header,r.description); 
                this.router.navigate(['/']);  
            },
            (err:Response) => {
                let e:ErrorResponse = err.json();
                //console.log(err);
                this.slimBarService.completeLoading();
                this.Modal(e.header,e.description);
                this.router.navigate(['/']);
            } 
        );
    }

    private Modal(header:String,description:String){
    const activeModal = this.modalService.open(ModalComponent, {size: 'lg'});
          activeModal.componentInstance.modalHeader = header;
          activeModal.componentInstance.modalContent = description;
    }


}