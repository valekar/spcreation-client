import { Component, OnInit } from '@angular/core';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {CONSTANT} from '../../models/CONSTANTS';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  modalHeader: string;
  modalContent: string = "";
  isCloseModal:boolean = true;
  confirmationResult:boolean = false;
  myModal:any;
  constructor(private activeModal: NgbActiveModal,private modalService:NgbModal) {
  }

  ngOnInit() {}

  closeModal() {
    this.activeModal.close();
  }

  confirmYes(){
    this.confirmationResult = true;
    this.activeModal.close(CONSTANT.CONFIRM_YES);
  }

  confirmNo(){
    this.activeModal.dismiss(CONSTANT.CONFIRM_NO);
  }


  // ModalMethod(header:string,description,isCloseModal){
  //   this.myModal = this.modalService.open(ModalComponent, { size: 'sm' });
  //   this.myModal.componentInstance.modalHeader = header;
  //   this.myModal.componentInstance.modalContent = description;
  //   this.myModal.componentInstance.isCloseModal = isCloseModal;
  // }


}

