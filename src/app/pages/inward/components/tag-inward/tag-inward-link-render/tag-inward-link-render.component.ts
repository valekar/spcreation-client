import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-tag-inward-link-render',
  templateUrl: './tag-inward-link-render.component.html',
  styleUrls: ['./tag-inward-link-render.component.scss']
})
export class TagInwardLinkRenderComponent implements OnInit {

  @Input() rowData;
  constructor() { }

  ngOnInit() {
  }

}
