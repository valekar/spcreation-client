import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropDownEditorComponent } from './drop-down-editor.component';

describe('DropDownEditorComponent', () => {
  let component: DropDownEditorComponent;
  let fixture: ComponentFixture<DropDownEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropDownEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropDownEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
