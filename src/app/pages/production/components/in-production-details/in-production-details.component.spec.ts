import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InProductionDetailsComponent } from './in-production-details.component';

describe('InProductionDetailsComponent', () => {
  let component: InProductionDetailsComponent;
  let fixture: ComponentFixture<InProductionDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InProductionDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InProductionDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
