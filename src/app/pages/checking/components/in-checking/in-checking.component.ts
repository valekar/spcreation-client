import { Component, OnInit } from '@angular/core';
import { ModalComponent } from '../../../services/modal/modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SlimBarService } from '../../../services/slim-bar.service';
import { ErrorResponse, SuccessReponseWithObj, ResObj, SuccessResponse } from '../../../models/common.model';
import { LocalDataSource } from 'ng2-smart-table';
import { CONSTANT } from '../../../models/CONSTANTS';
import { InCheckingService } from '../../services/in-checking.service';
import { Checking } from '../../../models/checking.model';

@Component({
  selector: 'app-in-checking',
  templateUrl: './in-checking.component.html',
  styleUrls: ['./in-checking.component.scss'],
  providers: [InCheckingService]
})
export class InCheckingComponent implements OnInit {

  activeModal: any;
  source: LocalDataSource;
  inCheckList: Array<Checking>;
  settings: any;
  columns: any;
  constructor(private inCheckingService: InCheckingService, private slimBarService: SlimBarService,
    private modalService: NgbModal) {
    this.slimBarService.progress();
    this.columns = inCheckingService.getInCheckingColumns();
    this.settings = {
      actions: {
        add: false,
        edit: false,
        delete: false
      },
      hideSubHeader: false,
      add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
      edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
      delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
      columns: this.columns
    }
  }

  ngOnInit() {
    this.getAllInCheckingPlans();
  }

  getAllInCheckingPlans() {
    this.slimBarService.startLoading();
    this.inCheckingService.getAll().subscribe((res: ResObj<Checking>) => {
      this.inCheckList = res.obj;
      this.source = new LocalDataSource(this.inCheckList);
      this.slimBarService.completeLoading();
    }, (err: ErrorResponse) => {
      this.slimBarService.completeLoading();
    });
  }

}
