import { Component, OnInit } from '@angular/core';
import { UserService} from '../services/user.service';
import {CommonService} from '../services/common.service';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { EmailValidator } from '../../theme/validators';
import {ResetPasswordModel} from '../models/user.model';
import {SlimBarService} from '../services/slim-bar.service';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {ModalComponent} from '../services/modal/modal.component';
import {SuccessResponse,ErrorResponse} from '../models/user.model';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  providers:[UserService,CommonService,SlimBarService]
})
export class ResetPassword implements OnInit {
  public form:FormGroup;
  public email:AbstractControl;

  constructor(private userService:UserService,fb:FormBuilder,private slimBarService : SlimBarService,private modalService: NgbModal) {
    this.form = fb.group({
      'email':['',Validators.compose([Validators.required, EmailValidator.validate])]
    });

    this.email = this.form.controls['email'];
   }

  ngOnInit() {
  }

  public onSubmit(obj:Object):void {
    
    if (this.form.valid) {
      let resetPassword = new ResetPasswordModel();
      resetPassword.email = obj["email"];  
      //this.userService.resetPassword(resetPassword);

      this.userService.resetPassword(resetPassword);
    }
  }



}
