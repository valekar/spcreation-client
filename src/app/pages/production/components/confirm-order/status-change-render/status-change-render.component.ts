import { Component, OnInit,Input,EventEmitter } from '@angular/core';
import {CONSTANT} from '../../../../models/CONSTANTS';
import {Inward} from '../../../../models/inward.model';


@Component({
  selector: 'app-status-change-render',
  templateUrl: './status-change-render.component.html',
  styleUrls: ['./status-change-render.component.scss']
})
export class StatusChangeRenderComponent implements OnInit {


  @Input() rowData:Inward;
  confirmed:String;
  approved:String;
  actionEmitter:EventEmitter<any> = new EventEmitter();
  constructor() {
    this.confirmed = CONSTANT.STATUS_CONFIRMED;
    this.approved = CONSTANT.STATUS_APPROVE;
   }

  ngOnInit() {
  }

  confirmOrder(){
    this.actionEmitter.emit(CONSTANT.STATUS_CONFIRMED);
  }
}
