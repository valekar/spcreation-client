import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderApprovalsComponent } from './order-approvals.component';

describe('OrderApprovalsComponent', () => {
  let component: OrderApprovalsComponent;
  let fixture: ComponentFixture<OrderApprovalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderApprovalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderApprovalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
