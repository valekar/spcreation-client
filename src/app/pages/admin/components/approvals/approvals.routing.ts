import { Routes, RouterModule }  from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { ApprovalsComponent } from './approvals.component';
import {OrderApprovalsComponent} from './components/order-approvals/order-approvals.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: ApprovalsComponent,
    children:[
      {path:"order-approvals",component:OrderApprovalsComponent},
      
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
