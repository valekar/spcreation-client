import { Component, OnInit } from '@angular/core';
import {ErrorResponse,SuccessReponseWithObj,ResObj,SuccessResponse} from '../../../models/common.model';
import {LocalDataSource } from 'ng2-smart-table';
import {CONSTANT} from '../../../models/CONSTANTS';
import {ModalComponent} from '../../../services/modal/modal.component';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {SlimBarService} from '../../../services/slim-bar.service';
import {RateService} from '../../services/rate.service';
import {Rate} from '../../../models/company.model';

@Component({
  selector: 'app-rate',
  templateUrl: './rate.component.html',
  styleUrls: ['./rate.component.scss'],
  providers:[RateService,SlimBarService]
})
export class RateComponent implements OnInit {

  settings:any;
  columns:any;
  activeModal:any;
  source:LocalDataSource;
  rateList:Array<Rate>;
  constructor(private rateService:RateService, private slimBarService:SlimBarService,private modalService:NgbModal) {
    this.slimBarService.progress();
    this.columns =rateService.getColumns();
    this.settings = {
      actions:{
        add:true,
        edit:true,
        delete:true
      },
      hideSubHeader: false,
      add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
      edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
      delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
      columns:this.columns
    }

   }
  ngOnInit() {
    this.getAllRates();
  }

  onCreateConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.NOTICE_CREATE, CONSTANT.CREATE_DESC,false);
    //this is the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.create(event.newData,event);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
  }


  onSaveConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.NOTICE_UPDATE, 
      CONSTANT.UPDATE_DESC + CONSTANT.STRONG_TAG_OPEN+ event.data.stitches + CONSTANT.STRONG_TAG_CLOSE + CONSTANT.QUESTION_MARK,
      false);
    //this the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.update(event.newData,event);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
  }

  onDeleteConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.WARNING, 
      CONSTANT.DELETE_WARNING_DESC + CONSTANT.STRONG_TAG_OPEN+ event.data.stitches + CONSTANT.STRONG_TAG_CLOSE+ CONSTANT.QUESTION_MARK,
      false);
    // this is the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.delete(event.data.id);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
    
  }

   //add/edit/delete/get rate
 private create(rate:Rate,event){
  //validation
  let validate = this.validation(rate);
  if(validate){
    this.slimBarService.startLoading();
    this.rateService.add(rate)
    .subscribe((res:SuccessReponseWithObj<Rate>)=>{
      this.slimBarService.completeLoading();
      this.getAllRates();
      this.Modal(res.header,res.description,true);
      //console.log(res);
      event.confirm.resolve();
    },
    (err:ErrorResponse)=>{
      this.slimBarService.completeLoading();
      this.Modal(err.header,err.description,true);
    }
    );

  }
  else{
    return;
      }
  
}

private delete(id:number){
  this.slimBarService.startLoading();
   this.rateService.delete(id).subscribe(
     (res:SuccessResponse)=>{
       
      for(let i=0;i<this.rateList.length;i++){
        if(id == this.rateList[i].id){
          this.rateList.splice(i,1);
          break;
        } 
      } 
      this.source.load(this.rateList);
      this.slimBarService.completeLoading(); 
      this.Modal(res.header,res.description,true);      
     },
     (err:ErrorResponse) => {
      this.slimBarService.completeLoading();
      this.Modal(err.header,err.description,true);
     }
    ); 
}

private update(rate:Rate,event){
  let validate = this.validation(rate);
  if(validate){
    this.slimBarService.startLoading();
    this.rateService.edit(rate,rate.id)
    .subscribe((res:SuccessReponseWithObj<Rate>) => {
      this.slimBarService.completeLoading(); 
      this.Modal(res.header,res.description,true);
      console.log(res);
      event.confirm.resolve();
    },
    (err:ErrorResponse) => {
      //console.log(err);
      this.slimBarService.completeLoading();
      this.Modal(err.header,err.description,true);
     }
  );
  }
   
}
private getAllRates(){
  this.slimBarService.startLoading();
  this.rateService.getAll().subscribe(
    (res:ResObj<Rate>)=>{
      this.rateList = res.obj;
      this.source = new LocalDataSource(this.rateList);
      this.slimBarService.completeLoading();
    },
    (err:ErrorResponse) => {}
  );
}

//end of add/edit/delete/get
private Modal(header:String,description:String,isCloseModal:boolean){
  this.activeModal = this.modalService.open(ModalComponent, {size: 'sm'});
  this.activeModal.componentInstance.modalHeader = header;
  this.activeModal.componentInstance.modalContent = description;
  this.activeModal.componentInstance.isCloseModal = isCloseModal;
}

private validation(rate:Rate){
  if(isNaN(+rate.rate) || isNaN(+rate.stitches)){
    this.Modal(CONSTANT.VALIDATION_ERROR, CONSTANT.RATE_VALIDATION_MESSAGE,true);
    return false;
  }
  return true;
}


}
