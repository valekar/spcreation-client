import { Component } from '@angular/core';

import { GlobalState } from '../../../global.state';
import {AuthenticationService} from '../../../services/authentication.service';

@Component({
  selector: 'ba-page-top',
  templateUrl: './baPageTop.html',
  styleUrls: ['./baPageTop.scss' ]
})
export class BaPageTop {

  isScrolled: boolean = false;
  isMenuCollapsed: boolean = false;

  constructor(private _state: GlobalState, private _authService:AuthenticationService) {
    this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
      this.isMenuCollapsed = isCollapsed;
    });
  }

  toggleMenu() {
    this.isMenuCollapsed = !this.isMenuCollapsed;
    this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);
    return false;
  }

   scrolledChanged(isScrolled) {
    this.isScrolled = isScrolled;
  }

  // private logout(){
  //   this._authService.logout();
  // }
}
