import { Component, OnInit,Input } from '@angular/core';
import {Order} from '../../../../models/order.model';

@Component({
  selector: 'app-order-status-render',
  templateUrl: './order-status-render.component.html',
  styleUrls: ['./order-status-render.component.scss']
})
export class OrderStatusRenderComponent implements OnInit {

  @Input() rowData;
  constructor() { }

  ngOnInit() {
  }

}
