import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InCheckingDetailsComponent } from './in-checking-details.component';

describe('InCheckingDetailsComponent', () => {
  let component: InCheckingDetailsComponent;
  let fixture: ComponentFixture<InCheckingDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InCheckingDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InCheckingDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
