import { Component, OnInit } from '@angular/core';
import {ShareOrderDataService} from './services/share-order-data.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
  providers:[ShareOrderDataService]
})
export class OrderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
