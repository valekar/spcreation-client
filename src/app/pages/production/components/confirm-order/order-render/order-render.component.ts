import { Component, OnInit,Input } from '@angular/core';
import {Inward} from '../../../../models/inward.model';

@Component({
  selector: 'app-order-render',
  templateUrl: './order-render.component.html',
  styleUrls: ['./order-render.component.scss']
})
export class OrderRenderComponent implements OnInit {

  @Input() rowData:Inward;
  constructor() { }

  ngOnInit() {
  }

}
