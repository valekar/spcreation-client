import { Injectable } from '@angular/core';
import {CustomHttp} from '../../../app-error-interceptor.service';
import {CONSTANT} from '../../models/CONSTANTS';
import {Response} from '@angular/http'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import {CommonService} from '../../services/common.service';
import {URLS} from '../../models/URLS';
import {Tag} from '../../models/inward.model';
import {InwardOrderRenderComponent} from '../components/inward-get-all/inward-order-render/inward-order-render.component';
import {TagInwardLinkRenderComponent} from '../components/tag-inward/tag-inward-link-render/tag-inward-link-render.component';
import {TagNumberEditorComponent} from '../components/tag/tag-number-editor/tag-number-editor.component';
import {SPCRenderComponent} from '../components/inward-get-all/spcrender/spcrender.component';

@Injectable()
export class TagService {
  
  constructor(private http:CustomHttp,private commonService:CommonService) { }
  
  getAll(){
    return this.http.get(URLS.TAG_URL, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  getAllByInward(id:number){
    return this.http.get(URLS.INWARD_URL+id+"/tags/", this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  get(id:number){
    return this.http.get(URLS.TAG_URL + id, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  add(tag:Tag){
    return this.http.post(URLS.TAG_URL,tag,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  edit(tag:Tag,id:number){
    return this.http.put(URLS.TAG_URL+id,tag,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  delete(id:number){
    return this.http.delete(URLS.TAG_URL+id,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  getInwardColumns(){
    return {
      SPC:{
        title:"SPC #",
        type:"custom",
        renderComponent:SPCRenderComponent,
        editable:false
      },
      inwardNumber:{
        title:"Inward #",
        type:"text",
        editable:false
        
      },
      layStr:{
        title:"Lay",
        type:"text",
        width:"15%",
        
      },
      order:{
        title:"Order",
        type:"custom",
        width:"35%",
        editable:false,
        renderComponent:InwardOrderRenderComponent,
        filter:false
      },
      updated_by:{
        title:'Updated By',
        type:'text',
        editable:false
      },
      formatted_updated_at:{
        title :"Updated At",
        type:"text",
        editable:false,
      },
      Actions:{
        title:"Add Tags",
        type:"custom",
        renderComponent:TagInwardLinkRenderComponent
      }
    }
  }

  getTagColumns(){
    return {
      tagId:{
        type:"text",
        title:"Tag Id",
        editor:{
          type:"custom",
          component:TagNumberEditorComponent
        }
      },
      size:{
        type:"text",
        title:"Size",
        editor:{
          type:"custom",
          component:TagNumberEditorComponent
        }
        
      },
      bundle:{
        type:"text",
        title:"Bundle",
        editor:{
          type:"custom",
          component:TagNumberEditorComponent
        }
        
      },
      quantity:{
        type:"text",
        title:"Quantity",
        editor:{
          type:"custom",
          component:TagNumberEditorComponent
        }
        
      },
      comments:{
        type:"text",
        title:"Comments"
      }
    }
  }

}
