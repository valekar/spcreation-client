import { Injectable } from '@angular/core';
import { UserToken } from '../models/user.model';
import { Headers, RequestOptions, ResponseContentType } from '@angular/http';

@Injectable()
export class CommonService {
  private userToken: UserToken;
  constructor() { }

  getAccessToken() {
    this.userToken = JSON.parse(localStorage.getItem('currentUser'));
    return this.userToken.access_token;
  }

  getHeaderOptions() {
    let header = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': "Bearer " + this.getAccessToken(),  
      'Access-Control-Allow-Origin':"*"    
    });

    let options = new RequestOptions({ headers: header });
    return options;
  }

  getUndefinedOptions() {
    let header = new Headers({
      'Accept': 'application/json',
      'Authorization': "Bearer " + this.getAccessToken()
    });

    let options = new RequestOptions({ headers: header });
    return options;
  }

  getHeaderOptionsWithoutAuth() {
    let header = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });

    let options = new RequestOptions({ headers: header });
    return options;
  }

  getBlobOptions() {
    let header = new Headers({
      'Authorization': "Bearer " + this.getAccessToken()
    });
    let options = new RequestOptions({ headers: header,responseType:ResponseContentType.Blob });
    return options;
  }
}
