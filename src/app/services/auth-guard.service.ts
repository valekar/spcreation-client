import { Injectable } from '@angular/core';
import {Router , CanActivate , ActivatedRouteSnapshot , RouterStateSnapshot} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';
import {Observable} from 'rxjs/observable';

@Injectable()
export class AuthGuardService implements CanActivate{

  constructor(private router:Router, private _authService:AuthenticationService) { }

 /* canActivate(route: ActivatedRouteSnapshot , state:RouterStateSnapshot){
    // if(localStorage.getItem('currentUser')){
    //   let user = JSON.parse(localStorage.getItem('currentUser'));
    //   if(user.access_token)
    //     return true;
    // }

    // this.router.navigate(['/'], {queryParams:{returnUrl : state.url}});
    // return false;
    return this._authService.isUserLoggedin();
  }*/

 
   canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean>|Promise<boolean>|boolean {
     if(this._authService.isUserLoggedin()){
       return true;
     }
      else{
        this._authService.redirectToLogin(state.url);
        return false;
      }
  }

 
}