import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OutwardService } from '../../services/outward.service';
import { ErrorResponse, ResObj } from '../../../models/common.model';
import { OutwardReport } from '../../../models/report.model';
import { LocalDataSource } from 'ng2-smart-table';
import { CONSTANT } from '../../../models/CONSTANTS';
import { ModalComponent } from '../../../services/modal/modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SlimBarService } from '../../../services/slim-bar.service';
@Component({
  selector: 'app-outward-details',
  templateUrl: './outward-details.component.html',
  styleUrls: ['./outward-details.component.scss'],
  providers: [OutwardService]
})
export class OutwardDetailsComponent implements OnInit {
  settings: any;
  columns: any;
  activeModal: any;
  source: LocalDataSource;
  outwardReportList:Array<OutwardReport>;
  constructor(private activatedRoute: ActivatedRoute, private outwardService: OutwardService,
    private slimBarService: SlimBarService, private modalService: NgbModal) { 
      this.slimBarService.progress();
      this.columns = this.outwardService.getOutwardColumns();
      this.settings = {
        actions: {
          add: false,
          edit: false,
          delete: false
        },
        hideSubHeader: false,
        add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
        edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
        delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
        columns: this.columns
      }
    }

  ngOnInit() {

    this.activatedRoute.params.subscribe((urlParams: { id: number }) => {
      this.slimBarService.startLoading();
      this.outwardService.getInwardsFromCustomerId(urlParams.id)
        .subscribe((res: ResObj<OutwardReport>) => {
          //console.log(res.obj);
          this.slimBarService.completeLoading();
          this.outwardReportList = res.obj;
          this.source = new LocalDataSource(this.outwardReportList);
        },
        (err: ErrorResponse) => { }
        );
    });


  }

}
