import { Routes, RouterModule }  from '@angular/router';
import {InwardComponent} from './inward.component';
import {InwardGetAllComponent} from './components/inward-get-all/inward-get-all.component';
import {TagComponent} from './components/tag/tag.component';
import {TagInwardComponent } from './components/tag-inward/tag-inward.component';
const routes: Routes = [
    {
      path: '',
      component:   InwardComponent,
       children: [
         { path: '', component: InwardGetAllComponent },
         { path:'tags',component:TagInwardComponent},
         { path: ':id/tags', component: TagComponent },
         
      ]
    }
  ];

export const routing = RouterModule.forChild(routes);