import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SPCRenderComponent } from './spcrender.component';

describe('SPCRenderComponent', () => {
  let component: SPCRenderComponent;
  let fixture: ComponentFixture<SPCRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SPCRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SPCRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
