import { Component, OnInit } from '@angular/core';
import {Cell,Editor,DefaultEditor} from 'ng2-smart-table';
import {Constant} from '../../../../models/constant.model';
import {DepartmentService} from '../../../services/department.service';
import {ErrorResponse,ResObj } from '../../../../models/common.model';
import {SlimBarService} from '../../../../services/slim-bar.service';
import {ConstantService} from '../../../../admin/services/constant.service';
@Component({
  selector: 'app-drop-down-editor',
  templateUrl: './drop-down-editor.component.html',
  styleUrls: ['./drop-down-editor.component.scss'],
  providers:[SlimBarService,ConstantService]
})
export class DropDownEditorComponent extends DefaultEditor implements OnInit {


  constantsList:Array<Constant>;
  selectedDepartmentName:string;
  constructor(private constantService:ConstantService ,private slimBarService:SlimBarService) { 
    super();
    this.slimBarService.progress();
  }

  ngOnInit() {
    if(this.cell.getValue() != ""){
      this.selectedDepartmentName = this.cell.getValue();
    }
    this.getAllConstants();
  }


  getAllConstants(){
    this.slimBarService.startLoading();
    this.constantService.getAll().subscribe((res:ResObj<Constant>)=>{
        this.constantsList = res.obj;
        this.slimBarService.completeLoading();
    },(err:ErrorResponse)=>{});
  }

  onDepartmentSelect(){
    this.cell.newValue = this.selectedDepartmentName;
  }
}
