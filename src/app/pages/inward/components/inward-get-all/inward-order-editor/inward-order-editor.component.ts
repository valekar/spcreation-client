import { Component, OnInit } from '@angular/core';
import { DefaultEditor, Cell, Editor } from 'ng2-smart-table';
import { SlimBarService } from '../../../../services/slim-bar.service';
import { Order } from '../../../../models/order.model';
import { OrderService } from '../../../../order/services/order.service';
import { ModalComponent } from '../../../../services/modal/modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorResponse, SuccessReponseWithObj, ResObj, SuccessResponse } from '../../../../models/common.model';
import {SharedDataService} from '../../../services/shared-data.service';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";

@Component({
  selector: 'app-inward-order-editor',
  templateUrl: './inward-order-editor.component.html',
  styleUrls: ['./inward-order-editor.component.scss'],
  providers: [OrderService]
})
export class InwardOrderEditorComponent extends DefaultEditor implements OnInit {


  orderList: Array<Order>;
  selectedOrder: Order;
  items:Array<any>;
  isOrderListEmpty:boolean= false;
  constructor(private orderService: OrderService,
    private slimBarService: SlimBarService, private modalService: NgbModal,
    private sharedDataService:SharedDataService,private _sanitizer: DomSanitizer) {
    super();
  }

  ngOnInit() {
    this.getAllOrders(this.sharedDataService.getOrderSize());
  }

  orderChanged() {
    //console.log(this.selectedOrder);
    this.cell.newValue = this.selectedOrder;
  }


  private getAllOrders(size:number) {
    this.slimBarService.startLoading();
    //for now lets not use server pagination
    let page = 0;
    //let size = 0;
    //for now lets not use server pagination
    this.orderService.getOnlyApprovedOrders(page, size).subscribe(
      (res: ResObj<Order>) => {
        this.orderList = res.obj;
        this.items = this.orderList;
        if(this.orderList.length>0){
          if (this.cell.getValue() != "" || this.cell.getValue() != undefined || this.cell.getValue() != null) {
            for(let order of this.orderList){
              if(order.id == this.cell.getValue().id){
                this.selectedOrder = order;
              }
            }
          }
        }
        else{
          this.isOrderListEmpty = true;
        }
        
        this.slimBarService.completeLoading();

      },
      (err: ErrorResponse) => {
        this.slimBarService.completeLoading();
      }
    );
  }

  // autocompleListFormatter = (data: Order) => {
  //   let html = `<span >${data.id},${data.customer.name}, ${data.spc},${data.designName},${data.part} </span>`;
  //   //return this._sanitizer.bypassSecurityTrustHtml(html);
  //   return html;
  // }

  // valueChanged(newValue){
  //   this.selectedOrder = newValue;
  // }

  

}
