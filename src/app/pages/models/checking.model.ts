import {Employee} from './company.model';
import {Tag} from './inward.model';
export class Checking{
    id:number;
    shift:string;
    planningDate:string;
    employees:Array<CheckEmployee>;
}

export class CheckEmployee{
    id:number;
    helper:Employee;
    checker:Employee;
    assignedQuantity:number;
    assignedTags:Array<CheckTag>;
}


export class CheckTag{
    id:number;
    tag:Tag;
}