import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { Ng2SmartTableModule} from 'ng2-smart-table';
import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

import { routing }       from './store.routing';
import {StoreComponent} from './store.component';
import { AppliqueComponent} from './components/applique/applique.component';
import { PaperComponent } from './components/paper/paper.component';
import { MachineComponent } from './components/machine/machine.component';
import { ThreadComponent } from './components/thread/thread.component';
import { NumberEditorComponent } from './components/machine/number-editor/number-editor.component';
@NgModule({
  imports: [
    CommonModule,
    SlimLoadingBarModule.forRoot(),
    routing,
    FormsModule,
    NgaModule,
    Ng2SmartTableModule,
    NgbDropdownModule,
    NgbModalModule,
  ],
  declarations: [
    StoreComponent,
    AppliqueComponent,
    PaperComponent,
    MachineComponent,
    ThreadComponent,
    NumberEditorComponent
  ],
  entryComponents:[NumberEditorComponent]
})
export class StoreModule { }
