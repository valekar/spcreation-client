import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OutwardComponent } from './components/outward/outward.component';
import { ReportsComponent } from './reports.component';
import {routing} from './reports.routing';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { Ng2SmartTableModule} from 'ng2-smart-table';
import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { OutwardDetailsComponent } from './components/outward-details/outward-details.component';
import { AnchorRenderComponent } from './components/outward/anchor-render/anchor-render.component';
import {SlimBarService} from '../services/slim-bar.service';
@NgModule({
  imports: [
    CommonModule,
    routing,
    NgaModule,
    Ng2SmartTableModule,
    NgbDropdownModule,
    NgbModalModule,
    SlimLoadingBarModule.forRoot(),
  ],
  declarations: [OutwardComponent, ReportsComponent, OutwardDetailsComponent, AnchorRenderComponent],
  entryComponents:[AnchorRenderComponent],
  providers:[SlimBarService]
})
export class ReportsModule { }
