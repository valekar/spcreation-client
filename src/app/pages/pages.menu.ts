export const PAGES_MENU = [
  {
    path: 'pages',
    children: [
      // {
      //   path: 'dashboard',
      //   data: {
      //     menu: {
      //       title: 'general.menu.dashboard',
      //       icon: 'ion-android-home',
      //       selected: true,
      //       expanded: false,
      //       order: 0,
      //     },
      //   },
      // },
      {
        path: 'orders',
        data: {
          menu: {
            title: 'general.menu.order',
            icon: 'ion-android-home',
            selected: true,
            expanded: false,
            order: 1,
            hidden:false,
            customData:"order_tab"
          },
        },
      },
      {
        path: 'inwards',
        data: {
          menu: {
            title: 'general.menu.inward',
            icon: 'ion-android-archive',
            selected: true,
            expanded: false,
            order: 2,
            customData:"inward_tab"
            
          },
        },
        children:[
          {
            path: '',
            order: 33,
            data: {
              menu: {
                title: 'general.menu.inwardGetAll',
              }
            }
          },
          {
            path: 'tags',
            order: 33,
            data: {
              menu: {
                title: 'general.menu.inwardTag',
              }
            }
          }
        ]
      },
      {
        path: 'productions',
        data: {
          menu: {
            title: 'general.menu.production',
            icon: 'ion-paintbucket',
            selected: true,
            expanded: false,
            order: 7,
            customData:"production_data"
          },
        },
        children:[
          {
            path:"confirm-orders",
            data:{
              menu:{
                title:"general.menu.confirmOrders"
              }
            }
          },
          {
            path:"planning",
            data:{
              menu:{
                title:"general.menu.productionPlanning"
              }
            }
          },
          {
            path:"in-production",
            data:{
              menu:{
                title:"general.menu.inProduction"
              }
            }
          }
        ]
      },
      {
        path: 'checkings',
        data: {
          menu: {
            title: 'general.menu.checking',
            icon: 'ion-android-checkbox-outline',
            selected: true,
            expanded: false,
            order: 8,
            customData:"checking_tab"
          },
        },
        children:[
          {
            path: 'planning',
            order: 81,
            data: {
              menu: {
                title: 'general.menu.checkPlanning',
              }
            }
          },
          {
            path: 'in-checking',
            order: 81,
            data: {
              menu: {
                title: 'general.menu.inChecking',
              }
            }
          },
        ]
      },
      {
        path: 'reports',
        data: {
          menu: {
            title: 'general.menu.reports',
            icon: 'ion-stats-bars',
            selected: true,
            expanded: false,
            order: 9,
            customData:"reports_tab"
          },
        },
        children:[
          {
            path: '',
            order: 91,
            data: {
              menu: {
                title: 'general.menu.outward',
              }
            }
          }
        ]
      },
      {
        path: 'admin',
        data: {
          menu: {
            title: 'general.menu.admin',
            icon: 'ion-gear-a',
            selected: true,
            expanded: false,
            order: 3,
            customData:"admin_tab"
          },
        }, children: [
          {
            path: 'user-permissions',
            order: 31,
            data: {
              menu: {
                title: 'general.menu.userPermissions',
              }
            }
          },
          {
            path: 'approvals',
            order: 32,
            data: {
              menu: {
                title: 'general.menu.approvals',
              }
            },
            children:[
              {
                path: 'order-approvals',
                order: 33,
                data: {
                  menu: {
                    title: 'general.menu.orderApprovals',
                  }
                }
              },
            ]
          },
          {
            path: 'constants',
            order: 31,
            data: {
              menu: {
                title: 'general.menu.constants',
              }
            }
          },
        ]
      },
      {
        path: 'store',
        data: {
          menu: {
            title: 'general.menu.store',
            icon: 'ion-cube',
            selected: true,
            expanded: false,
            order: 5,
            customData:"store_tab"
          },
        },
        children: [
          {
            path: 'appliques',
            data: {
              menu: {
                title: 'general.menu.applique',
              }
            }
          },
          {
            path: 'papers',
            data: {
              menu: {
                title: 'general.menu.paper',
              }
            }
          },
          {
            path: 'machines',
            data: {
              menu: {
                title: 'general.menu.machine',
              }
            }
          },
          {
            path: 'threads',
            data: {
              menu: {
                title: 'general.menu.thread',
              }
            }
          }
        ]
      },
      {
        path: 'company',
        data: {
          menu: {
            title: 'general.menu.company',
            icon: 'ion-android-contacts',
            selected: true,
            expanded: false,
            order: 6,
            customData:"company_tab"
          },
        }, children: [
          {
            path: 'employees',
            data: {
              menu: {
                title: 'general.menu.employee',
              }
            }
          },
          {
            path: 'departments',
            data: {
              menu: {
                title: 'general.menu.department',
              }
            }
          },
          {
            path: 'rates',
            data: {
              menu: {
                title: 'general.menu.rate',
              }
            }
          },
          {
            path: 'customers',
            data: {
              menu: {
                title: 'general.menu.customer',
              }
            }
          }
        ]

      },
    ],
  },
];
