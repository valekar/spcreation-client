import { Routes, RouterModule } from '@angular/router';
import { Pages } from './pages.component';
import { ModuleWithProviders } from '@angular/core';
import {AuthGuardService} from '../services/auth-guard.service';
import {ChangePasswordResolver} from '../pages/services/change-password.resolver.service';
// noinspection TypeScriptValidateTypes

// export function loadChildren(path) { return System.import(path); };

export const routes: Routes = [
  {
    path: 'login',
    loadChildren: 'app/pages/login/login.module#LoginModule',
  },
  {
     path: 'register',
     loadChildren: 'app/pages/register/register.module#RegisterModule'
  },
    {
     path: 'reset-password',
     loadChildren: 'app/pages/reset-password/reset-password.module#ResetPasswordModule',
     
  },
     {
     path: 'change-password',
     loadChildren: 'app/pages/change-password/change-password.module#ChangePasswordModule',
     resolve: {
              data:ChangePasswordResolver
            }
  },
  {
    path: 'pages',
    component: Pages,
    canActivate:[AuthGuardService],
    children: [
      { path: '', redirectTo: 'orders', pathMatch: 'full' },
      { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
      { path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
      { path:'store', loadChildren:'./store/store.module#StoreModule'},
      { path:'company',loadChildren:'./company/company.module#CompanyModule'},
      { path:'orders',loadChildren:'./order/order.module#OrderModule'},
      { path:'inwards',loadChildren:'./inward/inward.module#InwardModule'},
      { path:'productions',loadChildren:'./production/production.module#ProductionModule'},
      { path:'checkings',loadChildren:'./checking/checking.module#CheckingModule'},
      { path:'reports',loadChildren:'./reports/reports.module#ReportsModule'}
    ],
  },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
