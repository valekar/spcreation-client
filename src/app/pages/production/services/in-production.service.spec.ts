import { TestBed, inject } from '@angular/core/testing';

import { InProductionService } from './in-production.service';

describe('InProductionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InProductionService]
    });
  });

  it('should be created', inject([InProductionService], (service: InProductionService) => {
    expect(service).toBeTruthy();
  }));
});
