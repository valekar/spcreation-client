import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusChangeRenderComponent } from './status-change-render.component';

describe('StatusChangeRenderComponent', () => {
  let component: StatusChangeRenderComponent;
  let fixture: ComponentFixture<StatusChangeRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusChangeRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusChangeRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
