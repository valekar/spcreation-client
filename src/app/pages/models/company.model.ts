export class Department{
    id:number;
    name:string;
}

export class Rate{
    id:number;
    stitches:number;
    rate:number;
    machineType:string;
}    

export class Customer{
    id:number;
    name:string;
    address:string;
}

export class Employee{
    id:number;
    name:string;
    age:number;
    blood_gorup:string;
    sex:string;
    address:string;
    departments:Array<Department>;
}