import { TestBed, inject } from '@angular/core/testing';

import { ShareOrderDataService } from './share-order-data.service';

describe('ShareOrderDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShareOrderDataService]
    });
  });

  it('should be created', inject([ShareOrderDataService], (service: ShareOrderDataService) => {
    expect(service).toBeTruthy();
  }));
});
