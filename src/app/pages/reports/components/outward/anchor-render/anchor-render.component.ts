import { Component, OnInit,Input } from '@angular/core';
import {Customer} from '../../../../models/company.model';

@Component({
  selector: 'app-anchor-render',
  templateUrl: './anchor-render.component.html',
  styleUrls: ['./anchor-render.component.scss']
})
export class AnchorRenderComponent implements OnInit {


  @Input() rowData:Customer;
  constructor() { }

  ngOnInit() {
  }

}
