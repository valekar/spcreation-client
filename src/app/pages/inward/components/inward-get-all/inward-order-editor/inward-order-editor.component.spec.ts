import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InwardOrderEditorComponent } from './inward-order-editor.component';

describe('InwardOrderEditorComponent', () => {
  let component: InwardOrderEditorComponent;
  let fixture: ComponentFixture<InwardOrderEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InwardOrderEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InwardOrderEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
