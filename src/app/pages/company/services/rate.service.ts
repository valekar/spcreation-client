import { Injectable } from '@angular/core';
import {CONSTANT} from '../../models/CONSTANTS';
import {Response} from '@angular/http'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import {CommonService} from '../../services/common.service';
import {URLS} from '../../models/URLS';
import {Rate} from '../../models/company.model';
import {CustomHttp} from '../../../app-error-interceptor.service';

@Injectable()
export class RateService {

  constructor(private http:CustomHttp,private commonService:CommonService) { }
  getAll(){
    return this.http.get(URLS.RATE_URL, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  get(id:number){
    return this.http.get(URLS.RATE_URL + id, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  add(rate:Rate){
    return this.http.post(URLS.RATE_URL,rate,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  edit(rate:Rate,id:number){
    return this.http.put(URLS.RATE_URL+id,rate,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  delete(id:number){
    return this.http.delete(URLS.RATE_URL+id,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  getColumns(){
    return {
      stitches:{
        title:"Number of Stitches",
        type:"text"
      },
      rate:{
        title:"Rate",
        type:"text"
      },
      machineType:{
        title:"Machine Type",
        type:"text"
      },
      updated_by:{
        title:'Updated By',
        type:'text',
        editable:false,
      },
      formatted_updated_at:{
        title :"Updated At",
        type:"text",
        editable:false,
      }
    }
  }
}
