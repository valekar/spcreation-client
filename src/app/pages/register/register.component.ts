import {Component} from '@angular/core';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {EmailValidator, EqualPasswordsValidator} from '../../theme/validators';
import {User,ErrorResponse,SuccessResponse,CONSTANTS} from '../models/user.model';
import {UserService} from '../services/user.service';
import {CommonService} from '../services/common.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SlimBarService} from '../services/slim-bar.service';
import {ModalComponent} from '../services/modal/modal.component';
import {Router} from '@angular/router';
@Component({
  selector: 'register',
  templateUrl: './register.html',
  styleUrls: ['./register.scss'],
  providers:[UserService,CommonService, SlimBarService]
})
export class Register {

  public form:FormGroup;
  public username:AbstractControl;
  public email:AbstractControl;
  public password:AbstractControl;
  public repeatPassword:AbstractControl;
  public passwords:FormGroup;

  public submitted:boolean = false;

  constructor(fb:FormBuilder , private userService:UserService,private modalService: NgbModal,private slimBarService:SlimBarService,
    private router:Router) {

    //this.slimLoadingBarService.progress = 30;
    
    this.form = fb.group({
      'username': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'email': ['', Validators.compose([Validators.required, EmailValidator.validate])],
      'passwords': fb.group({
      'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'repeatPassword': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
      }, {validator: EqualPasswordsValidator.validate('password', 'repeatPassword')})
    });

    this.username = this.form.controls['username'];
    this.email = this.form.controls['email'];
    this.passwords = <FormGroup> this.form.controls['passwords'];
    this.password = this.passwords.controls['password'];
    this.repeatPassword = this.passwords.controls['repeatPassword'];
  }
   

  public onSubmit(values:Object):void {
    this.submitted = true;
    //console.log(values);
    let u = new User();
    u.email = values["email"];
    u.username = values["username"];
    u.password = values["passwords"]["password"];

    //console.log(u);

    if (this.form.valid) {
    this.slimBarService.startLoading();
      this.userService.saveUser(u)
      .subscribe((res:any)=> 
        {
          let successResponse : SuccessResponse = res.json();
          //console.log(res);
          this.slimBarService.completeLoading();
          this.registerModal(successResponse.header,successResponse.description +"," + CONSTANTS.PLEASE_LOGIN);
          this.router.navigate(['/']);

        },(err) => {
          this.clearUsernameInput();          
          let obj = err.json();
          let myResponse :ErrorResponse = obj;
          this.registerModal(myResponse.header,myResponse.description);
          //console.log(err);
          this.slimBarService.completeLoading();
        });
    }
  }

  registerModal(header:String,description:String){
    const activeModal = this.modalService.open(ModalComponent, {size: 'lg'});
          activeModal.componentInstance.modalHeader = header;
          activeModal.componentInstance.modalContent = description;
  }

  clearUsernameInput():void{
    this.form.controls['username'].reset();
  }
}
