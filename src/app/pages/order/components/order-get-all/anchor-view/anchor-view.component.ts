import { Component, OnInit,Input } from '@angular/core';
import {Order } from '../../../../models/order.model';
import {ShareOrderDataService} from '../../../services/share-order-data.service';

@Component({
  selector: 'app-anchor-view',
  templateUrl: './anchor-view.component.html',
  styleUrls: ['./anchor-view.component.scss']
})
export class AnchorViewComponent implements OnInit {


  @Input() rowData: Order;

  constructor(private sharedData:ShareOrderDataService) {
    //this.sharedData.setOrderData(this.rowData);
   }

  ngOnInit() {
  }

}
