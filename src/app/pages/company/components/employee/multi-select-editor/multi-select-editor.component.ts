import { Component, OnInit,Input,AfterViewInit  } from '@angular/core';
import { Cell, DefaultEditor,Editor } from 'ng2-smart-table';
import {Employee,Department} from '../../../../models/company.model';
import { IMultiSelectOption,IMultiSelectSettings } from 'angular-2-dropdown-multiselect';
import {DepartmentService} from '../../../services/department.service';
import {ResObj} from '../../../../models/common.model';
@Component({
  selector: 'app-multi-select-editor',
  templateUrl: './multi-select-editor.component.html',
  styleUrls: ['./multi-select-editor.component.scss'],
  providers:[DepartmentService]
})
export class MultiSelectEditorComponent extends DefaultEditor implements OnInit {

  optionsModel: number[] = new Array();
  myOptions: IMultiSelectOption[];
  employee:Employee;
  mySettings: IMultiSelectSettings;
  departmentList:Array<Department>;
  //@Output() notifyParent: EventEmitter<any> = new EventEmitter();

  constructor(private departmentService:DepartmentService) {
    super();
   }

  ngOnInit() {

    this.departmentService.getAll().subscribe(
      (res:ResObj<Department>)=> {
        this.myOptions = res.obj;
        this.departmentList = res.obj;
        this.employee = this.cell.getRow().getData();
        if(this.employee!=null){
          if(this.employee.departments!=null)
            for(let department of this.employee.departments){
              this.optionsModel.push(department.id);
            }
        }
      }
    );  

    this.mySettings = {
      //maxHeight:"100px",
      checkedStyle: 'fontawesome',
      buttonClasses: 'btn btn-default btn-block',
      showCheckAll:true,
      showUncheckAll:true,
      closeOnSelect:true
    };    
  }

  onChange(event:any) {
    console.log(this.optionsModel);
    let newSelectedList:Array<Department> = new Array();
    for(let department of this.departmentList){
      for(let id of this.optionsModel){
        if(id == department.id){
          newSelectedList.push(department);
        }
      }
    }

    //console.log(newSelectedList);
    this.cell.newValue = newSelectedList;
}

}
