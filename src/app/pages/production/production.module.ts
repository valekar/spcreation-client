import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { Ng2SmartTableModule} from 'ng2-smart-table';
import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { ProductionComponent } from './production.component';
import {routing } from './production.routing';
import { ConfirmOrderComponent } from './components/confirm-order/confirm-order.component';
import {ProductionService} from './services/production.service';
import {SlimBarService} from '../services/slim-bar.service';

import { OrderRenderComponent } from './components/confirm-order/order-render/order-render.component';
import { OrderStatusRenderComponent } from './components/confirm-order/order-status-render/order-status-render.component';
import { StatusChangeRenderComponent } from './components/confirm-order/status-change-render/status-change-render.component';
import { SPCRenderComponent } from './components/confirm-order/spcrender/spcrender.component';
import { PlanningComponent } from './components/planning/planning.component';
//import { Ng2DragDropModule } from 'ng2-drag-drop';
import {DragulaModule} from 'ng2-dragula';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { InProductionComponent } from './components/in-production/in-production.component';
import { ShowDetailRenderComponent } from './components/in-production/show-detail-render/show-detail-render.component';
import { InProductionDetailsComponent } from './components/in-production-details/in-production-details.component';
import {TagNumberEditorComponent} from './components/in-production-details/tag-number-editor/tag-number-editor.component';
import { MistakesEditorComponent } from './components/in-production-details/mistakes-editor/mistakes-editor.component';


@NgModule({
  imports: [
    CommonModule,
    routing,
    SlimLoadingBarModule.forRoot(),
    FormsModule,
    ReactiveFormsModule, 
    NgaModule,
    Ng2SmartTableModule,
    NgbDropdownModule,
    NgbModalModule,
    NguiDatetimePickerModule,
    //Ng2DragDropModule.forRoot(),
    DragulaModule
  ],
  providers:[ProductionService,SlimBarService],
  declarations: [ProductionComponent, ConfirmOrderComponent, OrderRenderComponent,TagNumberEditorComponent,
     OrderStatusRenderComponent, StatusChangeRenderComponent, SPCRenderComponent, PlanningComponent, InProductionComponent, 
     ShowDetailRenderComponent, InProductionDetailsComponent, MistakesEditorComponent ],
  entryComponents:[OrderRenderComponent,OrderStatusRenderComponent,StatusChangeRenderComponent,
    SPCRenderComponent,ShowDetailRenderComponent,TagNumberEditorComponent,MistakesEditorComponent]
})
export class ProductionModule { }
