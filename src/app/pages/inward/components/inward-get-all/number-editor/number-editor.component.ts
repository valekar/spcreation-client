import { Component, OnInit } from '@angular/core';
import { Cell, DefaultEditor, Editor } from 'ng2-smart-table';

@Component({
  selector: 'app-number-editor',
  templateUrl: './number-editor.component.html',
  styleUrls: ['./number-editor.component.scss']
})
export class NumberEditorComponent extends DefaultEditor implements OnInit {

  inwardNumber: number;
  constructor() {
    super();
  }

  ngOnInit() {
    if (this.cell.getValue() == "" || this.cell.getValue() == undefined || this.cell.getValue() == null) {
      this.inwardNumber = 0;
      this.cell.newValue = this.inwardNumber;
    }
    else{
      this.inwardNumber = this.cell.getValue();
    }
  }

  numberChange(){
    this.cell.newValue = this.inwardNumber;
  }

}
