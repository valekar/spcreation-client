import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnchorRenderComponent } from './anchor-render.component';

describe('AnchorRenderComponent', () => {
  let component: AnchorRenderComponent;
  let fixture: ComponentFixture<AnchorRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnchorRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnchorRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
