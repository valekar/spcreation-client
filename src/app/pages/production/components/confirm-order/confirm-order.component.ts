import { Component, OnInit } from '@angular/core';
import { ProductionService } from '../../services/production.service';
import { Inward } from '../../../models/inward.model';
import { ModalComponent } from '../../../services/modal/modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SlimBarService } from '../../../services/slim-bar.service';
import { ErrorResponse, SuccessReponseWithObj, ResObj, SuccessResponse } from '../../../models/common.model';
import { LocalDataSource } from 'ng2-smart-table';
import { CONSTANT } from '../../../models/CONSTANTS';
import { StatusChangeRenderComponent } from './status-change-render/status-change-render.component';


@Component({
  selector: 'app-confirm-order',
  templateUrl: './confirm-order.component.html',
  styleUrls: ['./confirm-order.component.scss']
})
export class ConfirmOrderComponent implements OnInit {

  activeModal: any;
  source: LocalDataSource;
  inwardList: Array<Inward>;
  settings: any;
  columns: any;
  constructor(private productionService: ProductionService, private slimBarService: SlimBarService,
    private modalService: NgbModal) {
    this.slimBarService.progress();
    this.columns = productionService.getColumns();
    this.columns["Action"] = this.actionJSON();
    this.settings = {
      actions: {
        add: false,
        edit: false,
        delete: false
      },
      hideSubHeader: false,
      add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
      edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
      delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
      columns: this.columns

    }
  }

  ngOnInit() {
    this.getAllInwards();
  }

  getAllInwards() {
    this.productionService.getAllInwards()
      .subscribe((res: ResObj<Inward>) => {
        this.inwardList = res.obj;
        this.source = new LocalDataSource(this.inwardList);
        this.slimBarService.completeLoading();
      }, (err: ErrorResponse) => { })
  }

  actionJSON() {
    return {
      title: "Actions",
      type: "custom",
      renderComponent: StatusChangeRenderComponent,
      onComponentInitFunction: (instance) => {
        instance.actionEmitter.subscribe(row => {
          if (row == CONSTANT.STATUS_CONFIRMED) {
            this.confirmInwardOrder(instance.rowData);
          }

        });
      },

      filter: false
    };
  }

  confirmInwardOrder(inward: Inward) {
    this.Modal(CONSTANT.ARE_YOU_SURE,
      CONSTANT.DO_YOU_WANT_TO_CONFIRM
      + CONSTANT.STRONG_TAG_OPEN
      + CONSTANT.CANNOT_REVERT_BACK_NOTE
      + CONSTANT.STRONG_TAG_CLOSE,
      false);

    this.activeModal.result.then((result: boolean) => {
      this.slimBarService.startLoading();
      this.productionService.confirmOrder(inward.id).subscribe((res: SuccessReponseWithObj<Inward>) => {
        for (let i = 0; i < this.inwardList.length; i++) {
          if (this.inwardList[i].id == res.obj.id) {
            this.inwardList.splice(i,1);
            this.source = new LocalDataSource(this.inwardList);
          }
        }
        this.slimBarService.completeLoading();
      }, (err: ErrorResponse) => { this.slimBarService.completeLoading() });
    });
  }

  private Modal(header: String, description: String, isCloseModal: boolean) {
    this.activeModal = this.modalService.open(ModalComponent, { size: 'sm' });
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }

}
