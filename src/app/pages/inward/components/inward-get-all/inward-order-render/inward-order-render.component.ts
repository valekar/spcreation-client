import { Component, OnInit,Input } from '@angular/core';
import {Order} from '../../../../models/order.model';
import {Inward} from '../../../../models/inward.model';

@Component({
  selector: 'app-inward-order-render',
  templateUrl: './inward-order-render.component.html',
  styleUrls: ['./inward-order-render.component.scss']
})
export class InwardOrderRenderComponent implements OnInit {

  @Input() rowData:Inward;

  constructor() { }

  ngOnInit() {

  }

}
