import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderStatusRenderComponent } from './order-status-render.component';

describe('OrderStatusRenderComponent', () => {
  let component: OrderStatusRenderComponent;
  let fixture: ComponentFixture<OrderStatusRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderStatusRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderStatusRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
