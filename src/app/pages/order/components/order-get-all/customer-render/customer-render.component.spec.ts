import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerRenderComponent } from './customer-render.component';

describe('CustomerRenderComponent', () => {
  let component: CustomerRenderComponent;
  let fixture: ComponentFixture<CustomerRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
