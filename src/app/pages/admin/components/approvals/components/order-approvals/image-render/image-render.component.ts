import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-image-render',
  templateUrl: './image-render.component.html',
  styleUrls: ['./image-render.component.scss']
})
export class ImageRenderComponent implements OnInit {

  @Input() rowData;
  constructor() { }

  ngOnInit() {
  }

}
