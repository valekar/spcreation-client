import { TestBed, inject } from '@angular/core/testing';

import { AppErrorInterceptorService } from './app-error-interceptor.service';

describe('AppErrorInterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppErrorInterceptorService]
    });
  });

  it('should be created', inject([AppErrorInterceptorService], (service: AppErrorInterceptorService) => {
    expect(service).toBeTruthy();
  }));
});
