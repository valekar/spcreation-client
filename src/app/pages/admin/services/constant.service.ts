import { Injectable } from '@angular/core';
import {CONSTANT} from '../../models/CONSTANTS';
import {Response} from '@angular/http'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import {CommonService} from '../../services/common.service';
import {URLS} from '../../models/URLS';
import {Applique} from '../../models/user.model';
import {CustomHttp} from '../../../app-error-interceptor.service';
import {Constant} from '../../models/constant.model';

@Injectable()
export class ConstantService {
  constructor(private http:CustomHttp,private commonService:CommonService) { }
  
  
    getAll(){
      return this.http.get(URLS.CONSTANT_URL, this.commonService.getHeaderOptions())
      .map((res:Response) => {return res.json()})
      .catch((err:Response)=> { return Observable.throw(err.json())});
    }
  
    get(id:number){
      return this.http.get(URLS.CONSTANT_URL + id, this.commonService.getHeaderOptions())
      .map((res:Response) => {return res.json()})
      .catch((err:Response)=> { return Observable.throw(err.json())});
    }
  
    add(constant:Constant){
      return this.http.post(URLS.CONSTANT_URL,constant,this.commonService.getHeaderOptions())
      .map((res:Response) => {return res.json()})
      .catch((err:Response)=> { return Observable.throw(err.json())});
    }
  
    edit(constant:Constant,id:number){
      return this.http.put(URLS.CONSTANT_URL+id,constant,this.commonService.getHeaderOptions())
      .map((res:Response) => {return res.json()})
      .catch((err:Response)=> { return Observable.throw(err.json())});
    }
  
    delete(id:number){
      return this.http.delete(URLS.CONSTANT_URL+id,this.commonService.getHeaderOptions())
      .map((res:Response) => {return res.json()})
      .catch((err:Response)=> { return Observable.throw(err.json())});
    }


    getColumns(){
      return {
        
        key:{
          title:"Key",
          type:"string"
        },
        value:{
          title:"Value",
          type:"string",
        },
        updated_by:{
          title:'Updated By',
          type:'string',
          editable:false,
        },
        formatted_updated_at:{
          title :"Updated At",
          type:"string",
          editable:false,
        }
      }
    }

}
