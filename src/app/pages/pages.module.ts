import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';

import { routing }       from './pages.routing';
import { NgaModule } from '../theme/nga.module';
import { AppTranslationModule } from '../app.translation.module';
import { Pages } from './pages.component';
import { Login} from '../pages/login/login.component';
import { AuthenticationService} from '../services/authentication.service';
import { AuthGuardService} from '../services/auth-guard.service';
import { ModalComponent } from './services/modal/modal.component';
import {ChangePasswordResolver } from './services/change-password.resolver.service';
import {UserService} from './services/user.service';
import {CommonService} from './services/common.service';
import {ExcelService} from './services/excel.service';


@NgModule({
  imports: [
    CommonModule, AppTranslationModule, NgaModule, routing 
   
  ],
  declarations: [Pages, ModalComponent],
  providers:[ AuthenticationService,AuthGuardService,ChangePasswordResolver,UserService,CommonService,ExcelService],
  
  entryComponents: [
    ModalComponent
  ],
})
export class PagesModule {
}
