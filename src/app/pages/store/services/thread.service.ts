import { Injectable } from '@angular/core';
import {CONSTANT} from '../../models/CONSTANTS';
import {Response} from '@angular/http'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import {CommonService} from '../../services/common.service';
import {URLS} from '../../models/URLS';
import {Thread} from '../../models/user.model';
import {CustomHttp} from '../../../app-error-interceptor.service';

@Injectable()
export class ThreadService {

  constructor(private http:CustomHttp,private commonService:CommonService) { }

  getAll(){
    return this.http.get(URLS.THREAD_URL, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  get(id:number){
    return this.http.get(URLS.THREAD_URL + id, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  add(thread:Thread){
    return this.http.post(URLS.THREAD_URL,thread,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  edit(thread:Thread,id:number){
    return this.http.put(URLS.THREAD_URL+id,thread,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  delete(id:number){
    return this.http.delete(URLS.THREAD_URL+id,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  getColumns(){
    return {
      brandName:{
        title:"Name of Brand",
        type:"string"
      },
      color:{
        title:"Color of thread",
        type:"string"
      },
      type:{
        title:"Type of thread",
        type:"string"
      },
      updated_by:{
        title:'Updated By',
        type:'string',
        editable:false,
      },
      formatted_updated_at:{
        title :"Updated At",
        type:"string",
        editable:false,
      }
    }
  }

}
