import { Component, OnInit } from '@angular/core';
import {DefaultEditor,Cell} from 'ng2-smart-table';

@Component({
  selector: 'app-mistakes-editor',
  templateUrl: './mistakes-editor.component.html',
  styleUrls: ['./mistakes-editor.component.scss']
})
export class MistakesEditorComponent extends DefaultEditor implements OnInit {

  maxValue:number=1000000;
  currentCellValue:any;
  constructor() { 
    super();
  }

  ngOnInit() {
    if(this.cell.getValue() == ""){
      this.cell.newValue = 0;
      this.currentCellValue = this.cell.newValue;
    }
    else{
      this.currentCellValue = this.cell.newValue;
    }
  }

  numberChange(){
    this.cell.newValue = this.currentCellValue;
  }


}
