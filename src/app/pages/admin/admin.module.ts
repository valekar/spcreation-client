import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AdminComponent} from './admin.component';
import { routing }       from './admin.routing';
import { Ng2SmartTableModule} from 'ng2-smart-table';
import { NgaModule } from '../../theme/nga.module';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { DropDownViewComponent } from './components/user-permissions/drop-down-view/drop-down.component';
import { DropDownEditorComponent } from './components/user-permissions/drop-down-editor/drop-down-editor.component';
import {FormsModule } from '@angular/forms';
import { UserPermissionsComponent } from './components/user-permissions/user-permissions.component';
import {AdminService} from './services/admin.service';
import {SlimBarService} from '../services/slim-bar.service';
import { ConstantComponent } from './components/constant/constant.component';

@NgModule({
  imports: [
    CommonModule,
    routing,
    NgaModule,
    FormsModule,
    Ng2SmartTableModule,
    SlimLoadingBarModule.forRoot()
  ],
  declarations: [AdminComponent, DropDownViewComponent, DropDownEditorComponent, 
    UserPermissionsComponent, ConstantComponent],
  entryComponents: [
    DropDownViewComponent,DropDownEditorComponent
  ],
  providers:[AdminService,SlimBarService]
})
export class AdminModule { }
