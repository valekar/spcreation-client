import { Component, OnInit } from '@angular/core';
import {ErrorResponse,SuccessReponseWithObj,ResObj,SuccessResponse} from '../../../models/common.model';
import {LocalDataSource } from 'ng2-smart-table';
import {CONSTANT} from '../../../models/CONSTANTS';
import {ModalComponent} from '../../../services/modal/modal.component';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {EmployeeService} from '../../services/employee.service';
import {DepartmentService} from '../../services/department.service';
import {SlimBarService} from '../../../services/slim-bar.service';
import {Employee} from '../../../models/company.model';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],
  providers:[EmployeeService,DepartmentService, SlimBarService]
})
export class EmployeeComponent implements OnInit {

  settings:any;
  columns:any;
  activeModal:any;
  source:LocalDataSource;
  employeeList:Array<Employee>;
  constructor(private employeeService:EmployeeService,private departmentService:DepartmentService, 
    private slimBarService:SlimBarService, private modalService:NgbModal) {
      this.slimBarService.progress();
      this.columns =employeeService.getColumns();
      
      this.settings = {
        actions:{
          add:true,
          edit:true,
          delete:true
        },
        hideSubHeader: false,
        add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
        edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
        delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
        columns:this.columns
      }
     }

  ngOnInit() {
    this.getAllEmployees();
  }

 


  onCreateConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.NOTICE_CREATE, CONSTANT.CREATE_DESC,false);
    //this is the event from modal
   
    this.activeModal.result.then((result:boolean)=> {
      this.create(event.newData,event);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
  }


  onSaveConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.NOTICE_UPDATE, 
      CONSTANT.UPDATE_DESC + CONSTANT.STRONG_TAG_OPEN+ event.data.name + CONSTANT.STRONG_TAG_CLOSE + CONSTANT.QUESTION_MARK,
      false);
    //this the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.update(event.newData,event);  
      //console.log(event.newData);
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
  }

  onDeleteConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.WARNING, 
      CONSTANT.DELETE_WARNING_DESC + CONSTANT.STRONG_TAG_OPEN+ event.data.name + CONSTANT.STRONG_TAG_CLOSE+ CONSTANT.QUESTION_MARK,
      false);
    // this is the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.delete(event.data.id);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
    
  }


  //add/edit/delete/get employee
 private create(employee:Employee,event){
   if(this.validation(employee)){
    this.slimBarService.startLoading();
    // console.log(user);
     this.employeeService.add(employee)
     .subscribe((res:SuccessReponseWithObj<Employee>)=>{
       this.slimBarService.completeLoading();
       this.getAllEmployees();
       this.Modal(res.header,res.description,true);
       //console.log(res);
       event.confirm.resolve();
     },
     (err:ErrorResponse)=>{
       this.slimBarService.completeLoading();
       this.Modal(err.header,err.description,true);
     }
    );
   }
 
}

private delete(id:number){
  this.slimBarService.startLoading();
   this.employeeService.delete(id).subscribe(
     (res:SuccessResponse)=>{
       
      for(let i=0;i<this.employeeList.length;i++){
        if(id == this.employeeList[i].id){
          this.employeeList.splice(i,1);
          break;
        } 
      } 
      this.source.load(this.employeeList);
      this.slimBarService.completeLoading(); 
      this.Modal(res.header,res.description,true);      
     },
     (err:ErrorResponse) => {
      this.slimBarService.completeLoading();
      this.Modal(err.header,err.description,true);
     }
    ); 
}

private update(employee:Employee,event){
  if(this.validation(employee)){
    this.slimBarService.startLoading();
    this.employeeService.edit(employee,employee.id)
    .subscribe((res:SuccessReponseWithObj<Employee>) => {
      this.slimBarService.completeLoading(); 
      this.Modal(res.header,res.description,true);
      console.log(res);
      event.confirm.resolve();
    },
    (err:ErrorResponse) => {
      //console.log(err);
      this.slimBarService.completeLoading();
      this.Modal(err.header,err.description,true);
     }
  );
  }
}
private getAllEmployees(){
  this.slimBarService.startLoading();
  this.employeeService.getAll().subscribe(
    (res:ResObj<Employee>)=>{
      this.employeeList = res.obj;
      this.source = new LocalDataSource(this.employeeList);
      this.slimBarService.completeLoading();
    },
    (err:ErrorResponse) => {}
  );
}

//end of add/edit/delete/get
private Modal(header:String,description:String,isCloseModal:boolean){
  this.activeModal = this.modalService.open(ModalComponent, {size: 'sm'});
  this.activeModal.componentInstance.modalHeader = header;
  this.activeModal.componentInstance.modalContent = description;
  this.activeModal.componentInstance.isCloseModal = isCloseModal;
}

private validation(employee:Employee){
  if(isNaN(+employee.age)){
    this.Modal(CONSTANT.VALIDATION_ERROR, CONSTANT.EMPLOYEE_VALIDATION_MESSAGE,true);
    return false;
  }
  return true;
}
}
