export class User {
    email:string;
    password:string;
    username : string ;
    enabled:boolean = true;
}

export class ErrorResponse{
    header : String;
    code : number;
    status : String;
    description:String;
    detailedDescription:String;

}

export class SuccessResponse {
    header:String;
    description:String;
    token : string; //custom added token    
}

export class SuccessReponseWithObj<T>{
    header:String;
    description:String;
    obj:T;
}

export class UserSuccessResponse{
    header:String;
    description:String;
    obj:ModifiedUser;
}



export class ResetPasswordModel{
    email:String
}

export class CONSTANTS {
    static PLEASE_LOGIN:string = "Please login to use the application";
}

export class ChangePasswordToken{
    token:string;
    password:string;
    repeatPassword:string;
}   


export class ModifiedUser{
    public username:string;
    public id:string;
    public enabled:boolean;
    public email:string;
    public privileges : Privilege;
    public inward_tab:string;
    public admin_tab:string;
    public production_tab:string;
    public order_tab:string;
    public checking_tab:string;
    public store_tab:string;
    public reports_tab:string;
    public company_tab:string;
}

export class Privilege{
    inward_tab:string;
    admin_tab:string;
}


//applique
export class AppliqueResObj{
    code:number;
    description:string;
    header:string;
    obj:Array<Applique>;
    status:string;
};
export class Applique{
    id:number;
    number:number;
    type:string;
    color:string;
    updated_by:string;
    formatted_updated_at:string;
    created_by:string;
    formatted_created_at:string;
};

//paper
export class Paper{
    id:number;
    quality:string;
    size:number;
    type:string;
}

export class PaperResObj{
    code:number;
    description:string;
    header:string;
    obj:Array<Paper>;
    status:string;
};

//Machine
export class Machine{
    id:number;
    heads:number;
    type:number;
    stitchCapacity:number;
}

export class ResObj<T>{
    code:number;
    description:string;
    header:string;
    obj:Array<T>;
    status:string;
}

//thread
export class Thread{
    id:number;
    brandName:string;
    color:string;
    type:string;
}



//user token

export class UserToken{
    access_token : string;
    token_type : string;
    refresh_token :string;
    expires_in :string;
    scope:string;
    authorities:Array<Authority>
}

export class Authority{
    authority:string;
}
