import { Component, OnInit } from '@angular/core';
import { DefaultEditor, Editor, Cell, } from 'ng2-smart-table';
import {CONSTANT} from '../../../../models/CONSTANTS';
import {InProductionService} from '../../../services/in-production.service';

@Component({
  selector: 'app-tag-number-editor',
  templateUrl: './tag-number-editor.component.html',
  styleUrls: ['./tag-number-editor.component.scss'],

})
export class TagNumberEditorComponent extends DefaultEditor implements OnInit {

  currentCellValue: any;
  maxValue:number;
  constructor() {
    super();
  }

  ngOnInit() {
    this.maxValue = this.getQuantity();
    if (this.cell.getValue() == "" || this.cell.getValue() == undefined || this.cell.getValue() == null) {
      this.currentCellValue = 0;
      
    }
    else {
      this.currentCellValue = this.cell.getValue();
    }
  }


  getQuantity(){
    return this.cell.getRow().cells[3].getValue();
  }

  numberChange() {
    this.cell.newValue = this.currentCellValue;
    let quantity = this.getQuantity();
    //console.log(quantity);
    if(this.currentCellValue == quantity){
      this.cell.getRow().cells[6].newValue = CONSTANT.STATUS_PRODUCED;
    }
    else{
      this.cell.getRow().cells[6].newValue = CONSTANT.STATUS_IN_PRODUCTION;
    }
   // console.log();
  }

}
