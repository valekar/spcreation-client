
export class SuccessReponseWithObj<T>{
    header:String;
    description:String;
    obj:T;
}

export class ResObj<T>{
    code:number;
    description:string;
    header:string;
    obj:Array<T>;
    status:string;
}

export class ErrorResponse{
    header : String;
    code : number;
    status : String;
    description:String;
    detailedDescription:String;

}

export class SuccessResponse {
    header:String;
    description:String;
    //token : string; //custom added token    
}