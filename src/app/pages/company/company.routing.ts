import { Routes, RouterModule }  from '@angular/router';
import {CompanyComponent} from './company.component';
import {DepartmentComponent } from './components/department/department.component';
import {RateComponent} from './components/rate/rate.component';
import {CustomerComponent} from './components/customer/customer.component';
import {EmployeeComponent } from './components/employee/employee.component';
const routes: Routes = [
    {
      path: '',
      component:   CompanyComponent,
       children: [
         { path: 'departments', component: DepartmentComponent },
         { path: 'rates', component: RateComponent },
         { path: 'customers', component: CustomerComponent },
         { path: 'employees', component: EmployeeComponent }
         
      ]
    }
  ];

export const routing = RouterModule.forChild(routes);