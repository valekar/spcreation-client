import { Component, OnInit, ViewChild, Input, Output, EventEmitter, ElementRef, Renderer } from '@angular/core';
import { NgUploaderOptions } from 'ngx-uploader';
import { OrderService } from '../../services/order.service';
import { Customer } from '../../../models/company.model';
import { Thread, Applique, Paper } from '../../../models/user.model';
import { Order, Combo } from '../../../models/order.model';
import { ResObj, ErrorResponse } from '../../../models/common.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SlimBarService } from '../../../services/slim-bar.service';
import { ModalComponent } from '../../../services/modal/modal.component';
import { Validators, FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CONSTANT } from '../../../models/CONSTANTS';

@Component({
  selector: 'app-order-add',
  templateUrl: './order-add.component.html',
  styleUrls: ['./order-add.component.scss'],
  providers: [OrderService, SlimBarService]
})
export class OrderAddComponent implements OnInit {

  public myForm: FormGroup; // form model

  @Input() defaultPicture: string = 'assets/img/theme/no-photo.png';
  @Input() picture: string = '';

  @Input() uploaderOptions: NgUploaderOptions = { url: '' };
  @Input() canDelete: boolean = true;

  @Output() onUpload = new EventEmitter<any>();
  @Output() onUploadCompleted = new EventEmitter<any>();

  @ViewChild('fileUpload') public _fileUpload: ElementRef;


  threadList: Array<Thread>;
  appliqueList: Array<Applique>;
  customerList: Array<Customer>;
  paperList: Array<Paper>;
  activeModal: any;
  public uploadInProgress: boolean;


  constructor(private renderer: Renderer, private orderService: OrderService, private _fb: FormBuilder
    , private slimBarService: SlimBarService, private modalService: NgbModal, private router: Router) {
    this.slimBarService.progress();
  }

  ngOnInit() {
    this.getAllAppliques();
    this.getAllCustomers();
    this.getAllPapers();
    this.getAllThreads();

    // we will initialize our form here
    this.myForm = this._fb.group({
      customer: ['', [Validators.required]],
      grade: ['', [Validators.required, Validators.minLength(1)]],
      designName: ['', [Validators.required, Validators.minLength(1)]],
      embroideryType: ['', [Validators.required, Validators.minLength(1)]],
      durationHR: ['', Validators.required],
      durationMN: ['', Validators.required],
      durationSS: ['', Validators.required],
      quantity: ['', [Validators.required, Validators.minLength(1)]],
      fabricColor: ['', [Validators.required, Validators.minLength(1)]],
      fabricSize: ['', [Validators.required, Validators.minLength(1)]],
      stitches: ['', [Validators.required, Validators.minLength(1)]],
      part: ['', [Validators.required, Validators.minLength(1)]],
      position: ['', [Validators.required, Validators.minLength(1)]],
      measurement: ['', [Validators.required, Validators.minLength(1)]],
      noOfCombos: ['', [Validators.required]],
      combos: this._fb.array([
        //this.initCombo()
      ])
    });

    //console.log(this.myForm);
  }


  comboChange() {
    //console.log(this.myForm.get('noOfCombos').value);
    let noOfCombos = this.myForm.get('noOfCombos').value
    this.clearCombos();
    if (this.myForm.get('noOfCombos').value > 0) {
      this.clearCombos();
      for (let i = 1; i <= noOfCombos; i++) {
        this.addCombos();
      }
    }

  }


  initCombo() {
    //initialize our combos
    return this._fb.group({
      cones: ['', Validators.required],
      boxes: ['', Validators.required],
      frameSize: ['', Validators.required],
      layers: ['', Validators.required],
      thread: ['', Validators.required],
      paper: ['', Validators.required],
      applique: ['', Validators.required]
    });
  }

  addCombos() {
    const control = <FormArray>this.myForm.controls['combos'];
    control.push(this.initCombo());
  }

  clearCombos() {
    //let control = <FormArray>this.myForm.controls['combos'];
    this.myForm.controls['combos'] = this._fb.array([]);
  }

  removeCombo(i: number) {
    const control = <FormArray>this.myForm.controls['combos'];
    control.removeAt(i);
  }

  //get customers
  getAllCustomers() {
    this.orderService.getAllCustomers().subscribe(
      ((res: ResObj<Customer>) => {
        this.customerList = res.obj;
      }),
      ((err: ErrorResponse) => {

      })
    );
  }
  getAllThreads() {
    this.orderService.getAllThreads().subscribe(((res: ResObj<Thread>) => {
      this.threadList = res.obj;
    }), ((err: ErrorResponse) => { }));
  }

  getAllPapers() {
    this.orderService.getAllPapers().subscribe((res: ResObj<Paper>) => {
      this.paperList = res.obj;
    }, (err: ErrorResponse) => { })
  }

  getAllAppliques() {
    this.orderService.getAllAppliques().subscribe((res: ResObj<Applique>) => {
      this.appliqueList = res.obj;
    }, (err: ErrorResponse) => { })
  }


  //related to image showing and fetching
  beforeUpload(uploadingFile): void {
    let files = this._fileUpload.nativeElement.files;

    if (files.length) {
      const file = files[0];
      this._changePicture(file);

      if (!this._canUploadOnServer()) {
        uploadingFile.setAbort();
      } else {
        this.uploadInProgress = true;
      }
    }
  }


  bringFileSelector(): boolean {
    this.renderer.invokeElementMethod(this._fileUpload.nativeElement, 'click');
    return false;
  }

  removePicture(): boolean {
    this.picture = '';
    return false;
  }

  _changePicture(file: File): void {
    const reader = new FileReader();
    reader.addEventListener('load', (event: Event) => {
      this.picture = (<any>event.target).result;
    }, false);
    reader.readAsDataURL(file);
  }

  _canUploadOnServer(): boolean {
    return !!this.uploaderOptions['url'];
  }

  //saving the order 
  save(model: FormGroup) {
    //console.log(model);
    this.slimBarService.startLoading();
    let order = new Order();
    order.combos = model.controls["combos"].value;
    order.customer = model.controls["customer"].value;
    order.designName = model.controls["designName"].value;
    order.embroideryType = model.controls["embroideryType"].value;
    order.fabric_size = model.controls["fabricSize"].value;
    order.fabricColor = model.controls["fabricColor"].value;
    order.grade = model.controls["grade"].value;
    order.part = model.controls["part"].value;
    order.position = model.controls["position"].value;
    order.measurement = model.controls["measurement"].value;
    order.noOfCombos = model.controls["noOfCombos"].value;
    order.quantity = model.controls["quantity"].value;
    order.stitches = model.controls["stitches"].value;
    //console.log(order);
    //this.orderService.add()
    let hour = model.controls["durationHR"].value;
    let minutes = model.controls["durationMN"].value;
    let seconds = model.controls["durationSS"].value;

    order.duration = hour * 3600 + minutes * 60 + seconds;

    //let inputEl: HTMLInputElement = this._fileUpload.nativeElement;
    let files = this._fileUpload.nativeElement.files
    let fileCount: number = files.length;
    let formData = new FormData();
    console.log()
    if (fileCount > 0) { // a file was selected
      // for (let i = 0; i < fileCount; i++) {
      formData.append('file', files[0]);
      // }
    }
    formData.append('order', new Blob([JSON.stringify(order)], { type: "application/json" }));


    //validations
    for (let i = 0; i < order.noOfCombos; i++) {
      for (let applique of this.appliqueList) {
        if(this.myForm.controls['combos']['controls'][i].controls.applique.value == ""){
          this.Modal(CONSTANT.VALIDATION_ERROR,CONSTANT.SELECT_APPLIQUE,true);
          this.slimBarService.completeLoading();
          return;
        }
        
      }
      for (let paper of this.paperList) {
        if(this.myForm.controls['combos']['controls'][i].controls.paper.value == ""){
          this.Modal(CONSTANT.VALIDATION_ERROR,CONSTANT.SELECT_PAPER,true);
          this.slimBarService.completeLoading();
          return;
        }
        
      }
      for (let thread of this.threadList) {
        if(this.myForm.controls['combos']['controls'][i].controls.thread.value == ""){
          this.Modal(CONSTANT.VALIDATION_ERROR,CONSTANT.SELECT_THREAD,true);
          this.slimBarService.completeLoading();
          return;
        }
        
      }
    }

    console.log(order);
    this.orderService.add(formData).subscribe((res: ResObj<Order>) => {
      console.log(res.obj);
      this.slimBarService.completeLoading();
      this.Modal(res.header, res.description, true);
      this.router.navigate([CONSTANT.BACK_TO_ORDERS]);
    },
      (err: ErrorResponse) => {
        this.slimBarService.completeLoading();
        this.Modal(err.header, err.detailedDescription + "<br><br>" + err.description + "", true);
        //console.log(err.description);
      }
    );
  }


  private Modal(header: String, description: String, isCloseModal: boolean) {
    this.activeModal = this.modalService.open(ModalComponent, { size: 'lg' });
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }

  back() {
    this.router.navigate([CONSTANT.BACK_TO_ORDERS]);
  }

  _onUpload($event){}

}
