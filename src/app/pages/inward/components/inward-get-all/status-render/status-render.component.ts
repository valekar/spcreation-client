import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-status-render',
  templateUrl: './status-render.component.html',
  styleUrls: ['./status-render.component.scss']
})
export class StatusRenderComponent implements OnInit {

  @Input() rowData;
  constructor() { }

  ngOnInit() {
  }

}
