import { Routes, RouterModule }  from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { OrderComponent } from './order.component';
import { OrderDetailsComponent} from './components/order-details/order-details.component';
import {OrderGetAllComponent} from './components/order-get-all/order-get-all.component';
import {OrderAddComponent} from './components/order-add/order-add.component';
import {OrderEditComponent} from './components/order-edit/order-edit.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: OrderComponent,
    children:[
        { path: '', component: OrderGetAllComponent },
        { path: 'add', component:OrderAddComponent},
        { path: ':id/edit', component:OrderEditComponent},
      { path: ':id', component: OrderDetailsComponent }
        
        
        
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
