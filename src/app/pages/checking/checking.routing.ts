import { Routes, RouterModule } from '@angular/router';
import { CheckingComponent } from './checking.component';
import { PlanningComponent } from './components/planning/planning.component';
import { InCheckingComponent } from './components/in-checking/in-checking.component';
import { InCheckingDetailsComponent } from './components/in-checking-details/in-checking-details.component';
const routes: Routes = [
  {
    path: '',
    component: CheckingComponent,
    children: [
      { path: 'planning', component: PlanningComponent },
      { path: "in-checking", component: InCheckingComponent },
      { path: "in-checking/:id", component: InCheckingDetailsComponent }


    ]
  }
];

export const routing = RouterModule.forChild(routes);