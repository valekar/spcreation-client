import { Component, OnInit, ViewChild, Input, Output, EventEmitter, ElementRef, Renderer } from '@angular/core';
import { NgUploaderOptions } from 'ngx-uploader';
import { OrderService } from '../../services/order.service';
import { Customer } from '../../../models/company.model';
import { Thread, Applique, Paper } from '../../../models/user.model';
import { Order, Combo } from '../../../models/order.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SlimBarService } from '../../../services/slim-bar.service';
import { ModalComponent } from '../../../services/modal/modal.component';
import { Validators, FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { ResObj, SuccessReponseWithObj, SuccessResponse, ErrorResponse } from '../../../models/common.model';
import { Router, ActivatedRoute } from '@angular/router';
import { CONSTANT } from '../../../models/CONSTANTS';
import { ShareOrderDataService } from '../../services/share-order-data.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/zip';
import { DurationFilterPipe } from '../../services/duration-filter.pipe';

@Component({
  selector: 'app-order-edit',
  templateUrl: './order-edit.component.html',
  styleUrls: ['./order-edit.component.scss'],
  providers: [OrderService, SlimBarService],

})
export class OrderEditComponent implements OnInit {

  public myForm: FormGroup; // form model

  @Input() defaultPicture: string = 'assets/img/theme/no-photo.png';
  @Input() picture: string = '';

  @Input() uploaderOptions: NgUploaderOptions = { url: '' };
  @Input() canDelete: boolean = true;

  @Output() onUpload = new EventEmitter<any>();
  @Output() onUploadCompleted = new EventEmitter<any>();

  @ViewChild('fileUpload') public _fileUpload: ElementRef;

  threadList: Array<Thread>;
  appliqueList: Array<Applique>;
  customerList: Array<Customer>;
  paperList: Array<Paper>;
  activeModal: any;
  originalBlobFile: Blob;
  public uploadInProgress: boolean;

  //order id
  orderId: number;
  order: Order;
  sharedOrderData: Order;
  setOrderPromise: Promise<any>;
  constructor(private renderer: Renderer, private orderService: OrderService, private _fb: FormBuilder
    , private slimBarService: SlimBarService, private modalService: NgbModal, private router: Router,
    private activatedRoute: ActivatedRoute, private sharedService: ShareOrderDataService) {
    this.slimBarService.progress();

    // console.log("Shared data" + this.sharedService.getOrderData().designName);
  }

  ngOnInit() {
    this.slimBarService.startLoading();
    Observable.zip(
      this.orderService.getAllCustomers(),
      this.orderService.getAllPapers(),
      this.orderService.getAllAppliques(),
      this.orderService.getAllThreads(),

    ).subscribe(([customers, papers, appliques, threads]) => {
      this.customerList = <Array<Customer>>customers.obj;
      this.paperList = <Array<Paper>>papers.obj;
      this.appliqueList = <Array<Applique>>appliques.obj;
      this.threadList = <Array<Thread>>threads.obj;
      this.getOrderData().then(() => {
        this.slimBarService.completeLoading();
        this.setFormValues(this.order);
      });
    });

    this.createForm();
    //console.log(this.myForm);
  }


  createForm() {
    //console.log("as");
    // we will initialize our form here
    this.myForm = this._fb.group({
      customer: ['', [Validators.required]],
      grade: ['', [Validators.required, Validators.minLength(1)]],
      designName: ['', [Validators.required, Validators.minLength(1)]],
      embroideryType: ['', [Validators.required, Validators.minLength(1)]],
      durationHR: ['', Validators.required],
      durationMN: ['', Validators.required],
      durationSS: ['', Validators.required],
      quantity: ['', [Validators.required, Validators.minLength(1)]],
      fabricColor: ['', [Validators.required, Validators.minLength(1)]],
      fabricSize: ['', [Validators.required, Validators.minLength(1)]],
      stitches: ['', [Validators.required, Validators.minLength(1)]],
      part: ['', [Validators.required, Validators.minLength(1)]],
      position: ['', [Validators.required, Validators.minLength(1)]],
      measurement: ['', [Validators.required, Validators.minLength(1)]],
      noOfCombos: [''],
      combos: this._fb.array([
        //this.initCombo()
      ])
    });
  }

  setFormValues(order: Order) {
    this.myForm.controls['customer'].setValue(order.customer.id);
    this.myForm.controls['grade'].setValue(order.grade);
    this.myForm.controls["designName"].setValue(order.designName);
    this.myForm.controls["embroideryType"].setValue(order.embroideryType);
    this.myForm.controls["fabricSize"].setValue(order.fabric_size);
    this.myForm.controls["fabricColor"].setValue(order.fabricColor);
    this.myForm.controls["grade"].setValue(order.grade);
    this.myForm.controls["part"].setValue(order.part);
    this.myForm.controls["position"].setValue(order.position);
    this.myForm.controls["measurement"].setValue(order.measurement);
    this.myForm.controls["noOfCombos"].setValue(order.noOfCombos);
    this.myForm.controls["quantity"].setValue(order.quantity);
    this.myForm.controls["stitches"].setValue(order.stitches);
    //duration
    let duration = new DurationFilterPipe().getDurationArray(order.duration);
    //console.log(durationFilter);
    if (duration.length > 0) {
      this.myForm.controls["durationHR"].setValue(duration[0]);
      this.myForm.controls["durationMN"].setValue(duration[1]);
      this.myForm.controls["durationSS"].setValue(duration[2]);
    }
    else {
      this.myForm.controls["durationHR"].setValue(0);
      this.myForm.controls["durationMN"].setValue(0);
      this.myForm.controls["durationSS"].setValue(0);
    }

    if (this.order.noOfCombos > 0) {
      for (var i = 0; i < this.order.noOfCombos; i++) {
        this.addCombos();
        this.setComboValue(this.order.combos[i], i);
      }
    }
    this.setPicture(order.imageBytes);

  }



  getOrderData() {
    return new Promise((resolve, reject) => {
      this.sharedOrderData = this.sharedService.getOrderData();
      if (this.sharedOrderData == null) {
        this.activatedRoute.params.subscribe((urlParams: any) => {
          this.orderId = +urlParams.id
          this.orderService.get(this.orderId).subscribe((res: SuccessReponseWithObj<Order>) => {
            this.order = res.obj;
            //console.log(this.order);
            //this.slimBarService.completeLoading();
            resolve();
          },
            (err: ErrorResponse) => {
              //this.slimBarService.completeLoading();
            });
        });
      }
      else {
        this.activatedRoute.params.subscribe((urlParams: any) => {
          this.orderId = +urlParams.id
          this.order = this.sharedOrderData;
          resolve();
        });
      }
    });
  }


  comboChange() {
    //console.log(this.myForm.get('noOfCombos').value);
    let noOfCombos = this.myForm.get('noOfCombos').value
    this.clearCombos();
    if (this.myForm.get('noOfCombos').value > 0) {
      this.clearCombos();
      for (let i = 0; i < noOfCombos; i++) {
        //console.log(this.order.combos);
        this.addCombos();
        if (this.order.combos[i] != null) {
          this.setComboValue(this.order.combos[i], i);
        }
      }
    }

  }


  setComboValue(combo: Combo, i: number) {
    this.myForm.controls['combos']['controls'][i].controls.cones.setValue(combo.cones);
    this.myForm.controls['combos']['controls'][i].controls.boxes.setValue(combo.boxes);
    this.myForm.controls['combos']['controls'][i].controls.frameSize.setValue(combo.frameSize);
    this.myForm.controls['combos']['controls'][i].controls.layers.setValue(combo.layers);
    this.myForm.controls['combos']['controls'][i].controls.applique.setValue(combo.applique.id);
    this.myForm.controls['combos']['controls'][i].controls.paper.setValue(combo.paper.id);
    this.myForm.controls['combos']['controls'][i].controls.thread.setValue(combo.thread.id);
  }

  initCombo() {
    //initialize our combos
    return this._fb.group({
      cones: ['', Validators.required],
      boxes: ['', Validators.required],
      frameSize: ['', Validators.required],
      layers: ['', Validators.required],
      thread: ['', Validators.required],
      paper: ['', Validators.required],
      applique: ['', Validators.required]
    });
  }

  addCombos() {
    const control = <FormArray>this.myForm.controls['combos'];
    control.push(this.initCombo());

  }

  clearCombos() {
    //let control = <FormArray>this.myForm.controls['combos'];
    this.myForm.controls['combos'] = this._fb.array([]);
  }

  removeCombo(i: number) {
    const control = <FormArray>this.myForm.controls['combos'];
    control.removeAt(i);
  }

  //related to image showing and fetching
  beforeUpload(uploadingFile): void {
    let files = this._fileUpload.nativeElement.files;

    if (files.length) {
      const file = files[0];
      this._changePicture(file);

      if (!this._canUploadOnServer()) {
        uploadingFile.setAbort();
      } else {
        this.uploadInProgress = true;
      }
    }
  }

  bringFileSelector(): boolean {
    this.renderer.invokeElementMethod(this._fileUpload.nativeElement, 'click');
    return false;
  }

  removePicture(): boolean {
    this.picture = '';
    return false;
  }

  _changePicture(file: File): void {
    const reader = new FileReader();
    reader.addEventListener('load', (event: Event) => {
      this.picture = (<any>event.target).result;
    }, false);
    reader.readAsDataURL(file);
  }

  _canUploadOnServer(): boolean {
    return !!this.uploaderOptions['url'];
  }


  setPicture(base64String: string) {
    let byteCharacters = atob(base64String);
    let byteNumbers = new Array(byteCharacters.length);
    for (var i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    let byteArray = new Uint8Array(byteNumbers);

    let blob = new Blob([byteArray], { type: 'image/png' });
    const reader = new FileReader();
    reader.addEventListener('load', (event: Event) => {
      this.picture = (<any>event.target).result;
    }, false);
    reader.readAsDataURL(blob);
    this.originalBlobFile = blob;
  }


  private Modal(header: String, description: String, isCloseModal: boolean) {
    this.activeModal = this.modalService.open(ModalComponent, { size: 'lg' });
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }

  back() {
    this.router.navigate([CONSTANT.BACK_TO_ORDERS + this.orderId]);
  }

  update(model: FormGroup) {
    //console.log(this.myForm.controls['customer']);
    //console.log(model);
    this.slimBarService.startLoading();
    let order = new Order();
    order.id = this.orderId;
    order.combos = model.controls["combos"].value;
    //order.customer = model.controls["customer"].value;
    order.designName = model.controls["designName"].value;
    order.embroideryType = model.controls["embroideryType"].value;
    order.fabric_size = model.controls["fabricSize"].value;
    order.fabricColor = model.controls["fabricColor"].value;
    order.grade = model.controls["grade"].value;
    order.part = model.controls["part"].value;
    order.position = model.controls["position"].value;
    order.measurement = model.controls["measurement"].value;
    order.noOfCombos = model.controls["noOfCombos"].value;
    order.quantity = model.controls["quantity"].value;
    order.stitches = model.controls["stitches"].value;
    //console.log(order);
    //this.orderService.add()
    let hour = model.controls["durationHR"].value;
    let minutes = model.controls["durationMN"].value;
    let seconds = model.controls["durationSS"].value;

    order.duration = hour * 3600 + minutes * 60 + seconds;
    //construct drop down values


    for (let customer of this.customerList) {
      if (customer.id == model.controls["customer"].value) {
        order.customer = customer;
      }
    }
    for (let i = 0; i < order.noOfCombos; i++) {
      for (let applique of this.appliqueList) {
        if(this.myForm.controls['combos']['controls'][i].controls.applique.value == ""){
          this.Modal(CONSTANT.VALIDATION_ERROR,CONSTANT.SELECT_APPLIQUE,true);
          this.slimBarService.completeLoading();
          return;
        }
        if (applique.id == this.myForm.controls['combos']['controls'][i].controls.applique.value) {
          order.combos[i].applique = applique;
        }
      }
      for (let paper of this.paperList) {
        if(this.myForm.controls['combos']['controls'][i].controls.paper.value == ""){
          this.Modal(CONSTANT.VALIDATION_ERROR,CONSTANT.SELECT_PAPER,true);
          this.slimBarService.completeLoading();
          return;
        }
        
        if (paper.id == this.myForm.controls['combos']['controls'][i].controls.paper.value) {
          order.combos[i].paper = paper;
        }
      }
      for (let thread of this.threadList) {
        if(this.myForm.controls['combos']['controls'][i].controls.thread.value == ""){
          this.Modal(CONSTANT.VALIDATION_ERROR,CONSTANT.SELECT_THREAD,true);
          this.slimBarService.completeLoading();
          return;
        }
        if (thread.id == this.myForm.controls['combos']['controls'][i].controls.thread.value) {
          order.combos[i].thread = thread;
        }
      }
    }
    //set combo Id
    let lengthOfCombos = 0;
    if(this.order.noOfCombos == order.noOfCombos){
        lengthOfCombos = this.order.noOfCombos;
    }
    else if(this.order.noOfCombos<order.noOfCombos){
      lengthOfCombos = this.order.noOfCombos;
    }
    else{
      lengthOfCombos = order.noOfCombos;
    }
    for (let i = 0; i < lengthOfCombos; i++) {
      if(order.noOfCombos)
      order.combos[i].id = this.order.combos[i].id;
    }

    //let inputEl: HTMLInputElement = this._fileUpload.nativeElement;
    let files = this._fileUpload.nativeElement.files
    let fileCount: number = files.length;
    let formData = new FormData();
    console.log(order);
    if (fileCount > 0) { // a file was selected
      // for (let i = 0; i < fileCount; i++) {
      formData.append('file', files[0]);
      // }
    }
    else {
      //const reader = new FileReader();
      //reader.readAsDataURL(this.originalBlobFile);
      formData.append("file", this.originalBlobFile, this.order.image.name);

    }
    formData.append('order', new Blob([JSON.stringify(order)], { type: "application/json" }));

    this.orderService.edit(formData, order.id).subscribe((res: ResObj<Order>) => {
      console.log(res.obj);
      this.slimBarService.completeLoading();
      this.Modal(res.header, res.description, true);
      this.router.navigate([CONSTANT.BACK_TO_ORDERS]);
    },
      (err: ErrorResponse) => {
        this.slimBarService.completeLoading();
        this.Modal(err.header, err.detailedDescription + "<br><br>" + err.description + "", true);
        //console.log(err.description);
      }
    );
  }

  _onUpload($event){}
}
