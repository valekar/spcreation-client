import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { Ng2SmartTableModule} from 'ng2-smart-table';
import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import {routing} from './inward.routing';

import { InwardComponent } from './inward.component';
import {InwardService} from './services/inward.service';
import { InwardGetAllComponent } from './components/inward-get-all/inward-get-all.component';
import {SlimBarService} from '../services/slim-bar.service';
import { DateTimeEditorComponent } from './components/inward-get-all/date-time-editor/date-time-editor.component';

import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NumberEditorComponent } from './components/inward-get-all/number-editor/number-editor.component';
import { InwardOrderRenderComponent } from './components/inward-get-all/inward-order-render/inward-order-render.component';
import { InwardOrderEditorComponent } from './components/inward-get-all/inward-order-editor/inward-order-editor.component';
import {SharedDataService} from './services/shared-data.service';
import {TagService} from './services/tag.service';
import { TagComponent } from './components/tag/tag.component';
import { TagInwardLinkRenderComponent } from './components/tag-inward/tag-inward-link-render/tag-inward-link-render.component';
import { TagInwardComponent } from './components/tag-inward/tag-inward.component';
import { TagNumberEditorComponent } from './components/tag/tag-number-editor/tag-number-editor.component';
import { SPCRenderComponent } from './components/inward-get-all/spcrender/spcrender.component';
import { StatusRenderComponent } from './components/inward-get-all/status-render/status-render.component';
//import {SelectModule} from 'ng2-select';
//import { Ng2AutoCompleteModule } from 'ng2-auto-complete';

@NgModule({
  imports: [
    CommonModule, 
    SlimLoadingBarModule.forRoot(),
    routing,
    FormsModule,
    NgaModule,
    Ng2SmartTableModule,
    NgbDropdownModule,
    NgbModalModule,
    NguiDatetimePickerModule,
    //Ng2AutoCompleteModule
    //SelectModule
  ],
  providers:[InwardService,SlimBarService,SharedDataService,TagService],
  entryComponents:[DateTimeEditorComponent,NumberEditorComponent,
    InwardOrderEditorComponent,InwardOrderRenderComponent,TagInwardLinkRenderComponent,SPCRenderComponent,
    TagNumberEditorComponent,StatusRenderComponent],
  declarations: [InwardComponent, InwardGetAllComponent, DateTimeEditorComponent, 
    NumberEditorComponent, InwardOrderRenderComponent, 
    InwardOrderEditorComponent, TagComponent, TagInwardLinkRenderComponent, TagInwardComponent, TagNumberEditorComponent, SPCRenderComponent, StatusRenderComponent]
})
export class InwardModule { }
