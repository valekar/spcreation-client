import { Component, OnInit } from '@angular/core';
import {SuccessResponse,ErrorResponse,SuccessReponseWithObj} from '../../../models/user.model';
import {LocalDataSource } from 'ng2-smart-table';
import {CONSTANT} from '../../../models/CONSTANTS';
import {ModalComponent} from '../../../services/modal/modal.component';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {SlimBarService} from '../../../services/slim-bar.service';
import {Machine,ResObj} from '../../../models/user.model';
import {MachineService} from '../../services/machine.service';

@Component({
  selector: 'app-machine',
  templateUrl: './machine.component.html',
  styleUrls: ['./machine.component.scss'],
  providers:[SlimBarService,MachineService]
})
export class MachineComponent implements OnInit {


  settings:any;
  columns:any;
  activeModal:any;
  source:LocalDataSource;
  machineList:Array<Machine>;
  constructor(private machineService:MachineService, private slimBarService:SlimBarService,private modalService:NgbModal) {
    this.slimBarService.progress();
    this.columns =machineService.getColumns();
    this.settings = {
      actions:{
        add:true,
        edit:true,
        delete:true
      },
      hideSubHeader: false,
      add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
      edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
      delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
      columns:this.columns
    }
   }

  ngOnInit() {
    this.getAllMachines();
  }


  onCreateConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.NOTICE_CREATE, CONSTANT.CREATE_DESC,false);
    //this is the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.create(event.newData,event);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
  }


  onSaveConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.NOTICE_UPDATE, 
      CONSTANT.UPDATE_DESC + CONSTANT.STRONG_TAG_OPEN+ event.data.name + CONSTANT.STRONG_TAG_CLOSE + CONSTANT.QUESTION_MARK,
      false);
    //this the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.update(event.newData,event);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
  }

  onDeleteConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.WARNING, 
      CONSTANT.DELETE_WARNING_DESC + CONSTANT.STRONG_TAG_OPEN+ event.data.name + CONSTANT.STRONG_TAG_CLOSE+ CONSTANT.QUESTION_MARK,
      false);
    // this is the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.delete(event.data.id);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
    
  }

  //add/edit/delete/get Machines

  private create(machine:Machine,event){
    let validate = this.validation(machine);
    if(validate){
      this.slimBarService.startLoading();
      // console.log(user);
       this.machineService.add(machine)
       .subscribe((res:SuccessReponseWithObj<Machine>)=>{
         this.slimBarService.completeLoading();
         this.getAllMachines();
         this.Modal(res.header,res.description,true);
         //console.log(res);
         event.confirm.resolve();
       },
       (err:ErrorResponse)=>{
         this.slimBarService.completeLoading();
         this.Modal(err.header,err.description,true);
       }
     )
    }
    
  }

  private delete(id:number){
    this.slimBarService.startLoading();
     this.machineService.delete(id).subscribe(
       (res:SuccessResponse)=>{
         
        for(let i=0;i<this.machineList.length;i++){
          if(id == this.machineList[i].id){
            this.machineList.splice(i,1);
            break;
          } 
        } 
        this.source.load(this.machineList);
        this.slimBarService.completeLoading(); 
        this.Modal(res.header,res.description,true);      
       },
       (err:ErrorResponse) => {
        this.slimBarService.completeLoading();
        this.Modal(err.header,err.description,true);
       }
      ); 
  }

  private update(machine:Machine,event){
    let validate = this.validation(machine);
    if(validate){
      this.slimBarService.startLoading();
      this.machineService.edit(machine,machine.id)
      .subscribe((res:SuccessReponseWithObj<Machine>) => {
        this.slimBarService.completeLoading(); 
        this.Modal(res.header,res.description,true);
        console.log(res);
        event.confirm.resolve();
      },
      (err:ErrorResponse) => {
        //console.log(err);
        this.slimBarService.completeLoading();
        this.Modal(err.header,err.description,true);
       }
    );
    }    
  }
  
  private getAllMachines(){
    this.slimBarService.startLoading();
    this.machineService.getMachines().subscribe(
      (res:ResObj<Machine>)=>{
        this.machineList = res.obj;
        this.source = new LocalDataSource(this.machineList);
        this.slimBarService.completeLoading();
      },
      (err:ErrorResponse) => {}
    );
  }

  //end of add/edit/delete/get

  private Modal(header:String,description:String,isCloseModal:boolean){
    this.activeModal = this.modalService.open(ModalComponent, {size: 'sm'});
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }

  private validation(machine:Machine){
    if(isNaN(+machine.heads)){
      this.Modal(CONSTANT.VALIDATION_ERROR, CONSTANT.MACHINE_VALIDATION_MESSAGE,true);
      return false;
    }
    return true;
  }

}
