import { Component, OnInit } from '@angular/core';
import {ErrorResponse,SuccessReponseWithObj,ResObj,SuccessResponse} from '../../../models/common.model';
import {LocalDataSource } from 'ng2-smart-table';
import {CONSTANT} from '../../../models/CONSTANTS';
import {ModalComponent} from '../../../services/modal/modal.component';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {SlimBarService} from '../../../services/slim-bar.service';
import {ThreadService} from '../../services/thread.service';
import {Thread} from '../../../models/user.model';

@Component({
  selector: 'app-thread',
  templateUrl: './thread.component.html',
  styleUrls: ['./thread.component.scss'],
  providers:[ThreadService,SlimBarService]
})
export class ThreadComponent implements OnInit {
  
  settings:any;
  columns:any;
  activeModal:any;
  source:LocalDataSource;
  threadList:Array<Thread>;
  constructor(private threadService:ThreadService, private slimBarService:SlimBarService,private modalService:NgbModal) {
    this.slimBarService.progress();
    this.columns =threadService.getColumns();
    this.settings = {
      actions:{
        add:true,
        edit:true,
        delete:true
      },
      hideSubHeader: false,
      add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
      edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
      delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
      columns:this.columns
    }
  }

  ngOnInit() {
    this.getAllThreads();
  }

  onCreateConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.NOTICE_CREATE, CONSTANT.CREATE_DESC,false);
    //this is the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.create(event.newData,event);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
  }


  onSaveConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.NOTICE_UPDATE, 
      CONSTANT.UPDATE_DESC + CONSTANT.STRONG_TAG_OPEN+ event.data.brandName + CONSTANT.STRONG_TAG_CLOSE + CONSTANT.QUESTION_MARK,
      false);
    //this the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.update(event.newData,event);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
  }

  onDeleteConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.WARNING, 
      CONSTANT.DELETE_WARNING_DESC + CONSTANT.STRONG_TAG_OPEN+ event.data.brandName + CONSTANT.STRONG_TAG_CLOSE+ CONSTANT.QUESTION_MARK,
      false);
    // this is the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.delete(event.data.id);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
    
  }



  //add/edit/delete/get threads
 private create(thread:Thread,event){
  this.slimBarService.startLoading();
 // console.log(user);
  this.threadService.add(thread)
  .subscribe((res:SuccessReponseWithObj<Thread>)=>{
    this.slimBarService.completeLoading();
    this.getAllThreads();
    this.Modal(res.header,res.description,true);
    //console.log(res);
    event.confirm.resolve();
  },
  (err:ErrorResponse)=>{
    this.slimBarService.completeLoading();
    this.Modal(err.header,err.description,true);
  }
)
}

private delete(id:number){
  this.slimBarService.startLoading();
   this.threadService.delete(id).subscribe(
     (res:SuccessResponse)=>{
       
      for(let i=0;i<this.threadList.length;i++){
        if(id == this.threadList[i].id){
          this.threadList.splice(i,1);
          break;
        } 
      } 
      this.source.load(this.threadList);
      this.slimBarService.completeLoading(); 
      this.Modal(res.header,res.description,true);      
     },
     (err:ErrorResponse) => {
      this.slimBarService.completeLoading();
      this.Modal(err.header,err.description,true);
     }
    ); 
}

private update(thread:Thread,event){
  this.slimBarService.startLoading();
    this.threadService.edit(thread,thread.id)
    .subscribe((res:SuccessReponseWithObj<Thread>) => {
      this.slimBarService.completeLoading(); 
      this.Modal(res.header,res.description,true);
      console.log(res);
      event.confirm.resolve();
    },
    (err:ErrorResponse) => {
      //console.log(err);
      this.slimBarService.completeLoading();
      this.Modal(err.header,err.description,true);
     }
  ); 
}
private getAllThreads(){
  this.slimBarService.startLoading();
  this.threadService.getAll().subscribe(
    (res:ResObj<Thread>)=>{
      this.threadList = res.obj;
      this.source = new LocalDataSource(this.threadList);
      this.slimBarService.completeLoading();
    },
    (err:ErrorResponse) => {}
  );
}


//end of add/edit/delete/get
private Modal(header:String,description:String,isCloseModal:boolean){
  this.activeModal = this.modalService.open(ModalComponent, {size: 'sm'});
  this.activeModal.componentInstance.modalHeader = header;
  this.activeModal.componentInstance.modalContent = description;
  this.activeModal.componentInstance.isCloseModal = isCloseModal;
}


}
