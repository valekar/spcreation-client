import { Injectable } from '@angular/core';
import {CONSTANT} from '../../models/CONSTANTS';
import {Response} from '@angular/http'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import {CommonService} from '../../services/common.service';
import {URLS} from '../../models/URLS';
import {Order} from '../../models/order.model';
import {AnchorViewComponent} from '../components/order-get-all/anchor-view/anchor-view.component';
import {ImageRenderComponent } from '../components/order-get-all/image-render/image-render.component';
import { CustomerRenderComponent } from '../components/order-get-all/customer-render/customer-render.component';
import {CustomHttp} from '../../../app-error-interceptor.service';
@Injectable()
export class OrderService {

  constructor(private http:CustomHttp,private commonService:CommonService) { }

  getCount(){
    return this.http.get(URLS.ORDER_COUNT_URL,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  getAll(pageNumber:number,pageSize:number){
    return this.http.get(URLS.ORDER_PAGE_URL+"page="+pageNumber+"&size="+pageSize, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  get(id:number){
    return this.http.get(URLS.ORDER_URL + id, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  //changed to add formData
  add(formData:FormData){
    return this.http.post(URLS.ORDER_URL,formData,this.commonService.getUndefinedOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  edit(formData:FormData,id:number){
    return this.http.put(URLS.ORDER_URL+id,formData,this.commonService.getUndefinedOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  delete(id:number){
    return this.http.delete(URLS.ORDER_URL+id,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  getAllCustomers(){
    return this.http.get(URLS.CUSTOMER_URL,this.commonService.getHeaderOptions())
    .map((res:Response)=>{return res.json()})
    .catch((err:Response)=>{return Observable.throw(err.json())});
  }

  getAllThreads(){
    return this.http.get(URLS.THREAD_URL,this.commonService.getHeaderOptions())
    .map((res:Response)=>{return res.json()})
    .catch((err:Response)=>{return Observable.throw(err.json())});
  }

  getAllPapers(){
    return this.http.get(URLS.PAPER_URL,this.commonService.getHeaderOptions())
    .map((res:Response)=>{return res.json()})
    .catch((err:Response)=>{return Observable.throw(err.json())});
  }

  getAllAppliques(){
    return this.http.get(URLS.APPLIQUE_URL,this.commonService.getHeaderOptions())
    .map((res:Response)=>{return res.json()})
    .catch((err:Response)=>{return Observable.throw(err.json())});
  }


  getOnlyApprovedOrders(page,size){
    return this.http.get(URLS.APPROVED_ORDERS_URL+"?page="+page+"&size="+size,this.commonService.getHeaderOptions())
    .map((res:Response)=>{return res.json()})
    .catch((err:Response)=>{return Observable.throw(err.json())});
  }

  getColumns(){
    return { 
     spc:{
      title:"SPC #",
      type:"text",
      width:"10%"
      },
    imageBytes:{
      title:"Image", 
      type:'custom',
      renderComponent:ImageRenderComponent,
      filter:false,
      width:"8%"
     },
     customer:{
      title:"Customer Name",
      type:"custom",
      renderComponent:CustomerRenderComponent,
      width:"12%"
     },
     grade:{
      title:"Grade", 
      type:"text",
      width:"8%"
     },
     designName:{
       title:"Design Name",
       type:"text",
       width:"10%"
     },
     quantity:{
       title:"Quantity",
       type:"text",
       width:"10%"
     },
      part:{
       title:"Part",
       type:"text",
       width:"10%"
     },
     position:{
      title:"Emd Position",
      type:"text",
      width:"10%"
    },
    measurement:{
      title:"Measurement",
      type:"text",
      width:"10%"
    },
     sam:
     {
      title:"Sample #",
      type:"text",
      width:"10%"
     },
     status:
     {
      title:"Status",
      type:"html",
      valuePrepareFunction(cell,row){
        return `<div class="text-uppercase" ><i>${cell}</i></div>`;
      },
      width:"10%"
     },
     Actions: //or something
     {
       title:'Show Details',
       type:'custom',
       renderComponent:AnchorViewComponent,
       filter:false,
       width:"10%"       
     }
    }
  }
}
