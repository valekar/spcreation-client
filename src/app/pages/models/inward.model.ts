import {Order} from './order.model';
import {CONSTANT} from './CONSTANTS';
export class Inward {
    id:number=null;
    layStr:string;
    lay:string;
    inwardNumber:number;
    order:Order=null;
    tags:Array<Tag>;
}

export class Tag {
    id:number;
    tagId:string;
    size:string;
    bundle:number;
    quantity:number;
    comments:string;
    inward:Inward;
    status:string = CONSTANT.STATUS_PENDING;
    quantityCompleted:number = 0;
    mistakes:number;
    checkingQuantityCompleted:number;
    checkingMistakes:number;
    //I defined it here
    stitches:number=0;



}