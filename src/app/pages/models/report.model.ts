export class OutwardReport{
    lay:string;
    part:string;
    SPC:string;
    totalTags:number ;
    totalClothes:number;
    totalCheckingMistakes:number;
    totalProdMistakes:number;
    totalProduced:number;
    toBeProdBalanceClothes:number;
    toBeCheckBalanceClothes:number;
    totalBundles:number;
    totalChecked:number;
    orderStatus:string;
    updated_by:string;
    formatted_updated_at:string;
}