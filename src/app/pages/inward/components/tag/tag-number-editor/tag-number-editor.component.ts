import { Component, OnInit } from '@angular/core';
import {DefaultEditor,Editor,Cell} from 'ng2-smart-table';
import {TagService} from '../../../services/tag.service';

@Component({
  selector: 'app-tag-number-editor',
  templateUrl: './tag-number-editor.component.html',
  styleUrls: ['./tag-number-editor.component.scss'],
})
export class TagNumberEditorComponent extends DefaultEditor implements OnInit {

  isFieldNumber:boolean = true; 
  currentCellValue:any;
  constructor() { 
    super();
  }

  ngOnInit() {
    if(this.cell.getId() == 'tagId'){
      this.isFieldNumber = false;
    }

    if (this.cell.getValue() == "" || this.cell.getValue() == undefined || this.cell.getValue() == null) {
      //console.log(this.cell.getId());
        if(this.isFieldNumber){
          this.currentCellValue = 0;
        }
        else{
          this.currentCellValue = "Auto generated";
        }
      
      //this.currentCellValue = 0;
      this.cell.newValue = this.currentCellValue;
    }
    else{
      this.currentCellValue = this.cell.getValue();
    }

  }

  numberChange(){
    this.cell.newValue = this.currentCellValue;
  }

}
