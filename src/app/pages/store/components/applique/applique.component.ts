import { Component, OnInit } from '@angular/core';
import {SuccessResponse,ErrorResponse,SuccessReponseWithObj} from '../../../models/user.model';
import {LocalDataSource } from 'ng2-smart-table';
import {CONSTANT} from '../../../models/CONSTANTS';
import {ModalComponent} from '../../../services/modal/modal.component';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {SlimBarService} from '../../../services/slim-bar.service';
import {AppliqueService} from '../../services/applique.service';
import {Applique,AppliqueResObj} from '../../../models/user.model';
//import {} from '../../models/store.model';

@Component({
  selector: 'app-applique',
  templateUrl: './applique.component.html',
  styleUrls: ['./applique.component.scss'],
  providers:[AppliqueService,SlimBarService]
})
export class AppliqueComponent implements OnInit {

  settings:any;
  columns:any;
  activeModal:any;
  source:LocalDataSource;
  appliqueList:Array<Applique>;

  constructor(private appliqueService:AppliqueService,private slimBarService:SlimBarService,private modalService:NgbModal) {
    this.slimBarService.progress();
    this.columns =appliqueService.getAppliqueColumns();
    this.settings = {
      actions:{
        add:true,
        edit:true,
        delete:true
      },
      hideSubHeader: false,
      add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
      edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
      delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
      columns:this.columns
    }
   }


  ngOnInit() {
    this.getAppliques();
  }

   onCreateConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.NOTICE_CREATE, CONSTANT.CREATE_DESC,false);
    //this is the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.createApplique(event.newData,event);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
  }


  onSaveConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.NOTICE_UPDATE, 
      CONSTANT.UPDATE_DESC + CONSTANT.STRONG_TAG_OPEN+ event.data.number + CONSTANT.STRONG_TAG_CLOSE + CONSTANT.QUESTION_MARK,
      false);
    //this the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.updateApplique(event.newData,event);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
  }

  onDeleteConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.WARNING, 
      CONSTANT.DELETE_WARNING_DESC + CONSTANT.STRONG_TAG_OPEN+ event.data.number + CONSTANT.STRONG_TAG_CLOSE+ CONSTANT.QUESTION_MARK,
      false);
    // this is the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.deleteApplique(event.data.id);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
    
  }


  private createApplique(applique:Applique,event){
    if(this.validation(applique)){
      this.slimBarService.startLoading();
      // console.log(user);
       this.appliqueService.addApplique(applique)
       .subscribe((res:SuccessReponseWithObj<Applique>)=>{
         this.slimBarService.completeLoading();
         this.getAppliques();
         this.Modal(res.header,res.description,true);
         //console.log(res);
         event.confirm.resolve();
       },
       (err:ErrorResponse)=>{
         this.slimBarService.completeLoading();
         this.Modal(err.header,err.description,true);
       }
     );
    }
    
  }

  private deleteApplique(id:string){
    this.slimBarService.startLoading();
     this.appliqueService.deleteApplique(id).subscribe(
       (res:SuccessResponse)=>{
         
        for(let i=0;i<this.appliqueList.length;i++){
          if(id == this.appliqueList[i].id.toString()){
            this.appliqueList.splice(i,1);
            break;
          } 
        } 
        this.source.load(this.appliqueList);
        this.slimBarService.completeLoading(); 
        this.Modal(res.header,res.description,true);      
       },
       (err:ErrorResponse) => {
        this.slimBarService.completeLoading();
        this.Modal(err.header,err.description,true);
       }
      ); 
  }

  private updateApplique(applique:Applique,event){
    if(this.validation(applique)){
      this.slimBarService.startLoading();
      this.appliqueService.editApplique(applique,applique.id)
      .subscribe((res:SuccessReponseWithObj<Applique>) => {
        this.slimBarService.completeLoading(); 
        this.Modal(res.header,res.description,true);
        console.log(res);
        event.confirm.resolve();
      },
      (err:ErrorResponse) => {
        //console.log(err);
        this.slimBarService.completeLoading();
        this.Modal(err.header,err.description,true);
       }
    );
    }
     
  }



  private Modal(header:String,description:String,isCloseModal:boolean){
    this.activeModal = this.modalService.open(ModalComponent, {size: 'sm'});
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }


  private getAppliques(){
    this.slimBarService.startLoading();
    this.appliqueService.getAppliques().subscribe((res:AppliqueResObj)=>{
      console.log(res);
      this.appliqueList = res.obj;
      this.source = new LocalDataSource(this.appliqueList);
      this.slimBarService.completeLoading();
    },
    (err:ErrorResponse) => {console.log(err)}
    );
  }

  private validation(applique:Applique){
    if(isNaN(+applique.number)){
      this.Modal(CONSTANT.VALIDATION_ERROR, CONSTANT.APPLIQUE_VALIDATION_MESSAGE,true);
      return false;
    }
    return true;
  }
}
