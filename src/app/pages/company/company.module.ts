import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { Ng2SmartTableModule} from 'ng2-smart-table';
import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

import {routing} from './company.routing';
import {CompanyComponent} from './company.component';
import { DepartmentComponent } from './components/department/department.component';
import { RateComponent } from './components/rate/rate.component';
import { CustomerComponent } from './components/customer/customer.component';
import { EmployeeComponent } from './components/employee/employee.component';
import { MultiSelectEditorComponent } from './components/employee/multi-select-editor/multi-select-editor.component';
import { MultiSelectViewerComponent } from './components/employee/multi-select-viewer/multi-select-viewer.component';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { DropDownEditorComponent } from './components/department/drop-down-editor/drop-down-editor.component';
import {DepartmentService} from './services/department.service';

@NgModule({
  imports: [
    CommonModule,
    SlimLoadingBarModule.forRoot(),
    routing,
    FormsModule,
    NgaModule,
    Ng2SmartTableModule,
    NgbDropdownModule,
    NgbModalModule,
    MultiselectDropdownModule
  ],
  providers:[DepartmentService],
  declarations: [DepartmentComponent,CompanyComponent, RateComponent,
     CustomerComponent, EmployeeComponent, MultiSelectEditorComponent, MultiSelectViewerComponent, DropDownEditorComponent],
  entryComponents:[MultiSelectEditorComponent,MultiSelectViewerComponent,DropDownEditorComponent]
  
})
export class CompanyModule { }
