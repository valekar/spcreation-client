import { Routes, RouterModule }  from '@angular/router';
import {ReportsComponent} from './reports.component'
import {OutwardComponent} from './components/outward/outward.component';
import {OutwardDetailsComponent} from './components/outward-details/outward-details.component';
const routes: Routes = [
    {
      path: '',
      component:   ReportsComponent,
       children: [
         { path: '', component: OutwardComponent },
         { path: 'outward/:id', component: OutwardDetailsComponent },
      ]
    }
  ];

export const routing = RouterModule.forChild(routes);