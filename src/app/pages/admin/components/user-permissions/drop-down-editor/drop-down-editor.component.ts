import { Component, OnInit, ViewChild,ElementRef,AfterViewInit,Input } from '@angular/core';
import { Cell, DefaultEditor,Editor } from 'ng2-smart-table';
import {CONSTANT} from '../../../../models/CONSTANTS';
import {ModifiedUser} from '../../../../models/user.model';

@Component({
  selector: 'app-drop-down-editor',
  templateUrl: './drop-down-editor.component.html',
  styleUrls: ['./drop-down-editor.component.scss']
})
export class DropDownEditorComponent extends DefaultEditor implements OnInit,AfterViewInit {
  
  selectedValue:string ;
  dropDownList:any ;
  @ViewChild('selectedViewChild') selectedViewChild : ElementRef;


  ngAfterViewInit(): void {
    
  }

  constructor() {
    super();
   }

   //this is doing nothing, before removing check once
   updateValue($event){
    //console.log($event);
    this.cell.newValue = this.selectedValue;
    //console.log(this.cell.newValue);
    this.onClick.emit($event)
   }

  ngOnInit() {
    if(this.cell.getId() == CONSTANT.ENABALED_COLUMN){
      if(this.cell.getValue() == ''){
        this.selectedValue = CONSTANT.TRUE;
        this.cell.newValue = CONSTANT.TRUE;
        //this.cell.
      }
      else{
        this.selectedValue = this.cell.getValue();
      }
      this.dropDownList = CONSTANT.ENABLED_LIST;
    }
    else{
      if(this.cell.getValue() == ''){
        this.selectedValue = CONSTANT.NONE;
        this.cell.newValue = CONSTANT.NONE;
      }
      else{
        this.selectedValue = this.cell.getValue();
      }
      this.dropDownList = CONSTANT.PRIVILEGE_LIST;
    }
  }


}
