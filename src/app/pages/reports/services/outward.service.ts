import { Injectable } from '@angular/core';
import {CustomHttp} from '../../../app-error-interceptor.service';
import {CONSTANT} from '../../models/CONSTANTS';
import {Response} from '@angular/http'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import {CommonService} from '../../services/common.service';
import {URLS} from '../../models/URLS';
import {AnchorRenderComponent} from '../components/outward/anchor-render/anchor-render.component';

@Injectable()
export class OutwardService {

  constructor(private http:CustomHttp,private commonService:CommonService) { }


  getAllCustomers(){
    return this.http.get(URLS.CUSTOMER_URL,this.commonService.getHeaderOptions())
     .map((res:Response)=>{return res.json()})
     .catch((err:Response)=>{return Observable.throw(err.json())});
  }

  getInwardsFromCustomerId(id:number){
    return this.http.get(URLS.REPORTS_INWARD_CUSTOMER_URL+id,this.commonService.getHeaderOptions())
    .map((res:Response)=>{return res.json()})
    .catch((err:Response)=>{return Observable.throw(err.json())});
  }

  getCustomerColumns(){
    return {
      name:{
        title:"Name of Customer",
        type:"text"
      },
      address:{
        title:"Address",
        
      },
      updated_by:{
        title:'Updated By',
        type:'text',
        editable:false
      },
      formatted_updated_at:{
        title :"Updated At",
        type:"text",
        editable:false,
      },
      Action:{
        title:"Get Report",
        type:"custom",
        renderComponent:AnchorRenderComponent
      }
    }
  }

  getOutwardColumns(){
    return {
      spc:{
        title:"SPC",
        type:"text"
      },
      part:{
        title:"Part",
        type:"text"
      },
      totalBundles:{
        title:"Total Bundles",
        type:"text",
      },
      totalTags:{
        title:"Total Tags",
        type:"text",
      },
      totalClothes:{
        title:"Quantity",
        type:"text"
      },
      totalCheckingMistakes:{
        title:"Mistakes",
        type:"text"
      },
      totalProduced:{
        title:"Produced",
        type:"text"
      },
      totalChecked:{
        title:"Checked",
        type:"text"
      },
      toBeProdBalanceClothes:{
        title:"Prod Balance",
        type:"text"
      },
      toBeCheckBalanceClothes:{
        title:"Check Balance",
        type:"text"
      },
      updated_by:{
        title:'Updated By',
        type:'text'
      },
      formatted_updated_at:{
        title :"Updated At",
        type:"text"
      },
    }
  }

}
