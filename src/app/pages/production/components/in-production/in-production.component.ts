import { Component, OnInit } from '@angular/core';
import { ModalComponent } from '../../../services/modal/modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SlimBarService } from '../../../services/slim-bar.service';
import { ErrorResponse, SuccessReponseWithObj, ResObj, SuccessResponse } from '../../../models/common.model';
import { LocalDataSource } from 'ng2-smart-table';
import { CONSTANT } from '../../../models/CONSTANTS';
import {InProductionService} from '../../services/in-production.service';
import {InProduction} from '../../../models/production.model';
@Component({
  selector: 'app-in-production',
  templateUrl: './in-production.component.html',
  styleUrls: ['./in-production.component.scss'],
  providers:[InProductionService]
})
export class InProductionComponent implements OnInit {


  activeModal: any;
  source: LocalDataSource;
  inProdList: Array<InProduction>;
  settings: any;
  columns: any;
  constructor(private inProductionService:InProductionService, private slimBarService: SlimBarService,
    private modalService: NgbModal) {
      this.slimBarService.progress();
      this.columns = inProductionService.getColumns();
      this.settings = {
        actions: {
          add: false,
          edit: false,
          delete: false
        },
        hideSubHeader: false,
        add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
        edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
        delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
        columns: this.columns
      }
     }

  ngOnInit() {
    this.getAllProductions();
  }


  getAllProductions() {
    this.slimBarService.startLoading();
    this.inProductionService.getAll().subscribe((res: ResObj<InProduction>) => { 
      this.inProdList = res.obj;
      this.source = new LocalDataSource(this.inProdList);
      this.slimBarService.completeLoading();
    });
  }

  private Modal(header: String, description: String, isCloseModal: boolean) {
    this.activeModal = this.modalService.open(ModalComponent, { size: 'sm' });
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }
}
