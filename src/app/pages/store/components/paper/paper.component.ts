import { Component, OnInit } from '@angular/core';
import {SuccessResponse,ErrorResponse,SuccessReponseWithObj} from '../../../models/user.model';
import {LocalDataSource } from 'ng2-smart-table';
import {CONSTANT} from '../../../models/CONSTANTS';
import {ModalComponent} from '../../../services/modal/modal.component';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {SlimBarService} from '../../../services/slim-bar.service';
import {Paper,PaperResObj} from '../../../models/user.model';
import {PaperService} from '../../services/paper.service';
@Component({
  selector: 'app-paper',
  templateUrl: './paper.component.html',
  styleUrls: ['./paper.component.scss'],
  providers:[PaperService,SlimBarService]
})
export class PaperComponent implements OnInit {

  settings:any;
  columns:any;
  activeModal:any;
  source:LocalDataSource;
  paperList:Array<Paper>;
  constructor(private paperService:PaperService,private slimBarService:SlimBarService,private modalService:NgbModal) {
    this.slimBarService.progress();
    this.columns =paperService.getColumns();
    this.settings = {
      actions:{
        add:true,
        edit:true,
        delete:true
      },
      hideSubHeader: false,
      add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
      edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
      delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
      columns:this.columns
    }
   }

  ngOnInit() {
    this.getAllPapers();
  }

  onCreateConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.NOTICE_CREATE, CONSTANT.CREATE_DESC,false);
    //this is the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.create(event.newData,event);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
  }


  onSaveConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.NOTICE_UPDATE, 
      CONSTANT.UPDATE_DESC + CONSTANT.STRONG_TAG_OPEN+ event.data.id + CONSTANT.STRONG_TAG_CLOSE + CONSTANT.QUESTION_MARK,
      false);
    //this the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.update(event.newData,event);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
  }

  onDeleteConfirm(event){
    //console.log(event);
    this.Modal(CONSTANT.WARNING, 
      CONSTANT.DELETE_WARNING_DESC + CONSTANT.STRONG_TAG_OPEN+ event.data.id + CONSTANT.STRONG_TAG_CLOSE+ CONSTANT.QUESTION_MARK,
      false);
    // this is the event from modal
    this.activeModal.result.then((result:boolean)=> {
      this.delete(event.data.id);  
    }, 
    (reason:boolean) => {
      //console.log(reason)
    });
    
  }


  //add/edit/delete
  private create(paper:Paper,event){
    if(this.validation(paper)){
      this.slimBarService.startLoading();
      // console.log(user);
       this.paperService.addPaper(paper)
       .subscribe((res:SuccessReponseWithObj<Paper>)=>{
         this.slimBarService.completeLoading();
         this.getAllPapers();
         this.Modal(res.header,res.description,true);
         //console.log(res);
         event.confirm.resolve();
       },
       (err:ErrorResponse)=>{
         this.slimBarService.completeLoading();
         this.Modal(err.header,err.description,true);
       }
     );
    }
  }

  private delete(id:number){
    this.slimBarService.startLoading();
     this.paperService.deletePaper(id).subscribe(
       (res:SuccessResponse)=>{
         
        for(let i=0;i<this.paperList.length;i++){
          if(id == this.paperList[i].id){
            this.paperList.splice(i,1);
            break;
          } 
        } 
        this.source.load(this.paperList);
        this.slimBarService.completeLoading(); 
        this.Modal(res.header,res.description,true);      
       },
       (err:ErrorResponse) => {
        this.slimBarService.completeLoading();
        this.Modal(err.header,err.description,true);
       }
      ); 
  }

  private update(paper:Paper,event){
    if(this.validation(paper)){
      this.slimBarService.startLoading();
      this.paperService.editPaper(paper,paper.id)
      .subscribe((res:SuccessReponseWithObj<Paper>) => {
        this.slimBarService.completeLoading(); 
        this.Modal(res.header,res.description,true);
        console.log(res);
        event.confirm.resolve();
      },
      (err:ErrorResponse) => {
        //console.log(err);
        this.slimBarService.completeLoading();
        this.Modal(err.header,err.description,true);
       }
    );
    } 
  }

  private getAllPapers(){
    this.slimBarService.startLoading();
    this.paperService.getPapers().subscribe(
      (res:PaperResObj)=>{
        this.paperList = res.obj;
        this.source = new LocalDataSource(this.paperList);
        this.slimBarService.completeLoading();
      },
      (err:ErrorResponse) => {}
    )
  }
  //end of add/edit/delete


  private Modal(header:String,description:String,isCloseModal:boolean){
    this.activeModal = this.modalService.open(ModalComponent, {size: 'sm'});
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }

  private validation(paper:Paper){
    if(isNaN(+paper.size)){
      this.Modal(CONSTANT.VALIDATION_ERROR, CONSTANT.PAPER_VALIDATION_MESSAGE,true);
      return false;
    }
    return true;
  }
}
