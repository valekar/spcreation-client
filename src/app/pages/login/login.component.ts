import {Component} from '@angular/core';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { AuthenticationService} from '../../services/authentication.service';
import { User} from '../models/user.model';
import { Router} from '@angular/router';
import {SlimBarService} from '../services/slim-bar.service';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {ModalComponent} from '../services/modal/modal.component';
import {CONSTANT} from '../models/CONSTANTS';
import {URLS} from '../models/URLS'; 
@Component({
  selector: 'login',
  templateUrl: './login.html',
  styleUrls: ['./login.scss']
  
})

export class Login {

  public form:FormGroup;
  public email:AbstractControl;
  public password:AbstractControl;
  public submitted:boolean = false;


  constructor(fb:FormBuilder, private authenticationService:AuthenticationService, private router : Router,
    private slimBar :SlimBarService,private modalService: NgbModal) {
    //this.slimBar.startLoading();
    this.form = fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
    });

    this.email = this.form.controls['email'];
    this.password = this.form.controls['password'];
  }

  public onSubmit(values:User):void {
    this.submitted = true;
    this.slimBar.startLoading();
    if (this.form.valid) {
      
      // your code goes here
      this.authenticationService.login(values.email, values.password)
    .subscribe((result:any) =>  { 
      this.slimBar.completeLoading();
      //console.log("loggin  in");
      if(localStorage.getItem('currentUser')){
        let user = JSON.parse(localStorage.getItem('currentUser'));
        
        if(user.access_token){
          //this.commonService.isSourceActive(true);  
          this.router.navigate([URLS.PAGES_ORDER_URL]);                    
        }
      }
      else {
        this.router.navigate([URLS.ROOT_URL]);
        this.slimBar.completeLoading();
      }
      
    },
    (err:any)=> {
      //console.log(err.json());
      this.Modal(CONSTANT.UNAUTH_HEADER,CONSTANT.UNAUTH_DESCRITPION);
      this.slimBar.completeLoading();

    });
    }
  }


  private Modal(header:String,description:String){
    const activeModal = this.modalService.open(ModalComponent, {size: 'lg'});
          activeModal.componentInstance.modalHeader = header;
          activeModal.componentInstance.modalContent = description;
  }
}
