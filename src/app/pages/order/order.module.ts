import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing }       from './order.routing';
import { Ng2SmartTableModule} from 'ng2-smart-table';
import { NgaModule } from '../../theme/nga.module';
import {OrderComponent} from './order.component';
import {FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { OrderGetAllComponent } from './components/order-get-all/order-get-all.component';
import { OrderDetailsComponent } from './components/order-details/order-details.component';
import { AnchorViewComponent } from './components/order-get-all/anchor-view/anchor-view.component';
import { OrderAddComponent } from './components/order-add/order-add.component';
import { NgUploaderModule } from 'ngx-uploader';
import { ImageRenderComponent } from './components/order-get-all/image-render/image-render.component';
import { CustomerRenderComponent } from './components/order-get-all/customer-render/customer-render.component';
import { DurationFilterPipe } from './services/duration-filter.pipe';
import { OrderEditComponent } from './components/order-edit/order-edit.component';
//import { TimeDurationPickerModule } from 'angular2-time-duration-picker';
@NgModule({
  imports: [
    CommonModule,
    routing,
    NgaModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    NgUploaderModule,
    SlimLoadingBarModule.forRoot(),
    //TimeDurationPickerModule
  ],
  declarations: [OrderComponent, OrderGetAllComponent, OrderDetailsComponent, AnchorViewComponent, 
    OrderAddComponent, ImageRenderComponent, CustomerRenderComponent, DurationFilterPipe, OrderEditComponent],
  entryComponents:[AnchorViewComponent,ImageRenderComponent,CustomerRenderComponent]
})
export class OrderModule { }
