import { Component, OnInit } from '@angular/core';
import { ErrorResponse, SuccessReponseWithObj, ResObj, SuccessResponse } from '../../../models/common.model';
import { LocalDataSource } from 'ng2-smart-table';
import { CONSTANT } from '../../../models/CONSTANTS';
import { ModalComponent } from '../../../services/modal/modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SlimBarService } from '../../../services/slim-bar.service';
import { InwardService } from '../../services/inward.service';
import { Inward } from '../../../models/inward.model';
import { Order } from '../../../models/order.model';
import { SharedDataService } from '../../services/shared-data.service';

@Component({
  selector: 'app-inward-get-all',
  templateUrl: './inward-get-all.component.html',
  styleUrls: ['./inward-get-all.component.scss']
})
export class InwardGetAllComponent implements OnInit {

  settings: any;
  columns: any;
  activeModal: any;
  source: LocalDataSource;
  inwardList: Array<Inward>;
  orderSize: number = 100;
  //selectedOrderSize:number=100;
  constructor(private inwardService: InwardService,
    private slimBarService: SlimBarService, private modalService: NgbModal, private sharedDataService: SharedDataService) {
    this.slimBarService.progress();
    this.columns = inwardService.getColumns();
    this.settings = {
      actions: {
        add: true,
        edit: true,
        delete: true
      },
      hideSubHeader: false,
      add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
      edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
      delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
      columns: this.columns
    }

    //set order size
    // this.sharedDataService.setOrderSize(this.orderSize);
  }
  ngOnInit() {
    this.sharedDataService.setOrderSize(this.orderSize);
    this.getAllInwards();

  }


  onCreateConfirm(event) {
    if (event.newData.order != "") {
      this.Modal(CONSTANT.NOTICE_CREATE, CONSTANT.CREATE_DESC, false);
      //this is the event from modal
      this.activeModal.result.then((result: boolean) => {
        //console.log(event.newData["layStr"]);
        this.create(event.newData, event);
      },
        (reason: boolean) => {
          //console.log(reason)
        });
    }

  }


  onSaveConfirm(event) {
    //console.log(event);
    if (event.newData.order != "") {
      if (event.newData.order.status != CONSTANT.STATUS_CONFIRMED) {
        this.Modal(CONSTANT.NOTICE_UPDATE,
          CONSTANT.UPDATE_DESC + CONSTANT.STRONG_TAG_OPEN + event.data.inwardNumber + CONSTANT.STRONG_TAG_CLOSE + CONSTANT.QUESTION_MARK,
          false);
        //this the event from modal
        this.activeModal.result.then((result: boolean) => {
          this.update(event.newData, event);
        },
          (reason: boolean) => {
            //console.log(reason)
          });
      }
      else {
        this.Modal(CONSTANT.VALIDATION_ERROR, CONSTANT.CANNOT_CHANGE_CONFIRMED_INWARDS, true);
      }
    }

  }

  onDeleteConfirm(event) {
    //console.log(event);
    if (event.data.order.status != CONSTANT.STATUS_CONFIRMED) {
      this.Modal(CONSTANT.WARNING,
        CONSTANT.DELETE_WARNING_DESC + CONSTANT.STRONG_TAG_OPEN + event.data.inwardNumber + CONSTANT.STRONG_TAG_CLOSE + CONSTANT.QUESTION_MARK,
        false);
      // this is the event from modal
      this.activeModal.result.then((result: boolean) => {
        this.delete(event.data.id);
      },
        (reason: boolean) => {
          //console.log(reason)
        });
    }
    else {
      this.Modal(CONSTANT.VALIDATION_ERROR, CONSTANT.CANNOT_CHANGE_CONFIRMED_INWARDS, true);
    }
  }

  //add/edit/delete/get rate
  private create(inward: Inward, event) {
    this.slimBarService.startLoading();
    // console.log(inward);
    this.inwardService.add(inward)
      .subscribe((res: SuccessReponseWithObj<Inward>) => {
        this.slimBarService.completeLoading();
        this.getAllInwards();
        this.Modal(res.header, res.description, true);
        //console.log(res);
        event.confirm.resolve();
      },
      (err: ErrorResponse) => {
        this.slimBarService.completeLoading();
        this.Modal(err.header, err.description, true);
      }
      )
  }

  private delete(id: number) {
    this.slimBarService.startLoading();
    this.inwardService.delete(id).subscribe(
      (res: SuccessResponse) => {

        for (let i = 0; i < this.inwardList.length; i++) {
          if (id == this.inwardList[i].id) {
            this.inwardList.splice(i, 1);
            break;
          }
        }
        this.source.load(this.inwardList);
        this.slimBarService.completeLoading();
        this.Modal(res.header, res.description, true);
      },
      (err: ErrorResponse) => {
        this.slimBarService.completeLoading();
        this.Modal(err.header, err.description, true);
      }
    );
  }

  private update(inward: Inward, event) {
    this.slimBarService.startLoading();
    this.inwardService.edit(inward, inward.id)
      .subscribe((res: SuccessReponseWithObj<Inward>) => {
        this.slimBarService.completeLoading();
        this.Modal(res.header, res.description, true);
        console.log(res);
        event.confirm.resolve();
      },
      (err: ErrorResponse) => {
        //console.log(err);
        this.slimBarService.completeLoading();
        this.Modal(err.header, err.description, true);
      }
      );
  }
  private getAllInwards() {
    this.slimBarService.startLoading();
    this.inwardService.getAll().subscribe(
      (res: ResObj<Inward>) => {
        this.inwardList = res.obj;
        this.source = new LocalDataSource(this.inwardList);
        this.slimBarService.completeLoading();
      },
      (err: ErrorResponse) => { }
    );
  }

  //end of add/edit/delete/get
  private Modal(header: String, description: String, isCloseModal: boolean) {
    this.activeModal = this.modalService.open(ModalComponent, { size: 'sm' });
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }

  orderSizeSubmit(e) {
    e.preventDefault();
    // this.orderSize = this.selectedOrderSize;
    console.log(this.orderSize);
    this.sharedDataService.setOrderSize(this.orderSize);
  }
}
