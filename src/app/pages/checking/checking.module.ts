import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckingComponent } from './checking.component';
import {routing } from './checking.routing';
import { PlanningComponent } from './components/planning/planning.component';

import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { Ng2SmartTableModule} from 'ng2-smart-table';
import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import {SlimBarService} from '../services/slim-bar.service';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import {DragulaModule} from 'ng2-dragula';
import { InCheckingComponent } from './components/in-checking/in-checking.component';
import { ShowDetailRenderComponent } from './components/in-checking/show-detail-render/show-detail-render.component';
import { InCheckingDetailsComponent } from './components/in-checking-details/in-checking-details.component';

import {TagNumberEditorComponent} from './components/in-checking-details/tag-number-editor/tag-number-editor.component';
import { MistakesEditorComponent } from './components/in-checking-details/mistakes-editor/mistakes-editor.component';

@NgModule({
  imports: [
    CommonModule,
    routing,
    NgbModalModule,
    NguiDatetimePickerModule,
    SlimLoadingBarModule.forRoot(),
    FormsModule,
    ReactiveFormsModule, 
    NgaModule,
    DragulaModule,
    Ng2SmartTableModule
  ],
  entryComponents:[ShowDetailRenderComponent,TagNumberEditorComponent,MistakesEditorComponent],
  providers:[SlimBarService],
  declarations: [CheckingComponent, PlanningComponent, InCheckingComponent,MistakesEditorComponent,TagNumberEditorComponent,
     ShowDetailRenderComponent, InCheckingDetailsComponent]
})
export class CheckingModule { }
