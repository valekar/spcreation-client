import {Inward,Tag} from './inward.model';
import {Order} from './order.model';
import {Employee} from './company.model';
import {Machine} from './user.model';

export class PlanningForm{
    shift:string;
    planningDate:string;
    machines:MachineForm[]
}

export interface MachineForm{
    idField: number;
    typeField:string;
    headsField: number;
    stitchCapacityField: number;
    noOfOrders: string;
    orders: OrderForm[];
}

export class OrderForm{
    inwardField: Inward;
    orderStitchesField:number;
    tags: Array<Tag>;
    tagsToBeProcessed: Array<Tag>;
    tagsClothesQuantity: string;
    operatorField:Employee;
    framerField:Employee;
    inchargeField:Employee;
}

export interface TagForm{
    tagField:Tag
}

export class InProduction{
    id:number;
    shift:string;
    date:string;
    machines:Array<ProdMachine>   
}

export class ProdMachine{
   id:number;
   type:string;
   heads:string;
   stitchCapacity:number;
   name:string;
   noOfOrders:number;
   orders:Array<ProdInward>;
}

export class ProdInward{
    id:number;
    tagsClothQuantity:number;
    inward:Inward;
    operator:Employee;
    inCharge:Employee;
    framer:Employee;
}
