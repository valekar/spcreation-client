import { Component, OnInit } from '@angular/core';
import { ProductionService } from '../../services/production.service';
import { Machine } from '../../../models/user.model';
import { Inward, Tag } from '../../../models/inward.model';
import { ResObj, ErrorResponse,SuccessReponseWithObj } from '../../../models/common.model';
import { Validators, FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { DragulaService } from 'ng2-dragula/ng2-dragula';
import { Constant } from '../../../models/constant.model';
import { Employee } from '../../../models/company.model';
import { CONSTANT } from '../../../models/CONSTANTS';
import { SlimBarService } from '../../../services/slim-bar.service';
import { ModalComponent } from '../../../services/modal/modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {InProduction} from '../../../models/production.model';



@Component({
  selector: 'app-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.scss']
})
export class PlanningComponent implements OnInit {

  constantList: Array<Constant>;
  machineList: Array<Machine>
  confirmedOrderInwards: Array<Inward>;
  framersList: Array<Employee> = new Array();
  operatorsList: Array<Employee> = new Array();
  inchargersList: Array<Employee> = new Array();
  public planningForm: FormGroup;
  activeModal: any;
  constructor(private productionService: ProductionService, private slimBarService: SlimBarService,private router:Router,
    private _fb: FormBuilder, private dragulaService: DragulaService, private modalService: NgbModal) {
    this.slimBarService.progress();
    this.dragulaCalculations();
  }

  ngOnInit() {
    this.planningNewFormGroup();
    this.getAllMachines();
    this.getAllConfirmedOrders();
    //this.getAllEmployees();
    this.getAllConstants();

    //this.getAllProductions();
  }

  planningNewFormGroup() {
    this.planningForm = this._fb.group({
      shift: ['', Validators.required],
      planningDate: ['', Validators.required],
      machines: this._fb.array([])
    });
  }

  machineNewFormGroup(machine: Machine) {
    return this._fb.group({
      idField: [machine.id],
      typeField: [machine.type],
      headsField: [machine.heads],
      stitchCapacityField: [machine.stitchCapacity],
      noOfOrders: [''],
      orders: this._fb.array([])
    });
  }

  orderNewFormGroup() {
    return this._fb.group({
      inwardField: [''],
      orderStitchesField: [''],
      tags: this._fb.array([]),
      tagsToBeProcessed: this._fb.array([]),
      tagsClothesQuantity: ['', Validators.required],
      operatorField: ['', Validators.required],
      framerField: ['', Validators.required],
      inchargeField: ['', Validators.required]
    })
  }

  tagNewFormGroup(tag) {
    return this._fb.group({
      tag: [tag],
      fieldId: [tag.id],
      fieldTagId: [tag.tagId],
      fieldQuantity: [tag.quantity],
      fieldQuantityCompleted:[tag.quantityCompleted]
    })
  }

  addMachineFormGroups(machineList) {
    const control = <FormArray>this.planningForm.controls['machines'];
    for (let machine of machineList) {
      control.push(this.machineNewFormGroup(machine));
    }
  }

  changeInOrderNumber(i: number) {
    const noOfOrders = +this.planningForm.controls['machines']['controls'][i].controls.noOfOrders.value;
    //reset the orders
    this.planningForm.controls['machines']['controls'][i].controls.orders = this._fb.array([]);
    const ordersControl = <FormArray>this.planningForm.controls['machines']['controls'][i].controls.orders;

    for (let i = 0; i < noOfOrders; i++) {
      //adding the orders
      ordersControl.push(this.orderNewFormGroup());
    }
    // console.log(ordersControl);
  }

  orderChange(machinesFormArrayId: number, ordersFormNumberId: number) {
    const noOfOrders = this.getNoOfOrders(machinesFormArrayId);
    if (noOfOrders > 0) {
      let ordersControl = <FormArray>this.planningForm.controls['machines']['controls'][machinesFormArrayId].controls.orders;
      const inward: Inward = ordersControl.controls[ordersFormNumberId]['controls'].inwardField.value;
      this.resetOrderFormControls(machinesFormArrayId, ordersFormNumberId);
      if (this.getSelectedInward(machinesFormArrayId, ordersFormNumberId) == "") {
        //this.resetOrderFormControls(machinesFormArrayId,ordersFormNumberId);
        //ordersControl.controls[ordersFormNumberId]['controls'].orderStitchesField.setValue("");
        //ordersControl.controls[ordersFormNumberId]['controls'].tags = this._fb.array([]);
        //ordersControl.controls[ordersFormNumberId]['controls'].tagsToBeProcessed = this._fb.array([]);
      }
      else {
        ordersControl.controls[ordersFormNumberId]['controls'].orderStitchesField.setValue(inward.order.stitches);
        //insert tag form group
        for (let tag of inward.tags) {
          //console.log(tag.status);
          //ordersControl.controls[ordersFormNumberId]['controls'].tags.push(this.tagNewFormGroup(tag));
          if (tag.status.toUpperCase() == CONSTANT.STATUS_PENDING.toUpperCase()) {
            ordersControl.controls[ordersFormNumberId]['controls'].tags.push(this.tagNewFormGroup(tag));
          }
          else if(tag.status.toUpperCase() == CONSTANT.STATUS_IN_PRODUCTION.toUpperCase()){
            console.log(tag.status);
            if(tag.quantity>tag.quantityCompleted){
              ordersControl.controls[ordersFormNumberId]['controls'].tags.push(this.tagNewFormGroup(tag));
            }
          }

        }
      }
    }
  }

  resetOrderFormControls(machinesFormArrayId: number, ordersFormNumberId: number) {
    let ordersControl = <FormArray>this.planningForm.controls['machines']['controls'][machinesFormArrayId].controls.orders;
    ordersControl.controls[ordersFormNumberId]['controls'].orderStitchesField.setValue("");
    ordersControl.controls[ordersFormNumberId]['controls'].tags = this._fb.array([]);
    ordersControl.controls[ordersFormNumberId]['controls'].tagsToBeProcessed = this._fb.array([]);
    ordersControl.controls[ordersFormNumberId]['controls'].tagsClothesQuantity.setValue("");
    ordersControl.controls[ordersFormNumberId]['controls'].operatorField.setValue("");
    ordersControl.controls[ordersFormNumberId]['controls'].framerField.setValue("");
    ordersControl.controls[ordersFormNumberId]['controls'].framerField.setValue("");
  }

  getNoOfOrders(machinesFormArrayId: number) {
    return +this.planningForm.controls['machines']['controls'][machinesFormArrayId].controls.noOfOrders.value;
  }

  getSelectedInward(machinesFormArrayId: number, ordersFormNumberId: number) {
    const ordersControl = <FormArray>this.planningForm.controls['machines']['controls'][machinesFormArrayId].controls.orders;
    return ordersControl.controls[ordersFormNumberId]['controls'].inwardField.value;
  }

  getOrderFormControl(machinesFormArrayId: number, ordersFormNumberId: number) {
    // if (this.planningForm.controls['machines']['controls'][machinesFormArrayId]
    //   .controls.orders.controls[ordersFormNumberId] == undefined) {
    //   this.Modal("Select Properly", "Drag the tags properly", true);
    //   //break;
    // }
    return <FormArray>this.planningForm.controls['machines']['controls'][machinesFormArrayId]
      .controls.orders.controls[ordersFormNumberId].controls;
  }

  dragulaCalculations() {
    this.dragulaService.drop.subscribe((value) => {
      let tdElement = null;
      let divElement = null;
      let bag = value[0];
      let i = +bag[0];
      let j = +bag[1];

      if (value[2].localName == 'td') {
        tdElement = value[2];
        divElement = value[3];
      }
      else if (value[3].localName == 'td') {
        tdElement = value[3];
        divElement = value[2];
      }
      //console.log(value[2]);
      this.tagsTobeProccessed(i, j, tdElement);

    });
  }

  tagsTobeProccessed(machineFormArrayId: number, orderFormArrayId: number, tdElement) {
    //we are getting the value in the form of element
    var td: Element = <Element>tdElement;
    //console.log(td);
    let orderFormControl = this.getOrderFormControl(machineFormArrayId, orderFormArrayId);

    if (td != null) {
      orderFormControl['tagsToBeProcessed'] = this._fb.array([]);
      orderFormControl['tagsClothesQuantity'].setValue("");
      if (td.children.length > 0) {
        let tagClothQuantity = 0;
        // orderFormControl['tagsToBeProcessed'] = this._fb.array([]);
        for (let i = 0; i < td.children.length; i++) {
          //from the order form control get that one tag control from many
          // if(td.children[i].children.length<=0){
          //   this.Modal("Select Properly","Drag the tags properly",true); 
          //   break;
          // }
          let tagFormArrayId = td.children[i].children[0].id;
          let droppedTagControl = orderFormControl['tags'].controls[tagFormArrayId];
          //console.log(droppedTagControl.controls.tagField.value);
          //get the tag 
          let tag: Tag = droppedTagControl.controls.tag.value;
          //console.log(droppedTagControl.controls.fieldQuantity.value);

          tagClothQuantity += tag.quantity - tag.quantityCompleted;
          orderFormControl['tagsToBeProcessed'].push(this.tagNewFormGroup(tag));
          //calculate the tags quantity      
        }
        orderFormControl['tagsClothesQuantity'].setValue(tagClothQuantity);
      }
      else {
        //clear tobeProcessed tags
        // orderFormControl['tagsToBeProcessed'] = this._fb.array([]);
      }
    }
  }

  getAllConfirmedOrders() {
    this.productionService.getAllConfirmedOrderInwards().subscribe(
      (res: ResObj<Inward>) => {
        this.confirmedOrderInwards = res.obj;
        //console.log(this.confirmOrderInwards);
      },
      (err: ErrorResponse) => {
      });
  }

  getAllMachines() {
    this.productionService.getAllMachines().subscribe(
      (res: ResObj<Machine>) => {
        this.machineList = res.obj;
        this.addMachineFormGroups(this.machineList);
      },
      (err: ErrorResponse) => {
      });
  }

  getAllConstants() {
    this.productionService.getAllContants().subscribe(
      (res: ResObj<Constant>) => {
        this.constantList = res.obj;
        this.getAllEmployees();
      },
      (err: ErrorResponse) => {
      });
  }

  getAllEmployees() {
    this.productionService.getAllEmployees().subscribe((res: ResObj<Employee>) => {
      let empList = res.obj;
      for (let constant of this.constantList) {
        for (let emp of empList) {
          for (let dept of emp.departments) {
            if (constant.key == CONSTANT.FRAMER.toUpperCase()) {
              if (dept.name == constant.key) {
                this.framersList.push(emp);
              }
            }
            if (constant.key == CONSTANT.INCHARGE.toUpperCase()) {
              if (dept.name == constant.key) {
                this.inchargersList.push(emp);
              }
            }
            if (constant.key == CONSTANT.OPERATOR.toUpperCase()) {
              if (dept.name == constant.key) {
                this.operatorsList.push(emp);
              }
            }
          }
        }
      }

    });
  }

  setPlanner(plannerForm) {
    //console.log(plannerForm);
    this.Modal(CONSTANT.ARE_YOU_SURE,CONSTANT.CREATE_DESC,false);
    
    this.activeModal.result.then(()=>{
      this.callPostPlanner(plannerForm);
    });

  }


callPostPlanner(plannerForm){
  let finalObj = this.productionService.planningCalculations(plannerForm);
  if (finalObj != null) {
    this.slimBarService.startLoading();
    this.productionService.postProductionPlanning(finalObj).subscribe((res: SuccessReponseWithObj<InProduction>) => {
      this.slimBarService.completeLoading();
      // console.log(res.obj);
      this.Modal(res.header, res.description, true);
      let inProd = res.obj;
      this.router.navigate([CONSTANT.TO_IN_PROD_REDIRECT+res.obj.id]);
    });
  }
}

  private Modal(header: String, description: String, isCloseModal: boolean) {
    this.activeModal = this.modalService.open(ModalComponent, { size: 'lg' });
    this.activeModal.componentInstance.modalHeader = header;
    this.activeModal.componentInstance.modalContent = description;
    this.activeModal.componentInstance.isCloseModal = isCloseModal;
  }


  planDateChanged(){}
}
