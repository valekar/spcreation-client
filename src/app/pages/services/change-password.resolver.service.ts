import {Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot,
         ActivatedRouteSnapshot } from '@angular/router'
import {UserService} from '../services/user.service';
import {Observable} from 'rxjs/Observable';
import {ChangePasswordToken} from '../models/user.model';

@Injectable()
export class ChangePasswordResolver implements Resolve<boolean>{
    public changePasswordToken: ChangePasswordToken = new ChangePasswordToken();
    constructor(private router : Router, private userService:UserService){}

    resolve(route : ActivatedRouteSnapshot, state:RouterStateSnapshot):Promise<any>{
       // let token = route.queryParams['token'];
       // console.log(token);
        this.changePasswordToken.token = route.queryParams['token'];
        return this.userService.validateToken(this.changePasswordToken).then((res)=> {
            //console.log(res);
            res['token'] = this.changePasswordToken.token;
            return res;
        });

    }
}