import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InCheckingComponent } from './in-checking.component';

describe('InCheckingComponent', () => {
  let component: InCheckingComponent;
  let fixture: ComponentFixture<InCheckingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InCheckingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InCheckingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
