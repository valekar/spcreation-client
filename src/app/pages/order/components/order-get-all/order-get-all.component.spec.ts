import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderGetAllComponent } from './order-get-all.component';

describe('OrderGetAllComponent', () => {
  let component: OrderGetAllComponent;
  let fixture: ComponentFixture<OrderGetAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderGetAllComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderGetAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
