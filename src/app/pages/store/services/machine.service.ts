import { Injectable } from '@angular/core';
import {CONSTANT} from '../../models/CONSTANTS';
import {Response} from '@angular/http'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import {CommonService} from '../../services/common.service';
import {URLS} from '../../models/URLS';
import {Machine,ResObj} from '../../models/user.model';
import {CustomHttp} from '../../../app-error-interceptor.service';
import {NumberEditorComponent} from '../components/machine/number-editor/number-editor.component'; 

@Injectable()
export class MachineService {

  constructor(private http:CustomHttp,private commonService:CommonService) { }


  getMachines(){
    return this.http.get(URLS.MACHINE_URL, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  getMachine(id:number){
    return this.http.get(URLS.MACHINE_URL + id, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  add(machine:Machine){
    return this.http.post(URLS.MACHINE_URL,machine,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  edit(machine:Machine,id:number){
    return this.http.put(URLS.MACHINE_URL+id,machine,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  delete(id:number){
    return this.http.delete(URLS.MACHINE_URL+id,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  getColumns(){
    return {
      id:{
        title:"M/C #",
        type:"text",
        editable:false
      },
      name:{
        title:"Name of Machine",
        type:"string"
      },
      heads:{
        title:"No of Heads",
        type:"string",
        editor:{
          type:"custom",
          component:NumberEditorComponent
        }
      },
      type:{
        title:"Type of Machine",
        type:"string"
      },
      stitchCapacity:{
        title:"Stitch Capacity",
        type:"text",
        editor:{
          type:"custom",
          component:NumberEditorComponent
        }
      },
      updated_by:{
        title:'Updated By',
        type:'string',
        editable:false,
      },
      formatted_updated_at:{
        title :"Updated At",
        type:"string",
        editable:false,
      }
    }
  }


}
