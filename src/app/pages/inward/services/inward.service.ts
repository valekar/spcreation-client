import { Injectable } from '@angular/core';
import {CustomHttp} from '../../../app-error-interceptor.service';
import {CONSTANT} from '../../models/CONSTANTS';
import {Response} from '@angular/http'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import {CommonService} from '../../services/common.service';
import {URLS} from '../../models/URLS';
import {Inward} from '../..//models/inward.model';
import {DateTimeEditorComponent} from '../components/inward-get-all/date-time-editor/date-time-editor.component';
import {NumberEditorComponent} from '../components/inward-get-all/number-editor/number-editor.component';
import {InwardOrderEditorComponent} from '../components/inward-get-all/inward-order-editor/inward-order-editor.component';
import {InwardOrderRenderComponent} from '../components/inward-get-all/inward-order-render/inward-order-render.component';
import {SPCRenderComponent} from '../components/inward-get-all/spcrender/spcrender.component';
import {StatusRenderComponent} from '../components/inward-get-all/status-render/status-render.component';
@Injectable()
export class InwardService {

  constructor(private http:CustomHttp,private commonService:CommonService) { }
  getAll(){
    return this.http.get(URLS.INWARD_URL, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  get(id:number){
    return this.http.get(URLS.INWARD_URL + id, this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  add(inward:Inward){
    return this.http.post(URLS.INWARD_URL,inward,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  edit(inward:Inward,id:number){
    return this.http.put(URLS.INWARD_URL+id,inward,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  delete(id:number){
    return this.http.delete(URLS.INWARD_URL+id,this.commonService.getHeaderOptions())
    .map((res:Response) => {return res.json()})
    .catch((err:Response)=> { return Observable.throw(err.json())});
  }

  getColumns(){
    return {
      SPC:{
        type:"custom",
        title:"SPC #",
        renderComponent:SPCRenderComponent,
        editable:false
      },
      inwardNumber:{
        title:"Inward #",
        type:"text",
        editor:{
          type:"custom",
          component:NumberEditorComponent
        }
      },
      layStr:{
        title:"Lay",
        type:"text",
        width:"15%",
        editor:{
          type:"custom",
          component:DateTimeEditorComponent
        }
      },
      order:{
        title:"Order",
        type:"custom",
        width:"35%",
        renderComponent:InwardOrderRenderComponent,
        editor:{
          type:"custom",
          component:InwardOrderEditorComponent
        },
        filter:false
      },
      status:{
        title:"status",
        type:"custom",
        width:"8%",
        editable:false,
        renderComponent:StatusRenderComponent
        
      },
      updated_by:{
        title:'Updated By',
        type:'text',
        editable:false
      },
      formatted_updated_at:{
        title :"Updated At",
        type:"text",
        editable:false,
      }
    }
  }
}
