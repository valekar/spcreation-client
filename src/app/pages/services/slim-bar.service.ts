import { Injectable } from '@angular/core';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';

@Injectable()
export class SlimBarService {

  constructor(private slimLoader:SlimLoadingBarService) {
    //this.slimLoader.progress = 30;
   }

   startLoading() {
       this.progress();
        this.slimLoader.start(() => {  
            console.log('Loading complete');
        });
    }

completeLoading() {
        this.progress();
        this.slimLoader.complete();
    } 

progress(){
    this.slimLoader.progress = 30;
}
    

}
