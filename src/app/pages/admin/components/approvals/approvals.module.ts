import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing }       from './approvals.routing';
import { NgaModule } from '../../../../theme/nga.module';
import {ApprovalsComponent } from '../approvals/approvals.component';
import { OrderApprovalsComponent } from './components/order-approvals/order-approvals.component';
import {ApprovalsService} from './services/approvals.service';
import { Ng2SmartTableModule} from 'ng2-smart-table';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import {ImageRenderComponent} from './components/order-approvals/image-render/image-render.component';
import {CustomerRenderComponent} from './components/order-approvals/customer-render/customer-render.component';
import { ActionRenderComponent } from './components/order-approvals/action-render/action-render.component';
import {SlimBarService} from '../../../services/slim-bar.service';

@NgModule({
  imports: [
    CommonModule,
    routing,
    NgaModule,
    Ng2SmartTableModule,
    SlimLoadingBarModule.forRoot()
  ],
  declarations: [ApprovalsComponent, OrderApprovalsComponent,ImageRenderComponent,CustomerRenderComponent, ActionRenderComponent],
  providers:[ApprovalsService,SlimBarService],
  entryComponents:[ImageRenderComponent,CustomerRenderComponent,ActionRenderComponent]
})
export class ApprovalsModule { }
