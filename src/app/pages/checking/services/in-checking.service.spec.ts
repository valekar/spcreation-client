import { TestBed, inject } from '@angular/core/testing';

import { InCheckingService } from './in-checking.service';

describe('InCheckingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InCheckingService]
    });
  });

  it('should be created', inject([InCheckingService], (service: InCheckingService) => {
    expect(service).toBeTruthy();
  }));
});
