import { Component, OnInit,Input } from '@angular/core';
import {InProduction} from '../../../../models/production.model';
@Component({
  selector: 'app-show-detail-render',
  templateUrl: './show-detail-render.component.html',
  styleUrls: ['./show-detail-render.component.scss']
})
export class ShowDetailRenderComponent implements OnInit {

  @Input() rowData:InProduction;
  constructor() { }

  ngOnInit() {
  }

}
