import { Component, OnInit,Input } from '@angular/core';
import {Order} from '../../../../../../models/order.model';
@Component({
  selector: 'app-customer-render',
  templateUrl: './customer-render.component.html',
  styleUrls: ['./customer-render.component.scss']
})
export class CustomerRenderComponent implements OnInit {

  @Input() rowData:Order; 
  
  constructor() { }

  ngOnInit() {
  }

}
