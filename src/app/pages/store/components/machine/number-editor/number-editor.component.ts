import { Component, OnInit } from '@angular/core';
import { DefaultEditor, Editor, Cell } from 'ng2-smart-table';

@Component({
  selector: 'app-number-editor',
  templateUrl: './number-editor.component.html',
  styleUrls: ['./number-editor.component.scss'],
  providers:[]
})
export class NumberEditorComponent extends DefaultEditor implements OnInit {

  numberModel: number;
  constructor() {
    super();
  }

  ngOnInit() {
    if (this.cell.getValue() !="") {
      //if (this.cell.getTitle() == this.machineService.getColumns().stitchCapacity.title) {
        this.numberModel = this.cell.getValue();
      //}
    }
  }

  changedValue(){
    //console.log(this.numberModel);
    this.cell.newValue = this.numberModel;
  }

}
