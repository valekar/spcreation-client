import { Component, OnInit } from '@angular/core';
import {OutwardService} from '../../services/outward.service'; 
import {Customer} from '../../../models/company.model';
import {ResObj,ErrorResponse} from '../../../models/user.model';
import { LocalDataSource } from 'ng2-smart-table';
import { CONSTANT } from '../../../models/CONSTANTS';
import { ModalComponent } from '../../../services/modal/modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SlimBarService } from '../../../services/slim-bar.service';
@Component({
  selector: 'app-outward',
  templateUrl: './outward.component.html',
  styleUrls: ['./outward.component.scss'],
  providers:[OutwardService]
})
export class OutwardComponent implements OnInit {

  customerList:Array<Customer>;
  settings: any;
  columns: any;
  activeModal: any;
  source: LocalDataSource;
  constructor(private outwardService:OutwardService,private slimBarService: SlimBarService, private modalService: NgbModal) {
    this.columns = this.outwardService.getCustomerColumns();
    this.settings = {
      actions: {
        add: false,
        edit: false,
        delete: false
      },
      hideSubHeader: false,
      add: CONSTANT.ADMIN_TABLE_ADD_CONFIG,
      edit: CONSTANT.ADMIN_TABLE_EDIT_CONFIG,
      delete: CONSTANT.ADMIN_TABLE_DELETE_CONFIG,
      columns: this.columns
    }
   }

  ngOnInit() {
    this.getAllCustomers();
  }


  getAllCustomers(){
    this.outwardService.getAllCustomers().subscribe(
      (res:ResObj<Customer>)=>{
        this.customerList = res.obj;
        this.source = new LocalDataSource(this.customerList);
      },
      (err:ErrorResponse)=>{}
    )

  }

}
